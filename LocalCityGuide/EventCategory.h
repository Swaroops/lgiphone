//
//  EventCategory.h
//  Visiting Ashland
//
//  Created by Anu on 12/05/16.
//  Copyright © 2016 iDeveloper. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventCategory : NSObject
@property (nonatomic, retain) NSString * eventcatid;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSString * status;
@end
