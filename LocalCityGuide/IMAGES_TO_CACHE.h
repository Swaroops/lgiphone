//
//  IMAGES_TO_CACHE.h
//  Visiting Ashland
//
//  Created by Anu on 29/12/15.
//  Copyright (c) 2015 iDeveloper. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface IMAGES_TO_CACHE : NSManagedObject
@property (nonatomic, retain) NSString * imageurl;
@property (nonatomic, retain) NSString * appid;

@end
