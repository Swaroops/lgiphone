

#import <UIKit/UIKit.h>
#import "ACPDownloadView.h"

@interface PDInappCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *appNameLbl;
@property (nonatomic, retain) IBOutlet UILabel *distLbl;
@property (nonatomic, retain) IBOutlet UIImageView *appImage;

@property (weak, nonatomic) IBOutlet ACPDownloadView *exampleIndicator;

@property (strong, nonatomic) IBOutlet UIButton *btnTblCellSel;

@property (strong, nonatomic) IBOutlet UIButton *btnOverAppLabel;


@end
