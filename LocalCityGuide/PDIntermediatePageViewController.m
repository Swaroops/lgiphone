//
//  PDIntermediatePageViewController.m
//  Visiting Ashland
//
//  Created by swaroop on 22/06/17.
//  Copyright © 2017 iDeveloper. All rights reserved.
//

#import "PDIntermediatePageViewController.h"
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"
#import "Reachability.h"
#import "Constants.h"
#import "PDViewController.h"
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"
#import "PDBuyViewController.h"
#import "PDWebViewController.h"
#import "MKStoreKit.h"
#import "FTWCache.h"
#import "ACPDownloadView.h"
#import "QuartzCore/QuartzCore.h"
#import "PDLoader.h"
#import "SSZipArchive.h"
#import "UIImageView+JMImageCache.h"
#import "CustomIOSAlertView.h"
#import "MKStoreKit.h"
#import "GMDCircleLoader.h"

@interface PDIntermediatePageViewController ()<UITabBarControllerDelegate,DBOperationsDelegate,NSURLSessionDataDelegate, NSURLSessionDelegate, NSURLSessionTaskDelegate,CustomIOSAlertViewDelegate,NSURLConnectionDelegate>
{
    NSMutableArray *inArr;
    NSInteger *indexe;
    BOOL isSingleApp;
    BOOL yThis;
    NSArray *arrFooters;
    PDViewController *homeVC;
    BOOL isOneGuide;
    UICircle* m_testView;
    UIButton *btnComplete;
    UIButton *cancelDwload;
    
    CustomIOSAlertView *popAlert;
    
    NSMutableArray *myMutableArrayAgain;
    NSMutableArray *arrGroups;
    NSString *paid_type;
    BOOL onlyOnce;
    
    NSURLSessionDataTask *dataTaske;
}

@property (nonatomic, retain) NSMutableData *dataToDownload;
@property (nonatomic) float downloadSize;

@end
UITabBarController *tabbar;
@implementation PDIntermediatePageViewController


-(void)viewWillAppear:(BOOL)animated
{
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if([[DBOperations getMainAppid] isEqualToString:@"266"])
    {
        if ([(NSString*)[UIDevice currentDevice].model hasPrefix:@"iPad"] )
        {
            
            [[NSBundle mainBundle] loadNibNamed:@"CRPDIntermediatePageViewController4" owner:self options:nil];
        }
        else if(IS_IPHONE_4_OR_LESS)
        {
            [[NSBundle mainBundle] loadNibNamed:@"CRPDIntermediatePageViewController4" owner:self options:nil];
        }
        else
        {
            [[NSBundle mainBundle] loadNibNamed:@"CRPDIntermediatePageViewController" owner:self options:nil];
            
        }

    }
    
    SDImageCache *imageCache =[SDImageCache sharedImageCache];
    imageCache = [imageCache initWithNamespace:@"JMCache"];
    
   
    
    
    inArr = [[NSMutableArray alloc]init];
    inArr= [[NSUserDefaults standardUserDefaults]objectForKey:@"guidesName"];
    
    indexe = [[NSUserDefaults standardUserDefaults]integerForKey:@"forGettingIndexPath"];
    
    NSString *str = [[inArr objectAtIndex:indexe]valueForKey:@"image"];
    
    
    NSString *sizeInterStr;
    
    if([[inArr objectAtIndex:indexe]valueForKey:@"zip_file_size"])
        sizeInterStr = [[inArr objectAtIndex:indexe]valueForKey:@"zip_file_size"];
    
    sizeIntermediateLbl.text = sizeInterStr;
    
    
    if(_fromSearchToInter)
    {
        if([[NSUserDefaults standardUserDefaults]objectForKey:@"guideimageFromASearch"])
        str = [[NSUserDefaults standardUserDefaults]objectForKey:@"guideimageFromASearch"];
        
       if([[NSUserDefaults standardUserDefaults]objectForKey:@"zipfilesizeFromlistto inter"])
       {
         sizeIntermediateLbl.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"zipfilesizeFromlistto inter"];
       }
    }
    
    UIImageView*imagevw=[[UIImageView alloc]init];
    imagevw.image=[imageCache imageFromDiskCacheForKey:str];
    
    NSString *string = [self extractNumberFromText:str];
    
  
    
    NSString *cacheDir = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    
    NSLog(@"str url :%@",str);
    
    NSString *filePath = [cacheDir stringByAppendingPathComponent:[NSString stringWithFormat:@"JMCache"]];
    
    NSString* Path = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",string]];
    
    UIImage* image = [UIImage imageWithContentsOfFile:Path];
    
    guideImage.image=image;

   
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        myMutableArrayAgain = [plistDict objectForKey:@"downloadsArray"];
    }
    

    
    
    
    paid_type=[[inArr objectAtIndex:indexe]objectForKey:@"paid_type"];
    
    if(_fromSearchToInter)
    {
       paid_type = [[NSUserDefaults standardUserDefaults]objectForKey:@"paid_typefromSearch"];
    }
    
    NSLog(@"paidtype :%@",paid_type);
    
    if([paid_type isEqualToString:@""]||paid_type.length==0||paid_type==nil||[paid_type isEqualToString:@"free"])
    {
      [btnpaid setBackgroundImage:[UIImage imageNamed:@"free.png"] forState:UIControlStateNormal];
        
        priceIntermediateLbl.text=@"$0.00";
    }
    else if([paid_type isEqualToString:@"paid"])
    {
        {
            
            
            if ([self reachable])
            {
                priceIntermediateLbl.text=@"$9.99";
                
                NSString *wUrl=[NSString stringWithFormat:@"http://insightto.com/webservice/settings.php?id=%@&multi=yes",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
                
                
                NSLog(@"url = %@",wUrl);
                
                NSURLSession *session = [NSURLSession sharedSession];
                NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    
                    if(data==nil)
                    {
                        
                        return ;
                    }
                    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    NSLog(@"URL :%@", wUrl);
                    NSLog(@"json :%@", json);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        
                        
                        NSArray *inappStr= [json objectForKey:@"inap_settings"];
                        [btnpaid setBackgroundImage:[UIImage imageNamed:@"paid_icon.png"] forState:UIControlStateNormal];
                        
                     
                        
                        priceIntermediateLbl.text= [NSString stringWithFormat:@"$%@",[[inappStr objectAtIndex:0] valueForKey:@"unlimited_category_price"]];
                        
                        
                    });
                }];
                
                [dataTask resume];
                
            
            }
            else
            {
              priceIntermediateLbl.text=@"$0.00";
                
            }
            
        }
        
       
    }
  
}

- (NSString *)extractNumberFromText:(NSString *)text
{
    NSCharacterSet *nonDigitCharacterSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    return [[text componentsSeparatedByCharactersInSet:nonDigitCharacterSet] componentsJoinedByString:@""];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(BOOL)reachable {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        

        return NO;
    }
    return YES;
}

-(BOOL)offlineAvailable
{
    if(![[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"100Percent%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]])
    {
        return NO;
    }
    else
        return YES;
}
- (IBAction)backBtn:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) receiveTestNotification:(NSNotification *) notification
{
    
    if ([[notification name] isEqualToString:@"TestNotification123"])
        NSLog (@"Successfully received the test notification!");
    
    [self loaderStop];
}

#pragma mark - PurchaseAction Loader
UIView *overlaay;
-(void)loaderStartWithText :(NSString *)loaderText
{
    
    
    overlaay =[[UIView alloc]initWithFrame:self.view.window.rootViewController.view.bounds];
    overlaay.backgroundColor =  [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    overlaay.tag = 1001;
    [overlaay setUserInteractionEnabled:YES];
    [GMDCircleLoader setOnView:overlaay withTitle:loaderText animated:YES];
    [self.view.window.rootViewController.view addSubview:overlaay];
   
}

-(void)loaderStop
{
    
    [GMDCircleLoader hideFromView:overlaay animated:YES];
    [overlaay removeFromSuperview];
}

#pragma mark -Inapp- Purchase Notification Methods
- (void) inappSuccessNotification:(NSNotification *) notification
{
    if(!onlyOnce)
    {
        onlyOnce=YES;
      [self storePurchaseContinueDownload];
    }
    
    [self loaderStop];
    NSLog (@"Successfully received the test notification %@ ",notification);
    
    
    
    
    NSArray *inappStr= [[NSUserDefaults standardUserDefaults]objectForKey:@"inappSettings"];
    NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
    dictionary=[inappStr objectAtIndex:0];
    
   
    NSString *notificationInap=[NSString stringWithFormat:@"%@",notification.object];
    
    if([notificationInap isEqual:[dictionary objectForKey:@"remove_ads_id_ios"]])
    {
        NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        [[NSUserDefaults standardUserDefaults]setObject:@"purchased" forKey:removeAdKey];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    else if([notificationInap isEqual:[dictionary objectForKey:@"unlimited_cat_id_ios"]])
    {
        NSLog (@"unlimitedcategry %@ ",notification);
        
        NSString *unlimicatKey=    [NSString stringWithFormat:@"unlimicat%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        
        [[NSUserDefaults standardUserDefaults]setObject:@"purchased" forKey:unlimicatKey];
        [[NSUserDefaults standardUserDefaults]synchronize];
      
    }
    
    
}
- (void) inappFailureNotification:(NSNotification *) notification
{
    [self loaderStop];
    NSLog (@"Successfully received the test notification %@ ",notification.object);
}
- (void) inappReStoreSuccessNotification:(NSNotification *) obj
{
    NSArray *inappids=obj.object;
    [self loaderStop];
    
    
    NSArray *inappStr= [[NSUserDefaults standardUserDefaults]objectForKey:@"inappSettings"];
    NSMutableDictionary *dictionary=[inappStr objectAtIndex:0];
    NSString *currentAppid=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
    
    
    
    for (int i=0;i<[inappids count];i++)
    {
        NSString *pid=[inappids objectAtIndex:i];
        if([pid isEqual:[dictionary objectForKey:@"remove_ads_id_ios"]])
        {
            NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
            [[NSUserDefaults standardUserDefaults]setObject:@"purchased" forKey:removeAdKey];
            [[NSUserDefaults standardUserDefaults]synchronize];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Restore Purchase" message:@"Remove Ads restored successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
          
        }
        else if([pid isEqual:[dictionary objectForKey:@"unlimited_cat_id_ios"]])
        {
            NSLog (@"unlimitedcategry %@ ",inappids);
            
            NSString *unlimicatKey=    [NSString stringWithFormat:@"unlimicat%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
            
            [[NSUserDefaults standardUserDefaults]setObject:@"purchased" forKey:unlimicatKey];
            [[NSUserDefaults standardUserDefaults]synchronize];
           
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Restore Purchase" message:@"Full access restored successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
        }
        
    }
    
    
    NSLog (@"Successfully received the test notification %@ ",inappids);
    
  
}
- (void) inappReStoreFailureNotification:(NSNotification *) notification
{
    
    [self loaderStop];
    NSLog (@"Successfully received the test notification %@ ",notification);
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Restore Purchase" message:@"Purchase restoring failed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    
    
}

- (IBAction)downloadBtn:(id)sender {
    
    
    if([self reachable])
    {
        
    if([paid_type isEqualToString:@"paid"])
    {
  
        {
            
            if ([self reachable])
            {
                
                
                NSString *wUrl=[NSString stringWithFormat:@"http://insightto.com/webservice/settings.php?id=%@&multi=yes",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
                
               
                NSLog(@"url = %@",wUrl);
                
                NSURLSession *session = [NSURLSession sharedSession];
                NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    
                    if(data==nil)
                    {
                        
                        return ;
                    }
                    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    NSLog(@"URL :%@", wUrl);
                    NSLog(@"json :%@", json);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                    
                        
                        
                         NSArray *inappStr= [json objectForKey:@"inap_settings"];
                         NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
                         dictionary=[inappStr objectAtIndex:0];
                         
                         if([[MKStoreKit sharedKit]isProductPurchased:[dictionary objectForKey:@"unlimited_cat_id_ios"]])
                         {
                         NSLog(@"Already Purchased");
                         
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                         message:@"Item already purchased"
                         delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                         
                         alert.tag=125;
                         
                         [alert show];
                         
                         [self loaderStop];
                         
                         
                         return ;
                         
                         }
                        
                    
                    [self loaderStartWithText:@"Purchasing"];
                    
                    [[NSNotificationCenter defaultCenter] addObserver:self
                                                             selector:@selector(inappSuccessNotification:)
                                                                 name:@"inappSuccessNotif"
                                                               object:nil];
                    [[NSNotificationCenter defaultCenter] addObserver:self
                                                             selector:@selector(inappFailureNotification:)
                                                                 name:@"inappFailureNotif"
                                                               object:nil];
                    [[NSNotificationCenter defaultCenter] addObserver:self
                                                             selector:@selector(inappReStoreSuccessNotification:)
                                                                 name:@"inappReStoreSuccessNotif"
                                                               object:nil];
                    [[NSNotificationCenter defaultCenter] addObserver:self
                                                             selector:@selector(inappReStoreFailureNotification:)
                                                                 name:@"inappReStoreFailureNotif"
                                                               object:nil];

                    
                    [[NSNotificationCenter defaultCenter] addObserver:self
                                                             selector:@selector(unlimitedCategoryPurchaseSuccess:)
                                                                 name:@"unlimitedCategoryPurchaseNotif"
                                                               object:nil];
                        
                        
                        if([dictionary objectForKey:@"unlimited_cat_id_ios"])
                        {
                            [[NSUserDefaults standardUserDefaults]setObject:[dictionary objectForKey:@"unlimited_cat_id_ios"] forKey:@"forGettingProductIDS"];
                        }

                        
                    [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:[dictionary objectForKey:@"unlimited_cat_id_ios"]];
                        
                      
                        });
                }];
                
                [dataTask resume];
                

            }
            else
            {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                                message:@"Could not connect to server"
                                                               delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            
            
            
            
            
        }
        
        return;
        
    }
        
    [[NSUserDefaults standardUserDefaults]setObject:@"downloadBtn" forKey:@"whichButtonPressed"];
    {
        if([self reachable])
            [self test];
        
        if([self reachable])
        {
            NSString *ida = [NSString stringWithFormat:@"launchfirst%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
            
            NSLog(@"ida :%@",ida);
            [[NSUserDefaults standardUserDefaults]setObject:@"noo" forKey:ida];
            
            [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"DataDownloaded"];
            
            

            [self  startSettingsWebServiceOfCurrentApp];
            
            
            [[NSUserDefaults standardUserDefaults]setInteger:indexe forKey:@"forIndexinHeaderArray"];
            
            CGRect frame;
            frame=CGRectZero;
            frame.origin.x=[UIScreen mainScreen].bounds.size.width/4;
             frame.origin.y=[UIScreen mainScreen].bounds.size.height/3;
             frame.size.height=[UIScreen mainScreen].bounds.size.height/2;
            frame.size.width=[UIScreen mainScreen].bounds.size.width/2;
            
            
            popAlert = [[CustomIOSAlertView alloc] init];
            
            float popUpWidth=self.view.frame.size.width-(self.view.frame.size.width*10/100);
            float popUpHeight=self.view.frame.size.height-(self.view.frame.size.height*40/100);
            
            UIView *viewInAlert = [[UIView alloc] initWithFrame:CGRectMake((self.view.frame.size.width-popUpWidth)/2,(self.view.frame.size.height-popUpHeight)/2 ,popUpWidth,popUpHeight)];
        
            viewInAlert.layer.cornerRadius = 7;
            viewInAlert.clipsToBounds = YES;
            
            [popAlert setContainerView:viewInAlert];
            viewInAlert.frame=CGRectMake((self.view.frame.size.width-popUpWidth)/2,(self.view.frame.size.height-popUpHeight)/2 ,popUpWidth,popUpHeight/4);
            
            [popAlert setDelegate:nil];
            
            popAlert.buttonTitles=[[NSArray alloc]initWithObjects:@"OK",nil];// @"OK",
            
            
            
            m_testView = [[UICircle alloc] initWithFrame:CGRectMake(downloadButton.frame.origin.x+((downloadButton.frame.size.width/2)-14), downloadButton.frame.origin.y+downloadButton.frame.size.height+10, 28, 28)];
            m_testView.percent = 0;
            m_testView.backgroundColor=[UIColor clearColor];
            
            
           [viewInAlert addSubview:m_testView];
            
            [self.view addSubview:m_testView];
           
            
            [onlineButton setEnabled:NO];
            [downloadButton setEnabled:NO];
            [bckBtn setEnabled:NO];
            
            
            
        }
        
            if([self reachable])
            {
                NSString *stri= [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
                NSLog(@"stri :%@",stri);
                
                if([self isTheAppHasOflineData:stri]&&[self offlineAvailable])
                    
                {
                    NSString *ida = [NSString stringWithFormat:@"launchfirst%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:@"yess" forKey:ida];
                }
                else{
                
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
        
                DBOperations *dbObject=[[DBOperations alloc]init];
                [dbObject setDelegate:nil];
                [dbObject dataWriteBeforeDownload];
        
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    
                });
            });
                    
            }
                
            }
        
        return;
    }

    }
    
else
    
{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"You need an active internet connection"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
    return;
}


}

-(void)testingLoadSplashImageForonlineButton
{

    splashImgv =[[UIImageView alloc]init];
    
    [self.view addSubview:vwLoading];
    splashImgv.frame=self.view.frame;
    [activityIndicator1 startAnimating];
    splashImgv.contentMode=UIViewContentModeScaleToFill;
    [self.view addSubview:splashImgv];
    
        
        NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        SDImageCache *imageCache =[SDImageCache sharedImageCache];
        imageCache = [imageCache initWithNamespace:imageCacheFolder];
        
        
        NSArray *arr= [[NSUserDefaults standardUserDefaults]objectForKey:@"guidesName"];
        
       NSString*str=[[arr objectAtIndex:indexe]objectForKey:@"splash_image"];
        
        
    splashImgv.image=[imageCache imageFromDiskCacheForKey:str];
        
        
          if(!splashImgv.image)
        {
            __block UIImage *img = nil;
            
            img = [[UIImage alloc] init];
            
            
            
            dispatch_async(dispatch_get_global_queue(0,0), ^{
                
               NSData * data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:str]];
                img = [UIImage imageWithData: data];
                
                [splashImgv sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil];

                [splashImgv setImageWithURL:[NSURL URLWithString:str] key:str placeholder:nil];
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    [splashImgv setImage:img];
                  
                    {
                        splashImgv.image=img;
                    }
                    
                    
                    int64_t delayInSeconds = 1.0;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                        
                        
                        [self startLoadingApp];
                     
                        
                    });
                    
                    
                    
                    
                });
            });
            
           
        }
    else
    {
        int64_t delayInSeconds = 1.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            
            [self startLoadingApp];
            
        });
        

    }
  
}
-(void)loadSplashImageTestForOnlineBtn
{
    [self.view addSubview:vwLoading];
    [activityIndicator1 startAnimating];
    splashImgv.contentMode=UIViewContentModeScaleToFill;
    
    NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    SDImageCache *imageCache =[SDImageCache sharedImageCache];
    imageCache = [imageCache initWithNamespace:imageCacheFolder];
    
    
    NSArray *arr= [[NSUserDefaults standardUserDefaults]objectForKey:@"guidesName"];
    
    NSString*str=[[arr objectAtIndex:indexe]objectForKey:@"splash_image"];
    
    NSString *offlineGuideStr = [[arr objectAtIndex:indexe]objectForKey:@"offline_guide"];
    
    if([offlineGuideStr isEqualToString:@""]||[offlineGuideStr isEqualToString:@"no"])
        
    {
       
        
        NSString *stt = [NSString stringWithFormat:@"%@",[NSURL URLWithString:str]];
        
        NSString *inte = [NSString stringWithFormat:@"splash_%@",[self extractNumberFromText:stt]];
        
        [splashImgv setImageWithURL:[NSURL URLWithString:stt] key:inte placeholder:nil];
        
        splashImgv.image=[imageCache imageFromDiskCacheForKey:stt];
        
        if (!splashImgv.image)
        {
          
            splashImgv.image=[imageCache imageFromDiskCacheForKey:stt];
            
        }
        
        return;
    }
    
    
    splashImgv.image=[imageCache imageFromDiskCacheForKey:str];
    
    [splashImgv sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil options:SDWebImageRefreshCached];
    
    
    
    if([offlineGuideStr isEqualToString:@""]||[offlineGuideStr isEqualToString:@"no"])
        
    {
        splashImgv.image=[imageCache imageFromDiskCacheForKey:str];
        
       
        
        if (!splashImgv.image)
        {
            [splashImgv sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil options:nil];
        }
        
    }
    
    else if (!splashImgv.image)
    {
        if (![str isEqualToString:@"NA"])
        {
            
            NSString *appNameFolder = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
            
            NSArray* foo = [appNameFolder componentsSeparatedByString:@".zip"];
            
            NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
            
            
            
            
            NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];
            
            NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];
            
            NSLog(@"firstBit folder :%@",firstBit);
            NSLog(@"secondBit folder :%@",secondBit2);
            
            NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
            
            NSLog(@"str url :%@",str);
            
            NSString *filePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/splash",secondBit2]];
            
            NSArray* str3 = [str componentsSeparatedByString:@"splash/"];
            
            NSString* firstBit3 = [str3 objectAtIndex:0]; NSString* secondBit3 = [str3 objectAtIndex:1];
            
            NSLog(@"firstBit folder :%@",firstBit3);
            NSLog(@"secondBit folder :%@",secondBit3);
            
            NSString* Path = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",secondBit3]];
            
            UIImage* image = [UIImage imageWithContentsOfFile:Path];
            
        
            splashImgv.image = image;
            
            if(!image)
            {
            
                if(!appNameFolder)
                {
                 NSString* zipName=[[arr objectAtIndex:indexe]objectForKey:@"app_name"];
                    
                
                    
                    NSString *appid =[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
                  
                    NSString *pstha = [NSString stringWithFormat:@"%@_images_%@",zipName,appid];
                    
                    NSLog(@"pstha :%@",pstha);
                    
                    NSString* Path = [pstha stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",secondBit3]];
                    
                    UIImage* image = [UIImage imageWithContentsOfFile:Path];
                    
                    splashImgv.image = image;
                    
                    NSLog(@"image :%@",image);
                    
                    
                }
            }
        }
        
        
        
    }
    
    
}

- (IBAction)onlineBtn:(id)sender {
    
   
    
    [self testingLoadSplashImageForonlineButton];
    
    [[NSUserDefaults standardUserDefaults]setObject:@"onlineBtn" forKey:@"whichButtonPressed"];
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.color=[UIColor whiteColor];
    CGFloat halfButtonHeight = onlineButton.bounds.size.height / 2;
    CGFloat buttonWidth = onlineButton.bounds.size.width;
    indicator.center = CGPointMake(buttonWidth - halfButtonHeight , halfButtonHeight);
    [onlineButton addSubview:indicator];
    [indicator startAnimating];
    
}

-(void)storePurchaseContinueDownload
{

    [[NSUserDefaults standardUserDefaults]setObject:@"downloadBtn" forKey:@"whichButtonPressed"];
{
    if([self reachable])
        [self test];
    
    if([self reachable])
    {
        NSString *ida = [NSString stringWithFormat:@"launchfirst%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        
        NSLog(@"ida :%@",ida);
        [[NSUserDefaults standardUserDefaults]setObject:@"noo" forKey:ida];
        
        [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"DataDownloaded"];
        
 
        
        [self  startSettingsWebServiceOfCurrentApp];
        
        
        [[NSUserDefaults standardUserDefaults]setInteger:indexe forKey:@"forIndexinHeaderArray"];
        
        CGRect frame;
        frame=CGRectZero;
        frame.origin.x=[UIScreen mainScreen].bounds.size.width/4;
        frame.origin.y=[UIScreen mainScreen].bounds.size.height/3;
        frame.size.height=[UIScreen mainScreen].bounds.size.height/2;
        frame.size.width=[UIScreen mainScreen].bounds.size.width/2;
        
 
        popAlert = [[CustomIOSAlertView alloc] init];
        
        float popUpWidth=self.view.frame.size.width-(self.view.frame.size.width*10/100);
        float popUpHeight=self.view.frame.size.height-(self.view.frame.size.height*40/100);
        
        UIView *viewInAlert = [[UIView alloc] initWithFrame:CGRectMake((self.view.frame.size.width-popUpWidth)/2,(self.view.frame.size.height-popUpHeight)/2 ,popUpWidth,popUpHeight)];
        
        viewInAlert.layer.cornerRadius = 7;
        viewInAlert.clipsToBounds = YES;
        
        [popAlert setContainerView:viewInAlert];
        viewInAlert.frame=CGRectMake((self.view.frame.size.width-popUpWidth)/2,(self.view.frame.size.height-popUpHeight)/2 ,popUpWidth,popUpHeight/4);
        
        [popAlert setDelegate:nil];
        
        popAlert.buttonTitles=[[NSArray alloc]initWithObjects:@"OK",nil];// @"OK",
        
        
        
        m_testView = [[UICircle alloc] initWithFrame:CGRectMake(downloadButton.frame.origin.x+downloadButton.frame.size.width/2, downloadButton.frame.origin.y+downloadButton.frame.size.height+10, 28, 28)];
        m_testView.percent = 0;
        m_testView.backgroundColor=[UIColor clearColor];
       
        
        [viewInAlert addSubview:m_testView];
        
        [self.view addSubview:m_testView];
        
        
        [onlineButton setEnabled:NO];
        [downloadButton setEnabled:NO];
        [bckBtn setEnabled:NO];
        
       
        
    }
    
    if([self reachable])
    {
        NSString *stri= [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
        NSLog(@"stri :%@",stri);
        
        if([self isTheAppHasOflineData:stri])
            
        {
            NSString *ida = [NSString stringWithFormat:@"launchfirst%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
            
            [[NSUserDefaults standardUserDefaults]setObject:@"yess" forKey:ida];
        }
        else{
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
               
                
                DBOperations *dbObject=[[DBOperations alloc]init];
                [dbObject setDelegate:nil];
                [dbObject dataWriteBeforeDownload];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    
                    
                });
            });
            
        }
        
    }
    
    return;
}
}

-(void)test
{
    
    NSString *wUrl=[NSString stringWithFormat:@"%@%@&multi=yes",offlinWB2,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSLog(@"url = %@",wUrl);
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(data==nil)
        {
            [self test];
            return ;
        }
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"URL :%@", wUrl);
        NSLog(@"json :%@", json);
        
        
        
        NSData *data2 = [NSKeyedArchiver archivedDataWithRootObject:json];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSArray *totalSize = [json objectForKey:@"total_file_size"];
            
            NSString *urlZip = [json objectForKey:@"app_image_folder"];
            
            [[NSUserDefaults standardUserDefaults]setObject:urlZip forKey:@"uurlZZip"];
            
            [[NSUserDefaults standardUserDefaults]setObject:urlZip forKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
            
            [self downloadZip];
            
            NSLog(@"totalSize :%@",totalSize);
            
            [[NSUserDefaults standardUserDefaults]setObject:totalSize forKey:@"TS"];
            
     
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[json objectForKey:@"businesses"]];
            [currentDefaults setObject:data forKey:[NSString stringWithFormat:@"forSearchOffline%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
            
            [currentDefaults setObject:data forKey:[NSString stringWithFormat:@"forGlobalSearchoffline"]];
            

        });
        
    }];
    
    [dataTask resume];
    
    
    
}

-(void)downloadZip
{
   
    
    NSString *url1 = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
    
    
    NSLog(@"Downloading Started");
    

    NSURL *url = [NSURL URLWithString:[url1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        NSInteger httpStatus = [((NSHTTPURLResponse *)response) statusCode];
        NSLog(@"responsecode:%ld", (long)httpStatus);
        
      
        if (error||httpStatus == 404) {
            
            NSLog(@"Download Error:%@",error.description);
            
           [m_testView removeFromSuperview];
            
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                            message:@"System error occured"
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            alert.tag=404;
            [alert show];
            
            [onlineButton setEnabled:YES];
            [downloadButton setEnabled:YES];
            [bckBtn setEnabled:YES];
            
            return ;
        }
        if (data) {
            
            
            NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
            documentsURL = [documentsURL URLByAppendingPathComponent:@"localFile.zip"];
            
            
            [data writeToURL:documentsURL atomically:YES];
            
            
            NSString *filename = @"localFile.zip";
            NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString * zipPath = [documentsDirectory stringByAppendingPathComponent:filename];
            
            NSString *destinationPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            [SSZipArchive unzipFileAtPath:zipPath toDestination:destinationPath];
            
            
                        
            NSLog(@"File is saved  ");
            
            
        }
        
        
        
    NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
        
        NSArray *contents = [[NSFileManager defaultManager]contentsOfDirectoryAtURL:documentsURL includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:nil];
        
        NSLog(@"description for zip:%@", [contents description]);
        
        
       
        
    }];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
    
        
        
        
  
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    
  
    NSURL *urlforData = [NSURL URLWithString:[url1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
        NSLog(@"urlforData 123 :%@",urlforData);
        
        [[NSUserDefaults standardUserDefaults]setObject:url1 forKey:@"urlforData123"];
        
        [self getContentLength:^(long long returnValue) {
            
            NSLog(@"your content length : %lld",returnValue);
            
            [[NSUserDefaults standardUserDefaults]setInteger:returnValue forKey:@"your content length"];
            
            dataTaske = [defaultSession dataTaskWithURL: urlforData];
            
            [dataTaske resume];
            
            
            
        }];
    
   
        dispatch_async(dispatch_get_main_queue(), ^{
            

        });
    });

    
    
    cancelDwload = [[UIButton alloc]init];
    cancelDwload.frame=downloadButton.frame;
    [cancelDwload setTitle:@"Cancel Download" forState:(UIControlStateNormal)];
    cancelDwload.titleLabel.font = [UIFont systemFontOfSize:12];
    [cancelDwload setTintColor:[UIColor whiteColor]];
    cancelDwload.backgroundColor=Rgb2UIColor(200, 81, 49);
    cancelDwload.layer.cornerRadius=5.0;
    [cancelDwload addTarget:self
                     action:@selector(cancelDownloadAction)
           forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:cancelDwload];

  
}

- (void) getContentLength : (void(^)(long long returnValue))completionHandler
{
   NSString *url= [[NSUserDefaults standardUserDefaults]objectForKey:@"urlforData123"];
    
   NSString * urlString =  [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
   
    
    __block long long totalContentFileLength = nil;
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    request.HTTPMethod = @"HEAD";
    [request addValue:@"identity" forHTTPHeaderField:@"Accept-Encoding"];
    
    
    NSURLSessionDownloadTask *uploadTask= [session downloadTaskWithRequest:request
                     completionHandler:^(NSURL *url,NSURLResponse *response,NSError *error) {
                         
                         NSLog(@"handler size: %lld", response.expectedContentLength);
                         
                         totalContentFileLength = [[NSNumber alloc] initWithFloat:response.expectedContentLength].longLongValue;
                         
                         completionHandler(totalContentFileLength);
                         
                     }];
    NSLog(@"content length=%lld", totalContentFileLength);
    [uploadTask resume];
    
}

#pragma mark -AlertView Delegate method
-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==404)
    {
        btnComplete.hidden=YES;
        downloadButton.hidden=NO;
        
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:[NSString stringWithFormat:@"100Percent%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
        
        [cancelDwload sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
    
    if(alertView.tag==125)
    {
        [self alreadyPurchasedContinueDownload];
    }
}

-(void)alreadyPurchasedContinueDownload
{
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"downloadBtn" forKey:@"whichButtonPressed"];
        
        
        if([self reachable])
            [self test];
        
        if([self reachable])
        {
            NSString *ida = [NSString stringWithFormat:@"launchfirst%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
            
            NSLog(@"ida :%@",ida);
            [[NSUserDefaults standardUserDefaults]setObject:@"noo" forKey:ida];
            
            [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"DataDownloaded"];
            
 
            
            [self  startSettingsWebServiceOfCurrentApp];
            
            
            [[NSUserDefaults standardUserDefaults]setInteger:indexe forKey:@"forIndexinHeaderArray"];
            
            CGRect frame;
            frame=CGRectZero;
            frame.origin.x=[UIScreen mainScreen].bounds.size.width/4;
            frame.origin.y=[UIScreen mainScreen].bounds.size.height/3;
            frame.size.height=[UIScreen mainScreen].bounds.size.height/2;
            frame.size.width=[UIScreen mainScreen].bounds.size.width/2;
            

            popAlert = [[CustomIOSAlertView alloc] init];
            
            float popUpWidth=self.view.frame.size.width-(self.view.frame.size.width*10/100);
            float popUpHeight=self.view.frame.size.height-(self.view.frame.size.height*40/100);
            
            UIView *viewInAlert = [[UIView alloc] initWithFrame:CGRectMake((self.view.frame.size.width-popUpWidth)/2,(self.view.frame.size.height-popUpHeight)/2 ,popUpWidth,popUpHeight)];
            
            viewInAlert.layer.cornerRadius = 7;
            viewInAlert.clipsToBounds = YES;
            
            [popAlert setContainerView:viewInAlert];
            viewInAlert.frame=CGRectMake((self.view.frame.size.width-popUpWidth)/2,(self.view.frame.size.height-popUpHeight)/2 ,popUpWidth,popUpHeight/4);
            
            [popAlert setDelegate:nil];
            
            popAlert.buttonTitles=[[NSArray alloc]initWithObjects:@"OK",nil];// @"OK",
            
            
            
            m_testView = [[UICircle alloc] initWithFrame:CGRectMake(downloadButton.frame.origin.x+((downloadButton.frame.size.width/2)-14), downloadButton.frame.origin.y+downloadButton.frame.size.height+10, 28, 28)];
            m_testView.percent = 0;
            m_testView.backgroundColor=[UIColor clearColor];
           
            
            [viewInAlert addSubview:m_testView];
            
            [self.view addSubview:m_testView];
            
            
            [onlineButton setEnabled:NO];
            [downloadButton setEnabled:NO];
            [bckBtn setEnabled:NO];
            
          
            
        }
        
    }
}

-(void)goBtn
{
    [self loadSplashImageTest];
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.color=[UIColor whiteColor];
    CGFloat halfButtonHeight = btnComplete.bounds.size.height / 2;
    CGFloat buttonWidth = btnComplete.bounds.size.width;
    indicator.center = CGPointMake(buttonWidth - halfButtonHeight , halfButtonHeight);
    [btnComplete addSubview:indicator];
    [indicator startAnimating];
    
    
    DBOperations *dbObject=[[DBOperations alloc]init];
    [dbObject setDelegate:self];
    [dbObject startOfflineDataLoading];


}

-(void)cancelDownloadAction
{
    onlyOnce=NO;
    
    [cancelDwload removeFromSuperview];
    
    [onlineButton setEnabled:YES];
    [downloadButton setEnabled:YES];
    [bckBtn setEnabled:YES];
    
    [m_testView removeFromSuperview];
    
    [dataTaske cancel];
    
}


-(void)downData
{
   
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSMutableArray *arrFavorites;
    
    {
        
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSMutableDictionary *plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
            arrFavorites = [plistDict objectForKey:@"downloadsArray"];
            
        
            if([fileManager fileExistsAtPath:finalPath])
            {
                NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
                myMutableArrayAgain = [plistDict objectForKey:@"downloadsArray"];
            }
            
            
            if (!myMutableArrayAgain) myMutableArrayAgain = [[NSMutableArray alloc] init];
            
            
            arrGroups=[[NSMutableArray alloc]initWithArray:inArr];

            
            if ([myMutableArrayAgain containsObject: [inArr objectAtIndex:indexe]])
            {
                NSLog(@"alreadycontains");
            }
            else
            {
                if([myMutableArrayAgain count]==0)
                {
                    myMutableArrayAgain = [[NSMutableArray alloc]init];
                   
                }
               
                
                NSString *fl = [NSByteCountFormatter stringFromByteCount:_downloadSize countStyle:NSByteCountFormatterCountStyleFile];
                NSLog(@"fl :%@",fl);
                
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                
                [dict setObject:fl forKey:@"FolderSize"];
                
                id something = [dict objectForKey:@"FolderSize"];
                
                NSLog(@"something :%@",something);
                
                 NSLog(@"%@",[arrGroups objectAtIndex:indexe]);
                
                NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                tempDict = [[arrGroups objectAtIndex:indexe] mutableCopy];
              
                [tempDict setObject:fl forKey:@"FolderSize"];
                
                
                
                [arrGroups replaceObjectAtIndex:indexe withObject:tempDict];
                
              
                NSLog(@"%@",[arrGroups objectAtIndex:indexe]);
                
                if(_fromSearchToInter)
                {
                
                }
                else
                [myMutableArrayAgain addObject:[arrGroups objectAtIndex:indexe]];
                
                [onlineButton setEnabled:YES];
                [downloadButton setEnabled:YES];
                [bckBtn setEnabled:YES];
                
                [[NSUserDefaults standardUserDefaults]setObject:@"skipIntermediate" forKey:[NSString stringWithFormat:@"skipIntermediate%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
            }
            
            [plistDict setObject:myMutableArrayAgain forKey:@"downloadsArray"];
            [plistDict writeToFile:finalPath atomically:YES];
            
        }
    }
    
}



- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler {
    completionHandler(NSURLSessionResponseAllow);
    
    
    _downloadSize=[response expectedContentLength];
    _dataToDownload=[[NSMutableData alloc]init];
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data {
    [_dataToDownload appendData:data];
   
    
    NSLog(@"_dataToDownload :%lu  ,_downloadedSize :%f",(unsigned long)[_dataToDownload length ],_downloadSize);
    
    
    float percenta = (float)_downloadSize;
    
    float sizeOFFoler=(float)[_dataToDownload length];
    
    NSLog(@"sizeFolder :%f",sizeOFFoler);
    
  
    
    percenta = [[NSUserDefaults standardUserDefaults]integerForKey:@"your content length"];
    NSLog(@"percenta :%f",percenta);
    
    float sizee = ((float)sizeOFFoler/(float)percenta)*100;
    
    NSLog(@"sizee :%f",sizee);
    
    NSLog(@"m_testView.percent :%f",m_testView.percent);
    
    if(sizee>=100)
    {
        
        [[NSUserDefaults standardUserDefaults]setObject:@"100Percent" forKey:[NSString stringWithFormat:@"100Percent%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
        
        int64_t delayInSeconds = 2.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            
            
            
            
        
        [m_testView removeFromSuperview];
        
    
        btnComplete = [[UIButton alloc]init];
        btnComplete.frame=downloadButton.frame;
        [btnComplete setTitle:@"GO" forState:(UIControlStateNormal)];
        btnComplete.titleLabel.font = [UIFont systemFontOfSize:12];
        [btnComplete setTintColor:[UIColor whiteColor]];
        btnComplete.backgroundColor=Rgb2UIColor(0, 153, 51);
        btnComplete.layer.cornerRadius=5.0;
        [btnComplete addTarget:self
                        action:@selector(goBtn)
              forControlEvents:UIControlEventTouchUpInside];
        
            
           
            
            NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
            
            NSString* foofile = [documentsPath stringByAppendingPathComponent:@"app_image_folder"];
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
            
         
           
            int64_t delayInSeconds = 2.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
               
                [self.view addSubview:btnComplete];
                
                downloadButton.hidden=YES;
                
                 [self downData];
                
            });
        
        
       
        return;
            
            });
    }

    
    if (sizee < 100) {
        
        if (m_testView.percent < 1)
        {
            m_testView.percent = (float)sizee;
            [m_testView setNeedsDisplay];
        }
        else
        {
            m_testView.percent = (int)sizee;
            [m_testView setNeedsDisplay];
        }
        
    }
    
 
}

-(BOOL)isTheAppHasOflineData:(NSString*)appid
{
    
    NSError *error;
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Categories" inManagedObjectContext:context];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"appid=%@",appid];
    [fetchRequest setEntity:entity];
    
    
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    BOOL hasData=false;
    if([fetchedObjects count]>0)
        hasData=true;
    return hasData;
    
    
    
}


- (void)startLoadingApp {
    
    NSLog(@".... %@",[[inArr objectAtIndex:indexe]objectForKey:@"id"]);
    
  
    if([self offlineAvailable])
    {
        [self coreDataReadAppsSettings];
    }
    else if([self reachable])
    {
        [self  startSettingsWebServiceOfCurrentApp];
    }
    else
    {
        
        [vwLoading removeFromSuperview];
        
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"You need an active internet connection"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
      
        
    }
    
}

-(void)loadSplashImageTest
{
    [self.view addSubview:vwLoading];
    [activityIndicator1 startAnimating];
    splashImgv.contentMode=UIViewContentModeScaleToFill;
    
    NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    SDImageCache *imageCache =[SDImageCache sharedImageCache];
    imageCache = [imageCache initWithNamespace:imageCacheFolder];
    
    
    
    NSArray *arr= [[NSUserDefaults standardUserDefaults]objectForKey:@"guidesName"];
    
    NSString*str=[[arr objectAtIndex:indexe]objectForKey:@"splash_image"];
    
    splashImgv.image=[imageCache imageFromDiskCacheForKey:str];
    
    [splashImgv sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil];
    
    if (!splashImgv.image)
    {
        if (![str isEqualToString:@"NA"])
        {
            
            NSString *appNameFolder = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
            
            NSArray* foo = [appNameFolder componentsSeparatedByString:@".zip"];
            
            NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
            
            
            
            
            NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];
            
            NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];
            
            NSLog(@"firstBit folder :%@",firstBit);
            NSLog(@"secondBit folder :%@",secondBit2);
            
            NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
            
            NSLog(@"str url :%@",str);
            
            NSString *filePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/splash",secondBit2]];
            
            NSArray* str3 = [str componentsSeparatedByString:@"splash/"];
            
            NSString* firstBit3 = [str3 objectAtIndex:0]; NSString* secondBit3 = [str3 objectAtIndex:1];
            
            NSLog(@"firstBit folder :%@",firstBit3);
            NSLog(@"secondBit folder :%@",secondBit3);
            
            NSString* Path = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",secondBit3]];
            
            UIImage* image = [UIImage imageWithContentsOfFile:Path];
            
         
            splashImgv.image = image;
        }
        
        
        
    }
    
    
    int64_t delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [vwLoading removeFromSuperview];
        
        
        
    });

}
#pragma mark - Webservice for settings

/*This functions request for settings from server of the selected app*/
-(void)startSettingsWebServiceOfCurrentApp
{
    
    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(settingsServiceResponseOfCurrentApp:);
    isSingleApp=[DBOperations isSingleApp];
    NSString *str;
    if (isSingleApp) {
        str=[NSString stringWithFormat:@"%@%@&multi=no",SETTINGS_WEBSERVICE_SUB,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    }
    else{
        NSString *ismulti=@"yes";
       
        
        str=[NSString stringWithFormat:@"%@%@&multi=%@",SETTINGS_WEBSERVICE_SUB,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"],ismulti];
    }
    [webService startParsing:str];
}
/*Webservice response method of selected app settings */
-(void)settingsServiceResponseOfCurrentApp:(NSData *) responseData
{
    NSError *er;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&er];
    NSArray *settingsArray=[dict objectForKey:@"settings"];
    NSDictionary *inappDict = [dict objectForKey:@"inap_settings"];
    
    if([settingsArray count]>0)
        
    {
        NSDictionary *settingsDict=[settingsArray objectAtIndex:0];
        
        [self saveCurrentAppSettings:settingsDict];
        [self saveCurrentInappSetting:inappDict];
        
        
        [self coreDataWriteAppsSettings:settingsDict forInappDic:inappDict];
        
        if([self reachable])
            [self footerValuesWebService];
        else
            [self coreDataReadFooterList];
        
    }
    
    
}
/*This function save selected app details to nsuserdefault for accessing the Inapp settings while control is inside the selected app. This storage is for only accessing it from anywhere of the app. When a new app is selected this replaces the data with Inapp settings data of new app*/
-(void)saveCurrentInappSetting:(NSDictionary*)inappDict
{
    [[NSUserDefaults standardUserDefaults]setObject:inappDict forKey:@"inappSettings"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSArray *inappStr= [[NSUserDefaults standardUserDefaults]objectForKey:@"inappSettings"];
    NSDictionary *dictionary=[inappStr objectAtIndex:0];
    [[MKStoreKit sharedKit]addProductWithIdentifier:[dictionary objectForKey:@"remove_ads_id_ios"]];
    [[MKStoreKit sharedKit]addProductWithIdentifier:[dictionary objectForKey:@"unlimited_cat_id_ios"]];
    
    
    
}
/*This function save selected app details to nsuserdefault for accessing the settings while control is inside the selected app. This storage is for only accessing it from anywhere of the app. When a new app is selected this replaces the data with settings data of new app*/
-(void)saveCurrentAppSettings:(NSDictionary*)dict
{
    
    [[NSUserDefaults standardUserDefaults]setObject:[dict objectForKey:@"app_home_header_img"] forKey:@"app_home_header_img"];
    
    
    [[NSUserDefaults standardUserDefaults]setObject:[dict objectForKey:@"app_title"] forKey:@"AppTitle"];
    
    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]init];
    
    
    [tempDict setValue:[dict objectForKey:@"ad_1"] forKey:@"ad_1"];
    [tempDict setValue:[dict objectForKey:@"ad_2"] forKey:@"ad_2"];
    [tempDict setValue:[dict objectForKey:@"ad_3"] forKey:@"ad_3"];
    
    [tempDict setValue:[dict objectForKey:@"link_1"] forKey:@"link_1"];
    [tempDict setValue:[dict objectForKey:@"link_2"] forKey:@"link_2"];
    [tempDict setValue:[dict objectForKey:@"link_3"] forKey:@"link_3"];
    
    [[NSUserDefaults standardUserDefaults] setObject:tempDict forKey:@"CustomAdDict"];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:[dict objectForKey:@"admob_status"] forKey:@"admob_status"];
    if ([[dict objectForKey:@"admob_status"] isEqualToString:@"Yes"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"CustomAd"];
        [[NSUserDefaults standardUserDefaults] setObject:[dict objectForKey:@"admob_id"] forKey:@"admob_id"];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"CustomAd"];
    }
    
    if ([[dict objectForKey:@"distance_in"] isEqualToString:@"km"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"KM"];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"KM"];
    }
    if ([[dict objectForKey:@"app_view"] isEqualToString:@"grid"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"VIEW_GRID"];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"VIEW_GRID"];
    }
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"HomeResponse"]) {
    }
    if([[dict objectForKey:@"offline_guide"] isEqualToString:@"yes"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"offline_guide"];
        [[NSUserDefaults standardUserDefaults] setObject:[dict objectForKey:@"refresh_interval"] forKey:@"refresh_interval"];
    }
    else
        [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"offline_guide"];
    
    
    if([[dict objectForKey:@"fav_icon"] isEqualToString:@"heart"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"heart" forKey:@"fav_icon"];
        
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"star" forKey:@"fav_icon"];
    }
    
    
}

/*This writes selected app settings to coredata for offline use*/
-(void)coreDataWriteAppsSettings:(NSDictionary*)dict forInappDic:(NSDictionary*)inappDict{
    
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context1 = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"appid==%@", [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    
    
    [fetchRequest setPredicate:predicate];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Apps" inManagedObjectContext:context1];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *results = [context1 executeFetchRequest:fetchRequest error:&error];
    NSManagedObject* favoritsGrabbed = [results objectAtIndex:0];
    [favoritsGrabbed setValue:dict forKey:@"settings"];
    [favoritsGrabbed setValue:inappDict forKey:@"inappsettings"];
    
    
    
   
    if (![context1 save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    else{
        
        
    }
    
    
}

/*This read App settings of selected app if online data is not available*/
-(void)coreDataReadAppsSettings{
    
    NSError *error;
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Apps" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"appid==%@", [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    
    
    
    [fetchRequest setPredicate:predicate];
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
    
    @try {
        Apps *appsObj=[fetchedObjects objectAtIndex:0];
        NSMutableDictionary *dicObject=[[NSMutableDictionary alloc]init];
        [dicObject setObject:appsObj.settings forKey:@"settings"];
        [dicObject setObject:appsObj.inappsettings forKey:@"inappsettings"];
        
        NSDictionary *setObjDict=[dicObject objectForKey:@"settings"];
        NSDictionary *inappSetObjDict=[dicObject objectForKey:@"inappsettings"];
        
        arrFooters  = [[NSArray alloc]init];
     
        [self saveCurrentAppSettings:setObjDict];
        
        [self saveCurrentInappSetting:inappSetObjDict];
        
        
        if([self isTheAppHasOflineData:[[inArr objectAtIndex:indexe]objectForKey:@"id"]])
        {
            if([self reachable])
                [self footerValuesWebService];
            else
                [self coreDataReadFooterList];
        }
        else if([self reachable])
            [self footerValuesWebService];
        else
            [self coreDataReadFooterList];
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Could not connect to server"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
       
        [self  startSettingsWebServiceOfCurrentApp];
        
    }
    
    
    
    
    
}

#pragma mark - Webservice and methods for Footer
/*This function request for tabbar value details from server for selected the app*/
-(void)footerValuesWebService
{
    
    
    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(footerValuesWebServiceResponse:);
    [webService startParsing:[NSString stringWithFormat:@"%@%@",FOOTER_WEBSERVICE,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
}

/*Webservice response function for tabbar details*/
-(void)footerValuesWebServiceResponse:(NSData *) responseData
{
    NSError *er;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&er];
    arrFooters = [dict objectForKey:@"footer"];
    
    if ([arrFooters isKindOfClass:[NSNull class]])
    {
        UILabel *errorLbl = [[UILabel alloc]initWithFrame:self.view.frame];
        errorLbl.textAlignment = NSTextAlignmentCenter;
        errorLbl.textColor=[UIColor blackColor];
        errorLbl.font=[UIFont systemFontOfSize:14];
        errorLbl.text=@"Null value is being returned. Please check webservice!";
        errorLbl.numberOfLines=2;
       
        return;
    }
    
    
    [[NSUserDefaults standardUserDefaults] setObject:arrFooters forKey:@"footerListArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
  
    NSLog(@"isTheAppHasOflineData :%@",[[inArr objectAtIndex:indexe]objectForKey:@"id"]);
    
    
    {
        [self coreDataWritefooterValues:dict];
    }
    [self setFooterWithArray:arrFooters];
    
    
}

/*This function write tabbar values to coredata for offline use*/
-(void)coreDataWritefooterValues:(NSDictionary*)dict{
    
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context1 = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"appid==%@", [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    
    
    
    [fetchRequest setPredicate:predicate];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Apps" inManagedObjectContext:context1];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *results = [context1 executeFetchRequest:fetchRequest error:&error];
    NSManagedObject* favoritsGrabbed = [results objectAtIndex:0];
    [favoritsGrabbed setValue:dict forKey:@"footerList"];
    
    
   
    if (![context1 save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    else{
       
        
    }
    
    
}


/*This read details of tabview details from coredata if online data is not available*/
-(void)coreDataReadFooterList{
    
    NSError *error;
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Apps" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"appid==%@", [[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    
    
    [fetchRequest setPredicate:predicate];
    
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
    
    
    
    
    arrFooters  = [[NSArray alloc]init];
    @try {
        Apps *appsObj=[fetchedObjects objectAtIndex:0];
        NSMutableDictionary *dicObject=[[NSMutableDictionary alloc]init];
        
        NSString *identifier = appsObj.footerList;
        
        if(identifier !=nil)
            [dicObject setObject:appsObj.footerList forKey:@"footerList"];
        else
            [dicObject setObject:@"" forKey:@"footerList"];
        
        
        arrFooters=[[dicObject objectForKey:@"footerList"]objectForKey:@"footer"];
        [self setFooterWithArray:arrFooters];
        
    }
    @catch (NSException * e) {
        
        NSLog(@"===%@",e);
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Could not connect to server"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
      
        
        [self footerValuesWebService];
    }
    
   
}

/*This function load footer values*/
-(void)loadFooterValues
{
    
    
    [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",FOOTER_WEBSERVICE,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]]]
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if (!error) {
                                   
                                   
                                   NSError *er;
                                   NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&er];
                                   arrFooters = [dict objectForKey:@"footer"];
                                   [[NSUserDefaults standardUserDefaults] setObject:arrFooters forKey:@"footerListArray"];
                                   [[NSUserDefaults standardUserDefaults] synchronize];
                                   
                                   
                                   [self performSelector:@selector(setFooterWithArray:) withObject:arrFooters afterDelay:0.0];
                                   NSMutableArray* myMutableArrayAgain = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"footerListArray"]];
                                   if (![myMutableArrayAgain isEqualToArray:arrFooters]) {
                                       
                                       
                                       [self setFooterWithArray:arrFooters];
                                   }
                                   
                               }
                               else {
                                   
                                   
                                   NSMutableArray* myMutableArrayAgain = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"footerListArray"]];
                                   [self setFooterWithArray:myMutableArrayAgain];
                                   
                                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Couldnot fetch values from server. Please try later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                   [alert show];
                               }
                               
                           }];
}


/*This function set appropriate tab to tabviewcontroller*/
-(void)setFooterWithArray:(NSArray *) arrayFooter {
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"order"  ascending:YES];
    NSArray *arrtemp3 = [arrayFooter sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
    NSArray *arrVals = [arrtemp3 copy];
    int selectedIndex;
    NSMutableArray *arrayFooters = [[NSMutableArray alloc] init];
    for (int i = 0; i < [arrVals count]; i++) {
        NSDictionary *dict1 = [arrVals objectAtIndex:i];
        if ([[dict1 objectForKey:@"name"] isEqualToString:@"Home"]) {
            selectedIndex = i;
        }
        UIViewController *viewController = [self checkAndReturnClass:dict1];
        UINavigationController *navig1 = [[UINavigationController alloc] initWithRootViewController:viewController];
        [arrayFooters addObject:navig1];
    }
    
    
    
    NSMutableArray* myMutableArrayAgain = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"footerListArray"]];
    
    tabbar = [[UITabBarController alloc] init];
    if (IS_OS_7_OR_LATER) {
        [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Verdana" size:10.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
       
        [[UITabBar appearance] setTintColor:[UIColor colorWithRed:251/255.0f green:173/255.0f blue:24/255.0f alpha:1.0]];
        [[UITabBar appearance] setTranslucent:YES];
    }
    else {
        [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Verdana" size:10.0f], UITextAttributeFont, nil] forState:UIControlStateNormal];
    }
    
    
    
    
    [tabbar setViewControllers:arrayFooters];
    tabbar.selectedIndex = selectedIndex;
    tabbar.delegate = self;
    
  
    NSString *wchBtn=[[NSUserDefaults standardUserDefaults]objectForKey:@"whichButtonPressed"];
    
    if([wchBtn isEqualToString:@"onlineBtn"])
    {
     DBOperations *dbObject=[[DBOperations alloc]init];
     [dbObject setDelegate:self];
     [dbObject startOfflineDataLoading];
    }
    
}
- (void) processCompleted:resultArray
{
    UIImageView *image = [[UIImageView alloc]init];
 
    NSLog(@"headerImage.image :%@",image.image);
    
    NSLog(@"result array=%@",resultArray);
    if([resultArray count]==0)
    {

    }
 
    
    NSString *str =   [[NSUserDefaults standardUserDefaults]objectForKey:@"DataDownloaded"];
    
    
    int64_t delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [[self navigationController] pushViewController:tabbar animated:YES];
    
        });
    
    [homeVC fillDataArray:resultArray];
   
    if(![self.navigationController.topViewController isKindOfClass:[tabbar class]]) {
        if(yThis)
        {
            
            [[NSUserDefaults standardUserDefaults]setBool:yThis forKey:@"yThis"];
            [[self navigationController] pushViewController:tabbar animated:NO];
            
            [vwLoading removeFromSuperview];
            yThis=false;
            return;
            
        }
        
    
    }
    
}

-(UIViewController *)checkAndReturnClass:(NSDictionary *) dictionary
{
    NSString *value = [dictionary objectForKey:@"content_type"];
    if ([value isEqualToString:@"Favorites"]) {
        PDFavoriteViewController *viewController = [[PDFavoriteViewController alloc] init];
        viewController.title = [dictionary objectForKey:@"name"];
        
        AsyncImageView*img=[[AsyncImageView alloc]init];
        
       
        [img sd_setImageWithURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]placeholderImage:nil];
        
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]]];
        
        UIImage *imageFromImageView = image;

        
        UIImage *screw =  [self imageByScalingImage:imageFromImageView];
        
        
      
        
        viewController.tabBarItem.image = screw;
        
        
        return viewController;
    }
    if ([value isEqualToString:@"Map"]){
        PDMapViewController *viewController = [[PDMapViewController alloc] init];
        viewController.title = [dictionary objectForKey:@"name"];
        
        UIImageView*img=[[UIImageView alloc]init];
      
        [img sd_setImageWithURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]placeholderImage:nil];
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]]];
        
        UIImage *imageFromImageView = image;

        
        UIImage *screw =  [self imageByScalingImage:imageFromImageView];
        
 
        
        viewController.tabBarItem.image = screw;
        return viewController;
    }
    if ([value isEqualToString:@"form"]) {
        PDRelocationViewController *viewController = [[PDRelocationViewController alloc] init];
        viewController.title = [dictionary objectForKey:@"name"];
        
        UIImageView*img=[[UIImageView alloc]init];
      
        [img sd_setImageWithURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]placeholderImage:nil];
        
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]]];
        
        UIImage *imageFromImageView = image;

        
        UIImage *screw =  [self imageByScalingImage:imageFromImageView];
        
        viewController.tabBarItem.image = screw;
        return viewController;
    }
    if ([value isEqualToString:@"Deals"]) {
        PDDealsViewController *viewController = [[PDDealsViewController alloc] init];
        viewController.title = [dictionary objectForKey:@"name"];
        if (![[dictionary objectForKey:@"maxdealid"] isEqualToString:@"FALSE"]){
            if (![[dictionary objectForKey:@"maxdealid"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"maxDealId"]]) {
                viewController.tabBarItem.badgeValue = @"new";
            }
        }
        
        UIImageView*img=[[UIImageView alloc]init];
      
        [img sd_setImageWithURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]placeholderImage:nil];
        
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]]];
        
        UIImage *imageFromImageView = image;

        
        UIImage *screw =  [self imageByScalingImage:imageFromImageView];
        
   
        viewController.tabBarItem.image = screw;
        return viewController;
    }
    
    if ([value isEqualToString:@"Buy"]||[value isEqualToString:@"Purchase"]) {
        PDBuyViewController *viewController = [[PDBuyViewController alloc] init];
        viewController.title = [dictionary objectForKey:@"name"];
        
        UIImageView*img=[[UIImageView alloc]init];
      
       [img sd_setImageWithURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]placeholderImage:nil];
        
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]]];
        
        UIImage *imageFromImageView = image;

        
        UIImage *screw =  [self imageByScalingImage:imageFromImageView];
        
        viewController.tabBarItem.image = screw;
        return viewController;
    }
    if ([value isEqualToString:@"Events"]) {
        PDEventsViewController *viewController = [[PDEventsViewController alloc] init];
        viewController.title = [dictionary objectForKey:@"name"];
        if (![[dictionary objectForKey:@"maxeventid"] isEqualToString:@"FALSE"]) {
            if (![[dictionary objectForKey:@"maxeventid"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"maxEventId"]]) {
                viewController.tabBarItem.badgeValue = @"new";
            }
        }
        
        UIImageView*img=[[UIImageView alloc]init];
      
       [img sd_setImageWithURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]placeholderImage:nil];
        
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]]];
        
        UIImage *imageFromImageView = image;

        
        UIImage *screw =  [self imageByScalingImage:imageFromImageView];
      
        
        viewController.tabBarItem.image = screw;
        viewController.hidesBottomBarWhenPushed=YES;
        return viewController;
    }
    if ([value isEqualToString:@"Home"]) {
        homeVC = [[PDViewController alloc] init];
        homeVC.showBkBtn=YES;
        if (isOneGuide)
            homeVC.showBkBtn=NO;
        [homeVC initwithNavigationController:self.navigationController];
        homeVC.title = [dictionary objectForKey:@"name"];
        homeVC.appName=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppName"];
        UIImageView*img =[[UIImageView alloc]init];
       
        [img sd_setImageWithURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]placeholderImage:nil];
        
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]]];
        
        UIImage *imageFromImageView = image;
        
        UIImage *screw =  [self imageByScalingImage:imageFromImageView];
        
        
        homeVC.tabBarItem.image = screw;
        
        NSInteger lengt =  homeVC.title.length;
        NSLog(@"lengt :%ld",(long)lengt);
        
        if (lengt > 8) {
            
            homeVC.title = [homeVC.title substringToIndex:8];
        }
        
        
        return homeVC;
    }
    if ([value isEqualToString:@"category"]) {
        if ([[dictionary objectForKey:@"type"] isEqualToString:@"url"])
        {
            PDWebViewController *webView = [[PDWebViewController alloc] initWithNibName:@"PDWebViewController" bundle:nil];
            webView.title = [dictionary objectForKey:@"name"];
            [webView getDictionary:dictionary];
            
            UIImageView*img=[[UIImageView alloc]init];
         
           [img sd_setImageWithURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]placeholderImage:nil];
            
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]]];
            
            UIImage *imageFromImageView = image;

            
            UIImage *screw =  [self imageByScalingImage:imageFromImageView];
       
            
            webView.tabBarItem.image = screw;
            return webView;
        }
        else if ([[dictionary objectForKey:@"type"] isEqualToString:@"static"])
        {
            PDDetailViewController *detailViewController = [[PDDetailViewController alloc] init];
            [detailViewController getDictionary:dictionary];
            
            UIImageView*img=[[UIImageView alloc]init];
         
            [img sd_setImageWithURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]placeholderImage:nil];
            
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]]];
            
            UIImage *imageFromImageView = image;

            
            UIImage *screw =  [self imageByScalingImage:imageFromImageView];
            
            detailViewController.tabBarItem.image = screw;
            
            detailViewController.title = [dictionary objectForKey:@"name"];
            detailViewController.hidesBottomBarWhenPushed=YES;
            
            return detailViewController;
        }
        else if ([[dictionary objectForKey:@"type"] isEqualToString:@"rss"]) {
            PDRssListViewController *rssListViewController = [[PDRssListViewController alloc] initWithNibName:@"PDRssListViewController" bundle:nil];
            [rssListViewController getDictionary:dictionary];
            
            UIImageView*img=[[UIImageView alloc]init];
           
            [img sd_setImageWithURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]placeholderImage:nil];
            
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]]];
            
            UIImage *imageFromImageView = image;

            
            UIImage *screw =  [self imageByScalingImage:imageFromImageView];
            
            rssListViewController.tabBarItem.image = screw;
            
            rssListViewController.title = [dictionary objectForKey:@"name"];
            return rssListViewController;
        }
        else {
            if ([[dictionary objectForKey:@"type"] isEqualToString:@"fixed"] || [[dictionary objectForKey:@"type"] isEqualToString:@"location"] ) {
                if ([[dictionary objectForKey:@"sub_exists"] isEqualToString:@"yes"]) {
                    PDSubCategoryViewController *subCategoryViewController = [[PDSubCategoryViewController alloc] init];
                    [subCategoryViewController getSubId:[dictionary objectForKey:@"Id"] ofType:@"fixed" withName:[dictionary objectForKey:@"name"]];
                    
                    UIImageView*img=[[UIImageView alloc]init];
                  
                    [img sd_setImageWithURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]placeholderImage:nil];
                    
                    
                    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]]];
                    
                    UIImage *imageFromImageView = image;

                    
                    UIImage *screw =  [self imageByScalingImage:imageFromImageView];
                    
                    
                    subCategoryViewController.tabBarItem.image = screw;
                    
                    subCategoryViewController.title = [dictionary objectForKey:@"name"];
                    subCategoryViewController.hidesBottomBarWhenPushed=YES;
                    
                    
                    NSInteger lengt =  subCategoryViewController.title.length;
                    NSLog(@"subCategoryViewController lengt :%ld",(long)lengt);
                    
                    if (lengt > 8) {
                       
                        subCategoryViewController.title = [subCategoryViewController.title substringToIndex:8];
                    }
                    
                    return subCategoryViewController;
                }
                else {
                    PDListViewController *listViewController = [[PDListViewController alloc] init];
                    [listViewController getDictionary:dictionary];
                    
                    UIImageView*img=[[UIImageView alloc]init];
                  
                   [img sd_setImageWithURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]placeholderImage:nil];
                    
                    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]]];
                    
                    UIImage *imageFromImageView = image;

                    
                    UIImage *screw =  [self imageByScalingImage:imageFromImageView];
                    
                    listViewController.tabBarItem.image = screw;
                    
                    listViewController.title = [dictionary objectForKey:@"name"];
                    listViewController.hidesBottomBarWhenPushed=YES;
                    
                    NSInteger lengt =  listViewController.title.length;
                    NSLog(@"listViewController lengt :%ld",(long)lengt);
                    
                    
                    if (lengt > 8) {
                       
                        listViewController.title = [listViewController.title substringToIndex:8];
                    }
                    
                    return listViewController;
                    
                }
                
            }
            
            if ([[dictionary objectForKey:@"type"] isEqualToString:@"normal"]) {
                
                PDListViewController *listViewController = [[PDListViewController alloc] init];
                [listViewController getDictionary:dictionary];
                
                UIImageView*img=[[UIImageView alloc]init];
             
                [img sd_setImageWithURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]placeholderImage:nil];
                
                UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[dictionary objectForKey:@"footer_icon"]]]];
                
                UIImage *imageFromImageView = image;

                
                UIImage *screw =  [self imageByScalingImage:imageFromImageView];

                listViewController.tabBarItem.image = screw;
                
                listViewController.title = [dictionary objectForKey:@"name"];
                listViewController.hidesBottomBarWhenPushed=YES;
                
                return listViewController;
                
            }
        }
    }
    return 0;
}

- (UIImage*)imageByScalingImage:(UIImage*)imgVw
{
    UIImage *screw =imgVw;
    if(imgVw.size.height>32 ||imgVw.size.width>32)
    {
        screw = [UIImage imageWithData:UIImagePNGRepresentation(imgVw) scale:2];
    }
    return screw;
}

#pragma mark - Image caching

- (void) loadImageFromURL1:(NSString*)URL image:(UIImageView*)im {
    NSURL *imageURL = [NSURL URLWithString:URL];
   
    NSData *data = [FTWCache objectForKey:URL];
    if (data) {
        UIImage *image = [UIImage imageWithData:data];
        im.image = image;
    } else {
        
        im.image = [UIImage imageNamed:@"img_def"];
       
    
        NSData *data = [NSData dataWithContentsOfURL:imageURL];
        [FTWCache setObject:data forKey:URL];
        UIImage *image = [UIImage imageWithData:data];
        
        im.image = image;
        
    }
}



@end
