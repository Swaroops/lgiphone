//
//  PDLoader.h
//  Visiting Ashland
//
//  Created by swaroop on 06/06/17.
//  Copyright © 2017 iDeveloper. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICircle : UIView
@property (nonatomic) float percent;
@end
