//
//  PDLoader.m
//  Visiting Ashland
//
//  Created by swaroop on 06/06/17.
//  Copyright © 2017 iDeveloper. All rights reserved.
//

#import "PDLoader.h"

@interface UICircle ()
{
    CGFloat startAngle;
    CGFloat endAngle;
    
}

@end

@implementation UICircle



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor whiteColor];
        
        // Determine our start and stop angles for the arc (in radians)
        startAngle = M_PI * 1.5;
        endAngle = startAngle + (M_PI * 2);
        
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    // Display our percentage as a string
    
  
    CGPoint center = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    CGFloat radius = MIN(CGRectGetWidth(self.frame) / 2, CGRectGetHeight(self.frame) / 2) - 4 / 2;
    radius = radius - 4/2;
//    CGFloat startAngle = (CGFloat)(-M_PI/2);
//    CGFloat endAngle = (CGFloat)(3 * M_PI_2);
//    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:center radius:radius startAngle:startAngle endAngle:endAngle clockwise:YES];
   
    
    
    NSString* textContent = [NSString stringWithFormat:@"%.1f%%",self.percent];
    
    if(self.percent<1)
    {
    
    }
    else
    {
       textContent = [NSString stringWithFormat:@"%.0f%%",self.percent];
    
    }
        
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    
    // Create our arc, with the correct angles
    //CGPointMake(rect.size.width / 2, rect.size.height / 2)
    [bezierPath addArcWithCenter:center
                          radius:radius
                      startAngle:startAngle
                        endAngle:(endAngle - startAngle) * (self.percent / 100.0) + startAngle
                       clockwise:YES];
    
    // Set the display for the path, and stroke it
    bezierPath.lineWidth = 2;
    [[UIColor redColor] setStroke];
    [bezierPath stroke];
    
//    CAShapeLayer *layer = [CAShapeLayer layer];
//    layer.path = bezierPath.CGPath;
//    layer.strokeColor = [UIColor whiteColor].CGColor;
   // [self addSublayer:layer];
    
   // NSLog(@"bezierPath: %@", bezierPath);
   // [bezierPath fill];
    
    // Text Drawing
    CGRect textRect = CGRectMake((rect.size.width / 2.0) - 71/2.0, (rect.size.height / 2.0) - 45/2.0, 71, 45);
    [[UIColor blackColor] setFill];
    
    
    
    NSDictionary *attribute =
    [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName: @"Helvetica-Bold" size: 12], NSFontAttributeName, nil];
    [textContent drawInRect:rect withAttributes:attribute];
    
//    [textContent drawInRect: textRect withFont: [UIFont fontWithName: @"Helvetica-Bold" size: 42.5] lineBreakMode: NSLineBreakByWordWrapping alignment: NSTextAlignmentCenter];
}



@end
