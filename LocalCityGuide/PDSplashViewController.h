//
//  PDSplashViewController.h
//  LocalsGuide
//
//  Created by swaroop on 21/03/14.
//  Copyright (c) 2014 iDeveloper. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PDSplashViewController : UIViewController
{
    
}
@property (strong, nonatomic) IBOutlet AsyncImageView *splashImgView;
@property (strong, nonatomic) IBOutlet AsyncImageView *splashImagView4s;

@property (strong, nonatomic) IBOutlet NSString *splashImgStr;

- (void)initWithSplashName:(NSString*)imgStr;
@end
