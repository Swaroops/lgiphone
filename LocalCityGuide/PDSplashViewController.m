//
//  PDSplashViewController.m
//  LocalsGuide
//
//  Created by swaroop on 21/03/14.
//  Copyright (c) 2014 iDeveloper. All rights reserved.
//

#import "PDSplashViewController.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+AFNetworking.h"
#import "PDAppDelegate.h"
#import "SDImageCache.h"
#import "UIImageView+JMImageCache.h"
#import "FTWCache.h"
#import "UIImageView+AFNetworking.h"

@interface PDSplashViewController ()
{
    
}
@end

@implementation PDSplashViewController
@synthesize splashImagView4s,splashImgView,splashImgStr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
       
    }
    return self;
}

- (void)initWithSplashName:(NSString*)imgStr
{
    splashImgStr=imgStr;
    
    [self showImage];
}

- (NSString *)extractNumberFromText:(NSString *)text
{
    NSCharacterSet *nonDigitCharacterSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    return [[text componentsSeparatedByCharactersInSet:nonDigitCharacterSet] componentsJoinedByString:@""];
}

-(void)showImage
{
    
    splashImgView=[[AsyncImageView alloc]init];
    splashImagView4s=[[AsyncImageView alloc]init];
    
    NSString *key=splashImgStr;
    SDImageCache *imageCachee =[SDImageCache sharedImageCache];
    imageCachee = [imageCachee initWithNamespace:key];
    
   
    if (IS_IPHONE5){
        
        CGRect frame=[[UIScreen mainScreen] bounds];
        splashImgView.frame=frame;
       
        splashImgView.backgroundColor=[UIColor clearColor];
        
        SDImageCache *imageCache =[SDImageCache sharedImageCache];
        imageCache = [imageCache initWithNamespace:@"guideSpalsh"];
        
        UIImage *splImg=[imageCache imageFromDiskCacheForKey:splashImgStr];
        
        if (IS_IPHONE5)
            [[NSBundle mainBundle] loadNibNamed:@"PDSplashViewController" owner:self options:nil];
            
        
        
        [splashImgView setImageWithURL:[NSURL URLWithString:splashImgStr] key:splashImgStr placeholder:nil];
        
        if (!splImg) {
            
           [self etestsplashiPhone];
            
        }
        else
        {
            [self.view addSubview:splashImgView];
            splashImgView.image=splImg;
            
            
            
            int64_t delayInSeconds = 3.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                [(PDAppDelegate *)[[UIApplication sharedApplication] delegate] navigatetoInapp];
                
            });

            
            
            NSLog(@"ima :%@",splImg);
            
        }
    }
    else{
        
        [[NSBundle mainBundle] loadNibNamed:@"PDSplashViewController4" owner:self options:nil];

        SDImageCache *imageCache =[SDImageCache sharedImageCache];
        imageCache = [imageCache initWithNamespace:@"guideSpalsh"];
        
        
        CGRect frame=[[UIScreen mainScreen] bounds];
        splashImagView4s.frame=frame;
        splashImagView4s.backgroundColor=[UIColor clearColor];
        
        UIImage *splImg=[imageCache imageFromDiskCacheForKey:splashImgStr];
        
        
        
        
        [splashImagView4s setImageWithURL:[NSURL URLWithString:splashImgStr] key:splashImgStr placeholder:nil];
        
        
        
        splashImagView4s.image=[imageCache imageFromDiskCacheForKey:splashImgStr];
        if (!splashImagView4s.image) {
            
            
            [self etestsplash];
            
        }
        
        else
        {
            [self.view addSubview:splashImgView];
            
            splashImagView4s.image = splImg;
            
            
            int64_t delayInSeconds = 2.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                [(PDAppDelegate *)[[UIApplication sharedApplication] delegate] navigatetoInapp];
                
            });
            

        }
    }
    
}

-(void)etestsplash
{
    __block UIImage *img = nil;
    
    img = [[UIImage alloc] init];
    
    NSString *str = [NSString stringWithFormat:@"%@",[NSURL URLWithString:splashImgStr]];
    
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        
        NSData * data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:str]];
        img = [UIImage imageWithData: data];
        
      
        dispatch_async(dispatch_get_main_queue(), ^{
        
            [splashImagView4s setImage:img];
            
            
            {
                
         
                if(splashImagView4s.image)
                {
                    SDImageCache *imageCache =[SDImageCache sharedImageCache];
                    imageCache = [imageCache initWithNamespace:@"guideSpalsh"];
                    
                    [imageCache storeImage:splashImagView4s.image forKey:splashImgStr completion:^{  }];
                }
                
            }
            
            int64_t delayInSeconds = 2.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                [(PDAppDelegate *)[[UIApplication sharedApplication] delegate] navigatetoInapp];
                
            });
            
            
            
            
        });
    });
    
    
}

-(void)etestsplashiPhone
{
    __block UIImage *img = nil;
    
    img = [[UIImage alloc] init];
    
    NSString *str = [NSString stringWithFormat:@"%@",[NSURL URLWithString:splashImgStr]];
    
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        
        NSData * data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:str]];
        img = [UIImage imageWithData: data];
        
   
        
        [splashImgView setImageWithURL:[NSURL URLWithString:splashImgStr] key:str placeholder:nil];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
           
            [splashImgView setImage:img];
            
            
            {
                
                if(splashImgView.image)
                {
                    SDImageCache *imageCache =[SDImageCache sharedImageCache];
                    imageCache = [imageCache initWithNamespace:@"guideSpalsh"];
                    
                    [imageCache storeImage:splashImgView.image forKey:splashImgStr completion:^{  }];
                }

                
            }
            
                       int64_t delayInSeconds = 2.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                [(PDAppDelegate *)[[UIApplication sharedApplication] delegate] navigatetoInapp];
                
            });
            
            
            
            
        });
    });
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
   
    if (IS_IPHONE5){
        [[NSBundle mainBundle] loadNibNamed:@"PDSplashViewController" owner:self options:nil];
        
        
        
       
    }
    else{
        [[NSBundle mainBundle] loadNibNamed:@"PDSplashViewController4" owner:self options:nil];
        
    }
    
}

-(void)showMyStatus : (NSNotification *)aNotification{
  
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

@end
