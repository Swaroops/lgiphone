//
//  PDTesterView.h
//  Visiting Ashland
//
//  Created by swaroop on 20/07/17.
//  Copyright © 2017 iDeveloper. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDTesterView : UIView
@property (nonatomic) float percent;
@end
