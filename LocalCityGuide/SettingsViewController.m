//
//  SettingsViewController.m
//  Visiting Ashland
//
//  Created by Anu on 28/04/16.
//  Copyright © 2016 iDeveloper. All rights reserved.
//

#import "SettingsViewController.h"
#import "CustomIOSAlertView.h"
#import "DBOperations.h"
#import <QuartzCore/QuartzCore.h>
#import <mach/mach.h>
#import <mach/mach_host.h>
#import "PDAppDelegate.h"
#import "SettingsInnerViewController.h"
#import "UIImageView+JMImageCache.h"
#import "UIImageView+AFNetworking.h"
#import "sTableViewController.h"

#import "ACPDownloadView.h"
#import "ACPStaticImagesAlternative.h"
#import "ACPIndeterminateGoogleLayer.h"
#import "SSZipArchive.h"
//#import "PDLoader.h"
#import "Constants.h"
#import "SDImageCache.h"
#import "PDTesterView.h"
#import "Reachability.h"




@interface SettingsViewController ()<CustomIOSAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,NSURLSessionDataDelegate, NSURLSessionDelegate, NSURLSessionTaskDelegate>
{
    CustomIOSAlertView *customAlert;
    UIRefreshControl *refreshControl;
    
    NSString *cachePath;
     NSString *cachePath1;
     NSString *cachePath2;
     NSString *cachePath3;
     NSString *cachePath4;
     NSString *cachePath5;
     NSString *cachePath6;
    
    UIView *loaderFI;
    
    UITableView *tblStorage;
    NSMutableArray *muttarr;
    NSMutableArray *muttarrSize;
    
    NSMutableArray* reversedArray;
    
    NSMutableArray *arrayForDetails;
    
    NSMutableArray* myMutableArrayAgain;
    
    UIView *viewInAlert;
    UIButton *btnBackStorage;
    UIButton *reloadBtn;
    
    UIButton *btnCancel;
    UILabel *lblPercen;
    
    PDTesterView* m_testView;
    
    UIView *container;
    NSURLSessionDataTask *dataTaske;
    
    BOOL boolOnce;
}
@property (weak, nonatomic) IBOutlet ACPDownloadView *downloadView;
@property (nonatomic, retain) NSMutableData *dataToDownload;
@property (nonatomic) float downloadSize;
@end

@implementation SettingsViewController
NSArray *arraystatic;
- (void)viewDidLoad {
   
    
    
    
if([[DBOperations getMainAppid] isEqualToString:@"266"])
{
    if ([(NSString*)[UIDevice currentDevice].model hasPrefix:@"iPad"] )
    {
        
        [[NSBundle mainBundle] loadNibNamed:@"CRSettingsViewController4" owner:self options:nil];
    }
    else if(IS_IPHONE_4_OR_LESS)
    {
        [[NSBundle mainBundle] loadNibNamed:@"CRSettingsViewController4" owner:self options:nil];
    }
    else
    {
        [[NSBundle mainBundle] loadNibNamed:@"CRSettingsViewController" owner:self options:nil];

    }
    
}
   
else
    {
    if ([(NSString*)[UIDevice currentDevice].model hasPrefix:@"iPad"] )
    {
        
        [[NSBundle mainBundle] loadNibNamed:@"SettingsViewController4" owner:self options:nil];
    }
    else if(IS_IPHONE_4_OR_LESS)
    {
        [[NSBundle mainBundle] loadNibNamed:@"SettingsViewController4" owner:self options:nil];
    }

}

    [super viewDidLoad];
    
    
  NSString  *distance=[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"];
    if ([distance isEqualToString:@"M"]) {
        distUnitSegment.selectedSegmentIndex=1;
        
    }
    else
        distUnitSegment.selectedSegmentIndex=0;
    
    NSString *nearby= [[NSUserDefaults standardUserDefaults]objectForKey:@"nearby"];
    
    if([nearby isEqualToString:@"on"])
    {
        nearBySwitch.on=YES;
    }
    else
    {
        nearBySwitch.on=NO;
      
    }
    
    NSLog(@"HOME > %@", NSHomeDirectory());
    
    
  
   

    int f=0;
    if(IS_IPHONE5 ||IS_IPHONE_4_OR_LESS)
    {
        f=80;
    }
    else
    {
        f=50;
    }
    
    if ([(NSString*)[UIDevice currentDevice].model hasPrefix:@"iPad"] )
    {
        
        NSLog(@"IPADD");
        f=20;
        
        
        

    }
    

    versionLbl.text=[NSString stringWithFormat:@"v.%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    versionLbl.textAlignment=NSTextAlignmentCenter;
    
    
    
    
    if(_segmentView.tag==13)
    {
     [_segmentView setDividerImage:[self imageWithColor:[UIColor clearColor]] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
     
     self.segmentView.layer.cornerRadius = 15.0;
     self.segmentView.layer.borderColor = [UIColor colorWithRed:(242/255.0) green:(155/255.0) blue:(20/255.0) alpha:1.0].CGColor;
     self.segmentView.layer.borderWidth = 2.0f;
     self.segmentView.layer.masksToBounds = YES;
     
     UIFont *font = [UIFont boldSystemFontOfSize:14.0f];
     NSDictionary *attributes = [NSDictionary dictionaryWithObject:font
     forKey:NSFontAttributeName];
     [_segmentView setTitleTextAttributes:attributes
     forState:UIControlStateNormal];
     
     [_segmentView setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor]} forState:UIControlStateNormal];
     
     [_segmentView setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]} forState:UIControlStateSelected];
     
    
     
     float width = 0.0;
     if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
     {
     if(IS_IPHONE5)
     {
     NSLog(@"is iphone 5");
     }
     
     width=(([UIScreen mainScreen].bounds.size.width)/4);
     NSLog(@"float width :%f",width);
     
     
   
     NSLog(@"segmentView frame :%@",NSStringFromCGRect(_segmentView.frame));

     [_segmentView setWidth:55 forSegmentAtIndex:4];

     
     _segmentView.apportionsSegmentWidthsByContent=YES;
     
  
     
     self.segmentView.layer.borderWidth = 1.0f;
     self.segmentView.layer.cornerRadius = 10.0;
     
     NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
     [UIFont boldSystemFontOfSize:10], NSFontAttributeName,
     [UIColor whiteColor], NSForegroundColorAttributeName, nil];
     
    
     }
    
    }
    
    NSInteger x=0;
    x =  [[NSUserDefaults standardUserDefaults]integerForKey:@"numberofAppsToShow"];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSObject * object = [prefs objectForKey:@"numberofAppsToShow"];
    
    
    
        if(object != nil){
           
            
            NSString * trimed = [[NSString stringWithFormat:@"%ld",(long)x] substringToIndex:1];
            
            int value = [trimed intValue]-1;
            
           [_segmentView setSelectedSegmentIndex:value];
        }

    NSString *str = [[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"];
    if([str isEqualToString:@"K"])
    {
    lblSlider.text=@" 0.5km     1km     2km     3km     4km     5km    6km     7km     8km     9km     10km";
    }
    else
    {
    lblSlider.text=@" 0.5mi      1mi      2mi      3mi      4mi      5mi     6mi      7mi      8mi      9mi      10mi";
    }
    
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
   myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        myMutableArrayAgain = [plistDict objectForKey:@"downloadsArray"];
    }
    
    


    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectorhere) name:@"buttonPressed" object:nil];
}
-(void)viewWillAppear:(BOOL)animated
{
    [loaderFI removeFromSuperview];
    
    NSString *str = [[NSUserDefaults standardUserDefaults]objectForKey:@"nearby"];
    if([str isEqualToString:@"on"])
    {
      [geoFeedSwitch setOn:NO];
    }
    else
    {
        [geoFeedSwitch setOn:YES];
    }
}


- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


-(NSString *)sizeOfFile:(NSString *)filePath
{

    NSString *cacheSize;
    

    
    return cacheSize;

    

}
-(NSString *)sizeOfFiless:(NSString *)filePath
{

    
    NSString *fileSizeStr;
   
    NSString *cacheSize;
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *URL = [documentsDirectory stringByAppendingPathComponent:@"XML/Extras/Approval.xml"];
    
    NSLog(@"URL:%@",URL);
    NSError *attributesError = nil;
    NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:URL error:&attributesError];
    
 
    
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *mainPath    = [myPathList  objectAtIndex:0];
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSArray *fileArray = [fileMgr contentsOfDirectoryAtPath:mainPath error:nil];
    
    for (NSString *filename in fileArray)
    {
        
        
        fileSizeStr = [NSByteCountFormatter stringFromByteCount:filename countStyle:NSByteCountFormatterCountStyleFile];

        
    }

    
    NSInteger si;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *bundleURL = [[NSBundle mainBundle] bundleURL];
    NSArray *contents = [fileManager contentsOfDirectoryAtURL:bundleURL
                                   includingPropertiesForKeys:@[]
                                                      options:NSDirectoryEnumerationSkipsHiddenFiles
                                                        error:nil];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"pathExtension == 'png'"];
    for (NSURL *fileURL in [contents filteredArrayUsingPredicate:predicate]) {
        
         si = [NSByteCountFormatter stringFromByteCount:fileURL countStyle:NSByteCountFormatterCountStyleFile];
    }
    
    NSLog(@"si :%ld",(long)si);
    
    
    
    
    
    //get full pathname of bundle directory
    NSString *bundlePath = [[NSBundle mainBundle] bundlePath];
    
    //get paths of all of the contained subdirectories
    NSArray *bundleArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:bundlePath error:nil];
    
    //to access each object in array
    NSEnumerator *filesEnumerator = [bundleArray objectEnumerator];
    
    NSString *fileName;
    
    unsigned long long int fileSize = 0;
    NSError *error = nil;
    
    //return next object from enumerator
    while (fileName = [filesEnumerator nextObject])
    {
        NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:[bundlePath stringByAppendingPathComponent:fileName] error:&error];
        
        fileSize += [fileDictionary fileSize];
    }
    
    NSString *folderSizeStr = [NSByteCountFormatter stringFromByteCount:fileSize countStyle:NSByteCountFormatterCountStyleMemory];
    NSLog(@"App size (bundle size): %@ \n\n\n",folderSizeStr);
    
    
     NSInteger big = [NSByteCountFormatter stringFromByteCount:bundleURL countStyle:NSByteCountFormatterCountStyleFile];
    NSLog(@"big :%ld",(long)big);
    
    
    
    
    
    
    
    
    
    NSString *string;
    
    NSArray* tmpDirectory = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:NSTemporaryDirectory() error:NULL];
    for (NSString *file in tmpDirectory) {

        
        string = [NSByteCountFormatter stringFromByteCount:file countStyle:NSByteCountFormatterCountStyleFile];
    }

    NSLog(@"string :%@",string);
    
    
   
    
    NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:NSTemporaryDirectory() error: &error];
    NSNumber *size = [fileDictionary objectForKey:NSFileSize];
    
    NSLog(@"size :%@",size);
    
    NSInteger a= [fileSizeStr integerValue]-[size integerValue];
    a=a+13;
    NSLog(@"a:%ld",(long)a);
    fileSizeStr=[NSString stringWithFormat:@"%ld Mb",(long)a];
    
    
    NSError *errorr = nil;
    NSDictionary *attribs = [[NSFileManager defaultManager] attributesOfItemAtPath:documentsDirectory error:&errorr];
    if (attribs) {
        NSString *string = [NSByteCountFormatter stringFromByteCount:[attribs fileSize] countStyle:NSByteCountFormatterCountStyleFile];
        NSLog(@"string:%@", string);
    }
    
    
    
    NSString *folderPath;
    
    NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:mainPath error:&error];
    for (NSString *strName in dirContents) {
        
        NSString *shit = [NSByteCountFormatter stringFromByteCount:dirContents countStyle:NSByteCountFormatterCountStyleFile];
        NSLog(@"shit:%@", shit);

        
    }
    
    
    

    
    NSLog(@"HOME > %@", NSHomeDirectory());
    
    
    NSFileManager *fileManagerr = [NSFileManager defaultManager];
    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
 
    {
        
        
      
        
        NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:cachePath error:&error];
        for (NSString *strName in dirContents) {
            
        
            {
            NSString *shit = [NSByteCountFormatter stringFromByteCount:dirContents countStyle:NSByteCountFormatterCountStyleFile];
            NSLog(@"shit:%@", shit);
                
           
                
                cachePath = [NSString stringWithFormat:@"%@/com.hackemist.SDWebImageCache.Splash",cachePath];
                
               
                NSLog(@"cachePath :%@",cachePath);
                
            cacheSize =  [self sizeOfFolder:cachePath];
            
                NSLog(@"cacheSize :%@",cacheSize);
            }
            
        }
        

        
       
    }
    
    return cacheSize;
    
   
    
}

-(NSString *)sizeOfFolder:(NSString *)folderPath
{
    NSArray *contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:folderPath error:nil];
    NSEnumerator *contentsEnumurator = [contents objectEnumerator];
    
    NSString *file;
    unsigned long long int folderSize = 0;
    
    while (file = [contentsEnumurator nextObject]) {
        NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:[folderPath stringByAppendingPathComponent:file] error:nil];
        folderSize += [[fileAttributes objectForKey:NSFileSize] intValue];
    }
    
    //This line will give you formatted size from bytes ....
    NSString *folderSizeStr = [NSByteCountFormatter stringFromByteCount:folderSize countStyle:NSByteCountFormatterCountStyleFile];
    
    NSLog(@"folderSizeStr folder :%@",folderSizeStr);
    
    return folderSizeStr;
}

- (long long)getFreeSpace
{
    long long freeSpace = 0.0f;
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary) {
        NSNumber *fileSystemFreeSizeInBytes = [dictionary objectForKey: NSFileSystemFreeSize];
        freeSpace = [fileSystemFreeSizeInBytes longLongValue];
    } else {
        //Handle error
    }
    
    NSLog(@"freeSpace : %lld",freeSpace);
    return freeSpace;
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}
- (void)testRefresh:(UIRefreshControl *)refreshControlH
{
    
    [refreshControl endRefreshing];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}


-(void) classificaionAccessComplete:(NSArray*)resultArray
{
    NSArray *sortedArray=[[NSArray alloc]init];
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    sortedArray=[resultArray sortedArrayUsingDescriptors:@[sort]];
    
    classIdArr=[[NSMutableArray alloc]initWithArray:sortedArray];
    
    
    [self showPopUp];
}

-(void) classificaionAccessCompleteSecond:(NSArray*)resultArray
{
    NSArray *sortedArray=[[NSArray alloc]init];
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    sortedArray=[resultArray sortedArrayUsingDescriptors:@[sort]];
    
    classIdArr=[[NSMutableArray alloc]initWithArray:sortedArray];
    
   
    [guideSelectTable reloadData];
}
-(void)showPopUp
{
    customAlert = [[CustomIOSAlertView alloc] init];
    
    float popUpWidth=self.view.frame.size.width-(self.view.frame.size.width*10/100);
    float popUpHeight=self.view.frame.size.height-(self.view.frame.size.height*40/100);
    
  
    
    
    
    
    
    
    viewInAlert.layer.cornerRadius = 7;
    viewInAlert.clipsToBounds = YES;
    
    
    
    
    [customAlert setContainerView:viewInAlert];
    viewInAlert.frame=CGRectMake(0, distUnitSegment.frame.origin.y, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    
    [customAlert setDelegate:self];
    
    customAlert.buttonTitles=[[NSArray alloc]initWithObjects:@"OK",nil];
    

    CATransition *transition = [CATransition animation];
    transition.duration = 1.0;
    transition.type = kCATransitionFromLeft; //choose your animation
    [viewInAlert.layer addAnimation:transition forKey:nil];
    [self.view addSubview:viewInAlert];
    
    btnBackStorage = [[UIButton alloc]initWithFrame:bckBtn.frame];
    
    if ([(NSString*)[UIDevice currentDevice].model hasPrefix:@"iPad"] )
    {
        
        CGRect frame;
        frame=CGRectZero;
        frame=btnBackStorage.frame;
        frame.origin.y=3;
        
        btnBackStorage.frame=frame;
        
    }
    
    
    btnBackStorage.backgroundColor=[UIColor clearColor];
    [btnBackStorage addTarget:self
                       action:@selector(myaction)
             forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBackStorage];
    
    
    
 
  
    
    [customAlert setUseMotionEffects:true];
    
  
    
    
   
    previos=-1;
    [guideSelectTable flashScrollIndicators];
    
    
    
  
    
    guideSelectTable=[[UITableView alloc]initWithFrame:viewInAlert.frame style:UITableViewStylePlain];
  
    
    guideSelectTable.frame=CGRectMake(0, bckBtn.frame.origin.y+bckBtn.frame.size.height+3, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-(bckBtn.frame.origin.y+bckBtn.frame.size.height+3));
    
    [self.view addSubview:guideSelectTable];
    
    
   
    guideSelectTable.dataSource=self;
    guideSelectTable.delegate=self;

    
    
    moreUpArrow=[[UIImageView alloc]init];
    moreUpArrow.image=[UIImage imageNamed:@"arrow-up.png"];
    [viewInAlert addSubview:moreUpArrow];
    moreUpArrow.hidden=YES;
      moreUpArrow.frame=CGRectMake((viewInAlert.frame.size.width/2)-10, 0, 20, 12.25);
    
    moreDownArrow=[[UIImageView alloc]init];
    moreDownArrow.image=[UIImage imageNamed:@"arrow-down.png"];
    [viewInAlert addSubview:moreDownArrow];
    moreDownArrow.hidden=YES;
    moreDownArrow.frame=CGRectMake((viewInAlert.frame.size.width/2)-10, viewInAlert.frame.size.height-15, 20, 12.25);
    
    
    
    [guideSelectTable reloadData];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(testRefresh:) forControlEvents:UIControlEventValueChanged];
    [guideSelectTable addSubview:refreshControl];
    

}
- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
       if(customAlert.tag==999)
       {
         [alertView close];
       }
       else{
           
           [alertView close];
     [self dismissViewControllerAnimated:NO completion:nil];
       }
    
}
#pragma mark - UITableView delegate and datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag==911)
    {
        return 60;
    }
    return 55;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView.tag==911)
    {
        return 1;
    }

    else
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag==911)
    {
        if(!([muttarr count]==0))
        return [muttarr count];
    }
    
    return [classIdArr count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{

    
    if(section==0)
        
    {
        if(tableView.tag==911)
        {
           
            
        UILabel *lblBelowBanner=[[UILabel alloc]initWithFrame:CGRectMake(0,0, tableView.bounds.size.width, 30)];
        lblBelowBanner.backgroundColor= Rgb2UIColor(241, 241, 241);
        lblBelowBanner.text=@"  Offline Guides";
        lblBelowBanner.textAlignment=NSTextAlignmentLeft;
        lblBelowBanner.textColor=[UIColor darkGrayColor];
        lblBelowBanner.font=[UIFont systemFontOfSize:12];
        return lblBelowBanner;
        
        }
        else
        {
            UILabel *lblBelowBanner=[[UILabel alloc]initWithFrame:CGRectMake(0,0, tableView.bounds.size.width, 30)];
            lblBelowBanner.backgroundColor=Rgb2UIColor(241, 241, 241);
            lblBelowBanner.text=@"  Guide Type";
            lblBelowBanner.textAlignment=NSTextAlignmentLeft;
            lblBelowBanner.textColor=[UIColor darkGrayColor];
            lblBelowBanner.font=[UIFont systemFontOfSize:12];
            return lblBelowBanner;
        
        }
        
    }
    
    else{
        return nil;
        
    }
    

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section==0)
    {
        return 30;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"celll"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
    }

    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if(tableView.tag==911)
    {

        
        if([muttarr count] > 0)
        {
            
        NSDictionary *dict=[muttarr objectAtIndex:indexPath.row];
        
       
        
        UIImageView *cellimage = [[UIImageView alloc]init];
        cellimage.frame=CGRectMake(5, 5, 50, 50);
       
        
        NSString *inte = [self extractNumberFromText:[dict objectForKey:@"image"]];
        
        [cellimage setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"image"]] key:inte placeholder:nil];
        
        NSString *imageCacheFolder=@"JMCache";
        SDImageCache *imageCache =[SDImageCache sharedImageCache];
        imageCache = [imageCache initWithNamespace:imageCacheFolder];
        
        cellimage.image=[imageCache imageFromDiskCacheForKey:inte];
        
        [cell addSubview:cellimage];
        
        UILabel *titlelabelh = [[UILabel alloc] init];
        titlelabelh.frame = CGRectMake(cellimage.frame.origin.x+60, 10, 250 , 55);
        titlelabelh.numberOfLines = 0;
        [titlelabelh setFont:[UIFont systemFontOfSize:16]];
        titlelabelh.textColor = [UIColor darkGrayColor];
        titlelabelh.backgroundColor = [UIColor clearColor];
        titlelabelh.text =[dict objectForKey:@"app_name"];
        [titlelabelh sizeToFit];
        
       float i = titlelabelh.intrinsicContentSize.width;
        NSLog(@"titlelabelh width :%f",i);
         if(i>140)
         {
             CGRect widthTrunc;
             widthTrunc=CGRectZero;
             widthTrunc=titlelabelh.frame;
             widthTrunc.size.width=139;
             titlelabelh.frame=widthTrunc;
         }
        [cell addSubview:titlelabelh];
        
        UILabel *sizeLbl = [[UILabel alloc] init];
        sizeLbl.frame = CGRectMake(titlelabelh.frame.origin.x+5, titlelabelh.frame.origin.y+titlelabelh.frame.size.height+2, 60 , 15);
        sizeLbl.numberOfLines = 0;
        [sizeLbl setFont:[UIFont systemFontOfSize:12]];
        sizeLbl.textColor = [UIColor darkGrayColor];
        sizeLbl.backgroundColor = [UIColor clearColor];
        sizeLbl.text = [[myMutableArrayAgain objectAtIndex:indexPath.row]valueForKey:@"zip_file_size"];//[muttarrSize objectAtIndex:indexPath.row];//reversedArray
        [sizeLbl sizeToFit];
        [cell addSubview:sizeLbl];
        
        UIButton *btnDel = [[UIButton alloc]init];
        btnDel.frame=CGRectMake(tblStorage.frame.size.width-80, titlelabelh.frame.origin.y+5, 60, 25);
        [btnDel setTitle:@"Delete" forState:UIControlStateNormal];
        [btnDel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnDel.titleLabel.font= [UIFont systemFontOfSize:12];
        [btnDel setBackgroundColor:[UIColor colorWithRed:(194/255.0) green:(0/255.0) blue:(10/255.0) alpha:1.0]];
        btnDel.layer.cornerRadius=5.0;
        [btnDel addTarget:self action:@selector(deleteBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        btnDel.tag = indexPath.row;
        
        [cell addSubview:btnDel];
        
        
        reloadBtn = [[UIButton alloc]init];
        reloadBtn.frame=CGRectMake(tblStorage.frame.size.width-(70+45), titlelabelh.frame.origin.y+5, 30, 30);
        
        [reloadBtn setBackgroundColor:[UIColor clearColor]];
        [reloadBtn setBackgroundImage:[UIImage imageNamed:@"reloadBttn.png"] forState:UIControlStateNormal];
        reloadBtn.layer.cornerRadius=5.0;
        reloadBtn.tag = indexPath.row;
        [reloadBtn addTarget:self action:@selector(reloadd:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [cell addSubview:reloadBtn];
        
        }
        return cell;
        
    }
    
    NSDictionary *dict=[classIdArr objectAtIndex:indexPath.row];
    
    UILabel *titlelabelh = [[UILabel alloc] init];
    titlelabelh.frame = CGRectMake(40, 2, 250 , 55);
    titlelabelh.numberOfLines = 2;
    [titlelabelh setFont:[UIFont fontWithName:@"Helvetica" size:16]];
    titlelabelh.textColor = [UIColor darkGrayColor];
    titlelabelh.backgroundColor = [UIColor clearColor];
    titlelabelh.text =[dict objectForKey:@"name"];
   
   
    NSLog(@"%ld",(long)indexPath.row);
    if ([classIdArr count]>6) {
          moreUpArrow.hidden=NO;
        moreDownArrow.hidden=NO;
  
        if(indexPath.row>=([classIdArr count]-1)||(indexPath.row==8))
        { moreDownArrow.hidden=YES;
            
        }
        if(indexPath.row==0||indexPath.row==6)
        {
            moreUpArrow.hidden=YES;
        }
    }
    
    [cell addSubview:titlelabelh];
    
    
    UISwitch *onoff = [[UISwitch alloc] initWithFrame: CGRectMake(guideSelectTable.frame.size.width-66, 12, 51, 31)];
    [onoff addTarget: self action: @selector(flip:) forControlEvents:UIControlEventValueChanged];
     onoff.tag=indexPath.row;
    if([[dict objectForKey:@"status"] isEqualToString:@"0"])//@"1"
    {
        onoff.on=YES;
    }
    else
         onoff.on=NO;
    
    
    [cell addSubview: onoff];
    
    
    
    return cell;
}
-(IBAction)segmentValueChanged:(id)sender
{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"TestNotification"
     object:self];

    
    if (distUnitSegment.selectedSegmentIndex == 0) {
        [[NSUserDefaults standardUserDefaults]setObject:@"K" forKey:@"showDistanceIn"];
        lblSlider.text=@" 0.5km     1km     2km     3km     4km     5km    6km     7km     8km     9km     10km";
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"M" forKey:@"showDistanceIn"];
        lblSlider.text=@" 0.5mi      1mi      2mi      3mi      4mi      5mi     6mi      7mi      8mi      9mi      10mi";
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }
}

-(IBAction)segmentViewValueChangedNumber:(id)sender
{
    if(_segmentView.selectedSegmentIndex==0)
    {
        [[NSUserDefaults standardUserDefaults]setInteger:10 forKey:@"numberofAppsToShow"];
    }
    else if(_segmentView.selectedSegmentIndex==1)
    {
        [[NSUserDefaults standardUserDefaults]setInteger:20 forKey:@"numberofAppsToShow"];
    }
    else if(_segmentView.selectedSegmentIndex==2)
    {
        [[NSUserDefaults standardUserDefaults]setInteger:30 forKey:@"numberofAppsToShow"];
    }
    else if(_segmentView.selectedSegmentIndex==3)
    {
        [[NSUserDefaults standardUserDefaults]setInteger:40 forKey:@"numberofAppsToShow"];
    }
    else if(_segmentView.selectedSegmentIndex==4)
    {
        [[NSUserDefaults standardUserDefaults]setInteger:50 forKey:@"numberofAppsToShow"];
    }
}
-(IBAction)nearyByValueChanged:(id)sender
{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"TestNotification"
     object:self];

    
    DBOperations *obj=[[DBOperations alloc]init];
    obj.delegate=self;
    [obj readGuideClassification:@"first"];
    
    
   
}

- (IBAction)geoFeedValeChanged:(id)sender {
    
    
    
    if ([geoFeedSwitch isOn]) {
      
        
        [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:@"nearby"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [nearBySwitch setOn:NO];
        [nearBySwitch sendActionsForControlEvents:UIControlEventValueChanged];
    }
    else
    {
        [nearBySwitch setOn:YES];
        [nearBySwitch sendActionsForControlEvents:UIControlEventValueChanged];
        
      
        
        [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"nearby"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
    }

    
}

-(void)flip:(id)sender
{
   
    
    UISwitch *tempSwitch=(UISwitch*)sender;
    NSString *switchTo;
    if (!tempSwitch.on==YES)
        switchTo=@"1";
    else
        switchTo=@"0";
        
    
    
    int tag= (int)((UISwitch*)sender).tag;
    
    NSDictionary *obj=[classIdArr objectAtIndex:tag];
    DBOperations *dbObj=[[DBOperations alloc]init];
    dbObj.delegate=self;
    [dbObj updateClassificationId:obj onOf:switchTo];
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"TestNotification"
     object:self];
    
    
}
-(void)processComplete
{
    DBOperations *obj=[[DBOperations alloc]init];
    obj.delegate=self;
    [obj readGuideClassification:@"second"];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView.tag==911)
    {
       
        NSString *stringForId;
        if(!([muttarr count]==0))
        {
            stringForId = [[muttarr objectAtIndex:indexPath.row]valueForKey:@"id"];
        }
        
        [[NSUserDefaults standardUserDefaults]setObject:stringForId forKey:@"stringForIdfromSettings"];
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"showVCfromSettings" object:self];
        
    }
}


-(IBAction)back:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTableView" object:self];
    
  
    [self dismissViewControllerAnimated:NO completion:nil];
  
}

+ (void)clearTmpDirectory
{
    NSArray* tmpDirectory = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:NSTemporaryDirectory() error:NULL];
    for (NSString *file in tmpDirectory) {
        [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), file] error:NULL];
    }
}
- (IBAction)btnClearCache:(id)sender
{
    [deleteBtn setTitle:@"Deleting" forState:UIControlStateNormal];
    
    
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *mainPath    = [myPathList  objectAtIndex:0];
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSArray *fileArray = [fileMgr contentsOfDirectoryAtPath:mainPath error:nil];
    for (NSString *filename in fileArray)  {
        [fileMgr removeItemAtPath:[mainPath stringByAppendingPathComponent:filename] error:NULL];
    }

    
    NSLog(@"cachePath :%@",cachePath);
    
    cacheLabel.text = [NSString stringWithFormat:@"Disk Cache : %@",[self sizeOfFolder:cachePath]];
    
    NSString *strg = [self sizeOfFile:nil];
    if (strg == (id)[NSNull null] || strg.length == 0 )
        {
          cacheLabel.text =[NSString stringWithFormat:@"Disk Cache : 0.0 Mb"];
        }
    else{
        
        cacheLabel.text =[NSString stringWithFormat:@"Disk Cache : %@", [self sizeOfFolder:cachePath]];
    
    }
    
    [customAlert close];
}

-(BOOL)reachableSettings {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}

- (IBAction)manageStorage:(id)sender {
    
    
    if([self reachableSettings])
    {
        
    [deleteBtn setTitle:@"Delete" forState:UIControlStateNormal];
    
    muttarr = [[NSMutableArray alloc]init];
    muttarrSize = [[NSMutableArray alloc]init];
    reversedArray = [[NSMutableArray alloc]init];
    arrayCacheP = [[NSMutableArray alloc]init];

    
    NSMutableDictionary *plistDict;
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSMutableArray *arrFavorites;
    
    {
        
        if([fileManager fileExistsAtPath:finalPath])
        {
            plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
            arrFavorites = [plistDict objectForKey:@"downloadsArray"];
            
            
            
            
            if([fileManager fileExistsAtPath:finalPath])
            {
                NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
                myMutableArrayAgain = [plistDict objectForKey:@"downloadsArray"];
            }
            
        }
    }
    
    for(int i=0;i<[myMutableArrayAgain count];i++)
    {
   
    
        
        
        NSString *string = [NSString stringWithFormat:@"totalSize%@",[[myMutableArrayAgain objectAtIndex:i]valueForKey:@"id"]];
        
        NSLog(@"string :%@",string);
        
    NSString *sizeOfApp = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"totalSize%@",[[myMutableArrayAgain objectAtIndex:i]valueForKey:@"id"]]];
    
        NSLog(@"sizeOfApp :%@",sizeOfApp);
        
        if(!(sizeOfApp==nil))
        {
     
        }
        else
        {
          
        }
    }

    
    
    [self popupManageStorage];
    
    }
    
}

-(NSString*)sizeOFUrl:(NSString*)appid
{
    NSString *string =[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",appid]]];
    
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURL *urlforData = [NSURL URLWithString:[string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithURL: urlforData];
    
    [dataTask resume];
    
    return string;
}
-(NSString*)sizeOFDownloadsFolderR:(NSString*)appid
{
   
    
    NSMutableArray *arrayCachePp;
    NSString *cachePathp;
    
    
    NSString* strCachePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    
    {
        
        
        NSError *error;
        NSMutableArray *arrayCachePathsp = [[NSMutableArray alloc] init];
        arrayCachePp = [[NSMutableArray alloc]init];
        
        int s =0;
        NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:strCachePath error:&error];
        for (NSString *strName in dirContents) {
            
           
            
            NSString *string =[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",appid]]];
            
            {
                NSArray* foo = [string componentsSeparatedByString:@".zip"];
                
                NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
                
                
                
                
                NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];
                
                NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];
                
                NSLog(@"firstBit folder :%@",firstBit);
                NSLog(@"secondBit folder :%@",secondBit2);
                
                
                
                
                
                if ([strName containsString:secondBit2]) {
                    
                    
                    NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
                    documentsURL = [documentsURL URLByAppendingPathComponent:secondBit2];
                    
                    
                    cachePathp = [NSString stringWithFormat:@"%@",documentsURL];
                    NSLog(@"cachePath :%@",cachePathp);
                    
                    
                    
                    
                    [arrayCachePathsp insertObject:cachePathp atIndex:s];
                    
                    NSLog(@"%@", arrayCachePathsp);
                    
                    NSString *str = [self sizeOfFolder:cachePathp];
                    
                    
                    
                    NSString *code = [str substringFromIndex: [str length] - 2];
                    NSLog(@"code :%@",code);
                    
                    if([code isEqualToString:@"MB"]||[code isEqualToString:@"GB"])
                    {
                        [arrayCachePp insertObject:str atIndex:s];
                    }
                    else
                    {
                        [arrayCachePp insertObject:@"0.1" atIndex:s];
                    }
                    s++;
                    
                }
                
            }
        }
    }
    
    
    NSLog(@"arrayCacheP :%@",arrayCachePp);
    
    
    
    double sum = 0;
    for (NSNumber * n in arrayCachePp) {
        sum += [n doubleValue];
    }
    
    NSLog(@"sum :%f",sum);
    
    
    NSString * sizeStr =[NSString stringWithFormat:@"%.2f",sum];
    
    NSString *size = [NSByteCountFormatter stringFromByteCount:sizeStr countStyle:NSByteCountFormatterCountStyleFile];
    
    
    
    return sizeStr;
    
    
}
-(NSString*)sizeOfAppFolder:(NSString*)appid
{
   
    NSLog(@"HOME > %@", NSHomeDirectory());
        
        NSMutableArray *arrayCacheP;
        NSString *cachePath;
        
    
        NSString* strCachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
        {
            
            
            NSError *error;
            NSMutableArray *arrayCachePaths = [[NSMutableArray alloc] init];
            arrayCacheP = [[NSMutableArray alloc]init];
            
            int s =0;
            NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:strCachePath error:&error];
            for (NSString *strName in dirContents) {
                
               
                {
                    if ([strName containsString:[NSString stringWithFormat:@"MyFolder%@",appid]]) {
                        
                        
                        cachePath = [NSString stringWithFormat:@"%@/%@/com.hackemist.SDWebImageCache.MyFolder%@",strCachePath,strName,appid];
                        NSLog(@"cachePath :%@",cachePath);
                        
                        
                        
                        
                        [arrayCachePaths insertObject:cachePath atIndex:s];
                       
                        NSLog(@"%@", arrayCachePaths);
                        
                        NSString *str = [self sizeOfFolder:cachePath];
                        
                        NSString *code = [str substringFromIndex: [str length] - 2];
                        NSLog(@"code :%@",code);
                        
                        if([code isEqualToString:@"MB"]||[code isEqualToString:@"GB"])
                        {
                            [arrayCacheP insertObject:str atIndex:s];
                        }
                        else
                        {
                            [arrayCacheP insertObject:str atIndex:s];
                        }
                        s++;
                        
                    }
                    
                }
            }
        }
        
        
        NSLog(@"arrayCachePsettings :%@",arrayCacheP);
        
        
        
        double sum = 0;
        for (NSNumber * n in arrayCacheP) {
            sum += [n doubleValue];
        }
        
        NSLog(@"sum :%f",sum);
        
    
        NSString * sizeStr =[NSString stringWithFormat:@"%.0f",sum];
        
        NSString *size = [NSByteCountFormatter stringFromByteCount:sizeStr countStyle:NSByteCountFormatterCountStyleFile];
        
        
        
        return sizeStr;
        

}

-(void)popupManageStorage
{

    
        customAlert = [[CustomIOSAlertView alloc] init];
        
        float popUpWidth=self.view.frame.size.width-(self.view.frame.size.width*10/100);
        float popUpHeight=self.view.frame.size.height-(self.view.frame.size.height*40/100);
        
       viewInAlert = [[UIView alloc] initWithFrame:CGRectMake((self.view.frame.size.width-popUpWidth)/2,(self.view.frame.size.height-popUpHeight)/2 ,popUpWidth,popUpHeight)];
        
    
        
        
    
        viewInAlert.layer.cornerRadius = 7;
        viewInAlert.clipsToBounds = YES;
        
        
        
        
        [customAlert setContainerView:viewInAlert];
        viewInAlert.frame=CGRectMake(0, distUnitSegment.frame.origin.y, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
        
        [customAlert setDelegate:self];
        
        customAlert.buttonTitles=[[NSArray alloc]initWithObjects:@"OK",nil];
        
        
    
        
        [customAlert setUseMotionEffects:true];
    
    CGRect frame;
    frame = CGRectZero;
    frame= deleteBtn.frame;
    frame.origin.x= (viewInAlert.frame.size.width/2)-(deleteBtn.frame.size.width/2);
    frame.origin.y= (viewInAlert.frame.size.height/2)-(deleteBtn.frame.size.height/2);
    deleteBtn.frame=frame;
    
    
    CGRect frame2;
    frame2 = CGRectZero;
    frame2= cacheLabel.frame;
    frame2.origin.x= (viewInAlert.frame.size.width/2)-(cacheLabel.frame.size.width/2);
    frame2.origin.y= (viewInAlert.frame.size.height/2)-(cacheLabel.frame.size.height+30);
    cacheLabel.frame=frame2;
    
    
    CGRect frame3;
    frame3 = CGRectZero;
    frame3= lblCap.frame;
    frame3.origin.x= (viewInAlert.frame.size.width/2)-(lblCap.frame.size.width/2);
    frame3.origin.y= (viewInAlert.frame.size.height/2)-(lblCap.frame.size.height+cacheLabel.frame.size.height+30);
    lblCap.frame=frame3;
    
    viewInAlert.backgroundColor=[UIColor whiteColor];
    
 
    
    customAlert.tag=999;
    
    
    CATransition *transition = [CATransition animation];
    transition.duration = 1.0;
    transition.type = kCATransitionFromLeft; //choose your animation
    [viewInAlert.layer addAnimation:transition forKey:nil];
    [self.view addSubview:viewInAlert];
    
    btnBackStorage = [[UIButton alloc]initWithFrame:bckBtn.frame];
    
    if ([(NSString*)[UIDevice currentDevice].model hasPrefix:@"iPad"] )
    {
        
        CGRect frame;
        frame=CGRectZero;
        frame=btnBackStorage.frame;
        frame.origin.y=3;
        
        btnBackStorage.frame=frame;
        
    }

    
    btnBackStorage.backgroundColor=[UIColor clearColor];
    [btnBackStorage addTarget:self
                 action:@selector(myaction)
       forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBackStorage];
    
    
   NSMutableArray *arr = [[NSUserDefaults standardUserDefaults]objectForKey:@"guidesName"];
    
    NSLog(@"arr :%@",arr);
    
    
    
    arrayForDetails = [[NSMutableArray alloc]init];
    
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSMutableArray *arrFavorites;
    
    {
        
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSMutableDictionary *plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
            arrFavorites = [plistDict objectForKey:@"downloadsArray"];
            
            
            
          
            if([fileManager fileExistsAtPath:finalPath])
            {
                NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
                myMutableArrayAgain = [plistDict objectForKey:@"downloadsArray"];
                muttarr =[myMutableArrayAgain copy];
                
            }
        }}

    
    
  
    
    NSLog(@"arrayForDetails :%@",arrayForDetails);
    
    tblStorage = [[UITableView alloc]initWithFrame:viewInAlert.frame style:UITableViewStylePlain];
    
    tblStorage.tag=911;
    tblStorage.delegate=self;
    tblStorage.dataSource=self;
    
   

    tblStorage.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    tblStorage.frame=CGRectMake(0, bckBtn.frame.origin.y+bckBtn.frame.size.height+3, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-(bckBtn.frame.origin.y+bckBtn.frame.size.height+3));
    
    [self.view addSubview:tblStorage];
    
     if([muttarr count] > 0)
     {
    
     }
    else
    {
        UILabel *lblNoGuides =[[UILabel alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height/2, self.view.frame.size.width, 60)];
        lblNoGuides.backgroundColor=[UIColor clearColor];
        lblNoGuides.text=@"Sorry No guides available";
        lblNoGuides.textColor=[UIColor grayColor];
        lblNoGuides.textAlignment=NSTextAlignmentCenter;
        [tblStorage addSubview:lblNoGuides];
        
        tblStorage.separatorColor = [UIColor clearColor];
        
    }
    
    
  
        
        
    
}

-(void)myaction
{
    [viewInAlert removeFromSuperview];
    [btnBackStorage removeFromSuperview];
    [tblStorage removeFromSuperview];
    
    [guideSelectTable removeFromSuperview];
    
}
- (IBAction)knowCommu:(id)sender {
    
    if([self reachableSettings])
    {
    SettingsInnerViewController *abc =[[SettingsInnerViewController alloc]init];
    
   [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"forloader123"];
    
     [self presentViewController:abc animated:NO completion:nil];
    

        
    }

}
- (IBAction)startGuide:(id)sender {
    
    if([self reachableSettings])
    {
        
    loaderFI= [[UIView alloc]initWithFrame:self.view.frame];
    loaderFI.backgroundColor=[UIColor whiteColor];
    
    CGRect frame=CGRectZero;
    frame=loaderFI.frame;
    
    loaderFI.frame=frame;
    
    
    UIActivityIndicatorView *spinnerFI = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinnerFI.center = CGPointMake([[UIScreen mainScreen]bounds].size.width / 2.0, ([[UIScreen mainScreen]bounds].size.height / 2.0)-frame.origin.y);
    [spinnerFI startAnimating];

    [loaderFI addSubview:spinnerFI];
    
    [self.view addSubview:loaderFI];
    
    [self staticWS];
    
    
  
    
    }
}
-(void)staticWS
{
  

    
    {
        NSString *wUrl=[NSString stringWithFormat:@"http://insightto.com/webservice/staticpage.php?id=1770"];
        //SETTINGS_WEBSERVICE_SUB
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(data==nil)
            {
                [self staticWS];
                return ;
            }
            
            NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSLog(@"URL :%@", wUrl);
            NSLog(@"json :%@", json);
            
            
            arraystatic=[[NSMutableArray alloc]init];
            arraystatic=[json objectForKey:@"static"];
            
            dispatch_async(dispatch_get_main_queue(), ^{
            
                PDDetailViewController *detailViewController = [[PDDetailViewController alloc] init];
              
                
                
                 NSMutableArray *mut = [[NSMutableArray alloc]initWithArray:arraystatic];
                
                [mut insertObject:@"Type" atIndex:0];
                

                
                NSMutableDictionary *combinedAttributes;
                
                if([[mut objectAtIndex:1] isKindOfClass:[NSDictionary class]])
                {
                    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
                    [dictionary setObject:@"static" forKey:@"type"];
                    NSLog(@"dictionary :%@",dictionary);
                    
                    combinedAttributes = [[mut objectAtIndex:1] mutableCopy];
                    [combinedAttributes addEntriesFromDictionary:dictionary];
                    
                   
                    NSLog(@"combinedAttributes :%@",combinedAttributes);
                    
                    
                   
                }
                else
                {
                    [[mut objectAtIndex:1] addObject:@"type"];
                }
        
                
                
             
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                dic = [combinedAttributes copy];
                
                [detailViewController getDictionary:dic];
                detailViewController.hidesBottomBarWhenPushed = YES;
                detailViewController.isFromFrom=YES;
                [self presentViewController:detailViewController animated:NO completion:nil];
            
            });
        }];
        
        [dataTask resume];
        
    }
    
}
- (IBAction)review:(id)sender {
    
    NSString *theUrl;
    
    if([[DBOperations getMainAppid] isEqualToString:@"266"])
    {
        theUrl = @"https://itunes.apple.com/us/app/costa-rica-travel/id1244935084?ls=1&mt=8";
    }
    else
    theUrl = @"https://itunes.apple.com/us/app/localsguide/id1094964995?mt=8";
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];

    
}
- (IBAction)facebook:(id)sender {
    
    NSURL *facebookURL = [NSURL URLWithString:@"https://www.facebook.com/localsguideapp/?ref=bookmarks"];
    
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://"]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"fb://profile/625769634120952"]];
    }
    else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"fb://profile/625769634120952"]];
        
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://profile/625769634120952"]]) {
            
           
        }
        else
        {
             [[UIApplication sharedApplication] openURL:facebookURL];
        
        }
    }

    
}

-(void)deleteBtnAction:(UIButton*)sender {
    
    if(!boolOnce)
    {
    NSMutableDictionary *plistDict;
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSMutableArray *arrFavorites;
    
    {
        
        if([fileManager fileExistsAtPath:finalPath])
        {
            plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
            arrFavorites = [plistDict objectForKey:@"downloadsArray"];
            
            
            
    
            if([fileManager fileExistsAtPath:finalPath])
            {
                NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
                myMutableArrayAgain = [plistDict objectForKey:@"downloadsArray"];
            }
            
            
            if (!myMutableArrayAgain) myMutableArrayAgain = [[NSMutableArray alloc] init];
            
            
            
            
            
        }
    }

    
    //[tblStorage setEditing:YES animated:YES];
    
     NSLog(@"deleteBtn tag : %ld",(long)sender.tag);
    
   // [deleteBtn setTitle:@"Deleting" forState:UIControlStateNormal];
    
    
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *mainPath    = [myPathList  objectAtIndex:0];
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSArray *fileArray = [fileMgr contentsOfDirectoryAtPath:mainPath error:nil];
    
    
    NSString *idvalue = [[myMutableArrayAgain objectAtIndex:sender.tag]valueForKey:@"id"];
    
    NSString *namevalue =  [[myMutableArrayAgain objectAtIndex:sender.tag]valueForKey:@"app_name"];
    
    NSString *stringToDelete = [NSString stringWithFormat:@"%@_images_%@",namevalue,idvalue];
    NSLog(@"stringToDelete :%@",stringToDelete);
    
   
     NSLog(@"check for app id: %@",[[myMutableArrayAgain objectAtIndex:sender.tag]valueForKey:@"id"]);
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:[NSString stringWithFormat:@"urlZip%@",[[myMutableArrayAgain objectAtIndex:sender.tag]valueForKey:@"id"]]];
    
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:[NSString stringWithFormat:@"skipIntermediate%@",[[myMutableArrayAgain objectAtIndex:sender.tag]valueForKey:@"id"]]];
    
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:[NSString stringWithFormat:@"100Percent%@",[[myMutableArrayAgain objectAtIndex:sender.tag]valueForKey:@"id"]]];
    
    
     [myMutableArrayAgain removeObjectAtIndex:sender.tag];
   
    
    [plistDict setObject:myMutableArrayAgain forKey:@"downloadsArray"];
    [plistDict writeToFile:finalPath atomically:YES];
    
    NSIndexPath *path = [NSIndexPath indexPathForRow:sender.tag inSection:1];
    
   
    
   
        muttarr = [[NSMutableArray alloc] init];
    

    {
        muttarr = [myMutableArrayAgain copy];
    
    }
    
    
    [tblStorage reloadData];
    

    
    }
}

- (IBAction)toSTableVC:(id)sender {
    
  
}

-(void)selectorhere {
    
    int fl=m_testView.percent;
    lblPercen.text=[NSString stringWithFormat:@"%d",fl];
    
    
}
-(void)cancelBtnAction:(UIButton*)sender
{
    boolOnce=NO;
    [container removeFromSuperview];

    [dataTaske cancel];
    
    m_testView.hidden=YES;
    
    container.hidden=YES;
    
    btnBackStorage.enabled=YES;
    bckBtn.enabled=YES;
    tblStorage.userInteractionEnabled=true;
    m_testView.percent=0;
    
}
-(void)reloadd:(UIButton*)sender
{
    if(!boolOnce)
    {
        boolOnce=YES;
        
     m_testView.percent=0;
    
    NSLog(@"sender tag reload :%ld",(long)sender.tag);
    
    
    
    btnCancel = [[UIButton alloc]init];
    btnCancel.frame=CGRectMake(30, 0, 60, 25);
    
    [btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    [btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnCancel.titleLabel.font= [UIFont systemFontOfSize:12];
    [btnCancel setBackgroundColor:[UIColor colorWithRed:(194/255.0) green:(0/255.0) blue:(10/255.0) alpha:1.0]];
    btnCancel.layer.cornerRadius=5.0;

    [btnCancel addTarget:self action:@selector(cancelBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
   
    
    lblPercen = [[UILabel alloc]init];
    lblPercen.frame=CGRectMake(0, 0, 25, 25);
    int fl=m_testView.percent;
    lblPercen.text=[NSString stringWithFormat:@"%d %%",fl];
    lblPercen.font=[UIFont systemFontOfSize:10];
    
   
    
    m_testView = [[PDTesterView alloc] initWithFrame:CGRectMake(0,0,25,25)];
   
    m_testView.percent = 0;
    m_testView.backgroundColor=[UIColor whiteColor];
    
    
   
    
    
    UITableViewCell* cell = (UITableViewCell*)[sender superview];
    NSIndexPath* indexPath = [tblStorage indexPathForCell:cell];
    
    
  
    
     
     CGPoint touchPoint = [sender convertPoint:CGPointZero toView:tblStorage];
     NSIndexPath *clickedButtonIndexPath = [tblStorage indexPathForRowAtPoint:touchPoint];
     
     NSLog(@"index path.section ==%ld",(long)clickedButtonIndexPath.section);
     
     NSLog(@"index path.row ==%ld",(long)clickedButtonIndexPath.row);
    
    [[NSUserDefaults standardUserDefaults]setInteger:sender.tag
                                              forKey:@"index-path.row"];
    
    UITableViewCell* cell1 = [tblStorage cellForRowAtIndexPath:clickedButtonIndexPath];
    cell1.accessoryView = m_testView;
    
    
    CGRect adjustedFrame = cell1.accessoryView.frame;
    adjustedFrame.origin.x += 10.0f;
    cell1.accessoryView.frame = adjustedFrame;
    
    
    
    container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 44)];
    [container addSubview:btnCancel];
    [container addSubview:lblPercen];
    container.backgroundColor=[UIColor whiteColor];
    
    
    cell1.accessoryView = container;
    
    btnBackStorage.enabled=NO;
    bckBtn.enabled=NO;
    
    
    
    [self test];
        
    }
}



- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler {
    completionHandler(NSURLSessionResponseAllow);
    
    
    _downloadSize=[response expectedContentLength];
    _dataToDownload=[[NSMutableData alloc]init];
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data {
    [_dataToDownload appendData:data];
    
    
    NSLog(@"_dataToDownload STORAGE :%lu  ,_downloadedSize STORAGE :%f",(unsigned long)[_dataToDownload length ],_downloadSize); //[_dataToDownload length ]/_downloadSize
    
    
    float percenta = (float)_downloadSize;
    
    float sizeOFFoler=(float)[_dataToDownload length];
    
    NSLog(@"sizeFolder STORAGE :%f",sizeOFFoler);
    
    
    percenta = [[NSUserDefaults standardUserDefaults]integerForKey:@"your content length"];
    NSLog(@"percenta :%f",percenta);
    
    
    float sizee = ((float)sizeOFFoler/(float)percenta)*100;//6.0f / 100.0f);
    
    NSLog(@"sizee STORAGE:%f",sizee);
    
    NSLog(@"m_testView.percent STORAGE:%f",m_testView.percent);
    
    int fl=m_testView.percent;
    lblPercen.text=[NSString stringWithFormat:@"%d %%",fl];
    
    if(sizee >99)
    {
      m_testView.hidden=YES;
        
        container.hidden=YES;
        
        btnBackStorage.enabled=YES;
        bckBtn.enabled=YES;
        tblStorage.userInteractionEnabled=true;
        
        boolOnce=NO;
       

    }
    if (sizee < 100) {
        
        if (m_testView.percent < 1)
        {
            m_testView.percent = (float)sizee;
            [m_testView setNeedsDisplay];
        }
        else
        {
            m_testView.percent = (int)sizee;
            [m_testView setNeedsDisplay];
        }
        
    }
    
    
}
-(void)test
{
    NSInteger indexpathe =[[NSUserDefaults standardUserDefaults]integerForKey:@"index-path.row"];
    
    NSLog(@"indexpathe :%ld",(long)indexpathe);
    
    NSString *wUrl=[NSString stringWithFormat:@"%@%@&multi=yes",OFFLINE_WEBSERVICE,[[myMutableArrayAgain objectAtIndex:indexpathe]valueForKey:@"id"]];
    
   
    NSLog(@"url = %@",wUrl);
    
    NSURLSession *session = [NSURLSession sharedSession];
   NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(data==nil)
        {
            [self test];
            return ;
        }
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"URL :%@", wUrl);
        NSLog(@"json :%@", json);
        
       
        
        NSData *data2 = [NSKeyedArchiver archivedDataWithRootObject:json];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSArray *totalSize = [json objectForKey:@"total_file_size"];
            
            NSString *urlZip = [json objectForKey:@"app_image_folder"];
            
            [[NSUserDefaults standardUserDefaults]setObject:urlZip forKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
            
            [self downloadZip];
            
            NSLog(@"totalSize :%@",totalSize);
            
            [[NSUserDefaults standardUserDefaults]setObject:totalSize forKey:@"TS"];
            
          
        });
        
    }];
    
    [dataTask resume];
    
    
    
}

-(void)downloadZip
{
  
    
    NSString *url1 = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
    
   
    NSLog(@"Downloading Started");
    
    NSURL *url = [NSURL URLWithString:[url1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        NSInteger httpStatus = [((NSHTTPURLResponse *)response) statusCode];
        NSLog(@"responsecode:%ld", (long)httpStatus);
        
        
        if (error||httpStatus == 404) {
            
            NSLog(@"Download Error:%@",error.description);
            
            
            btnBackStorage.enabled=YES;
            bckBtn.enabled=YES;
            tblStorage.userInteractionEnabled=true;
            boolOnce=NO;
            
            m_testView.hidden=YES;
            container.hidden=YES;

            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                            message:@"System error occured"
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
            return ;
        }
        if (data) {
            
          
            NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
            documentsURL = [documentsURL URLByAppendingPathComponent:@"localFile.zip"];
            
            
            [data writeToURL:documentsURL atomically:YES];
            
            
            NSString *filename = @"localFile.zip";
            NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString * zipPath = [documentsDirectory stringByAppendingPathComponent:filename];
            
            NSString *destinationPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            [SSZipArchive unzipFileAtPath:zipPath toDestination:destinationPath];
            
            
            NSLog(@"File is saved  ");
            
            btnBackStorage.enabled=YES;
            bckBtn.enabled=YES;
            tblStorage.userInteractionEnabled=true;
            boolOnce=NO;
            
        }
        
        
        
        
        
        
        // list contents of Documents Directory just to check
        NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
        
        NSArray *contents = [[NSFileManager defaultManager]contentsOfDirectoryAtURL:documentsURL includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:nil];
        
        NSLog(@"description for zip:%@", [contents description]);
        
        
        
    }];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        
        
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
        
      
        //♥
        NSURL *urlforData = [NSURL URLWithString:[url1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        NSLog(@"urlforData 123 :%@",urlforData);
        
        [[NSUserDefaults standardUserDefaults]setObject:url1 forKey:@"urlforData123"];
        
        [self getContentLength:^(long long returnValue) {
            
            NSLog(@"your content length : %lld",returnValue);
            
            [[NSUserDefaults standardUserDefaults]setInteger:returnValue forKey:@"your content length"];
            
            dataTaske = [defaultSession dataTaskWithURL: urlforData];
            
            [dataTaske resume];
            
        }];
        
        
        
        
     
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
         
            
        });
    });
    
    
}

- (void) getContentLength : (void(^)(long long returnValue))completionHandler
{
    NSString *url= [[NSUserDefaults standardUserDefaults]objectForKey:@"urlforData123"];
    
    NSString * urlString =  [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
   
    
    __block long long totalContentFileLength = nil;
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    request.HTTPMethod = @"HEAD";
    [request addValue:@"identity" forHTTPHeaderField:@"Accept-Encoding"];
    
    
    NSURLSessionDownloadTask *uploadTask= [session downloadTaskWithRequest:request
                                                         completionHandler:^(NSURL *url,NSURLResponse *response,NSError *error) {
                                                             
                                                             NSLog(@"handler size: %lld", response.expectedContentLength);
                                                             
                                                             totalContentFileLength = [[NSNumber alloc] initWithFloat:response.expectedContentLength].longLongValue;
                                                             
                                                             completionHandler(totalContentFileLength);
                                                             
                                                         }];
    NSLog(@"content length=%lld", totalContentFileLength);
    [uploadTask resume];
    
}

-(void)sizeCapDeleteAction
{
    
    
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *mainPath    = [myPathList  objectAtIndex:0];
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSArray *fileArray = [fileMgr contentsOfDirectoryAtPath:mainPath error:nil];
    
     NSString *str = [mainPath stringByAppendingPathComponent:@"JMCache"];
    
    NSError* error;
    NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:mainPath error: &error];
    NSNumber *size = [fileDictionary objectForKey:NSFileSize];
    
   uint64_t fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:mainPath error:nil] fileSize];
    
    NSLog(@"size of file :%@",size);NSLog(@"size of file :%llu",fileSize);
    
    unsigned long long int fileSizeee = 0;
    
    fileSizeee = [self folderSize:mainPath];
    
    NSLog(@"fileSizeee :%llu",fileSizeee);
    
    for (NSString *filename in fileArray)  {
        
       
    }
    


}

- (unsigned long long int)folderSize:(NSString *)folderPath {
    NSArray *filesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:folderPath error:nil];
    NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
    NSString *fileName;
    unsigned long long int fileSize = 0;
    
    while (fileName = [filesEnumerator nextObject]) {
        NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:[folderPath stringByAppendingPathComponent:fileName] error:nil];
        fileSize += [fileDictionary fileSize];
    }
    
    return fileSize;
}

- (NSString *)extractNumberFromText:(NSString *)text
{
    NSCharacterSet *nonDigitCharacterSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    return [[text componentsSeparatedByCharactersInSet:nonDigitCharacterSet] componentsJoinedByString:@""];
}


@end
