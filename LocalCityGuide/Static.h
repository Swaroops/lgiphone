//
//  Static.h
//  Visiting Ashland
//
//  Created by Anu on 11/01/16.
//  Copyright (c) 2016 iDeveloper. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface Static : NSManagedObject
@property (nonatomic, retain) NSString * appid;
@property (nonatomic, retain) NSString * catid;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSArray * image;
@property (nonatomic, retain) NSString * latitude;
@property (nonatomic, retain) NSString * longitude;
@property (nonatomic, retain) NSString * staticId;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * title;

@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * email_text;
@property (nonatomic, retain) NSArray * favorites;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * share;
@property (nonatomic, retain) NSString * show_email;
@property (nonatomic, retain) NSString * show_phone;
@property (nonatomic, retain) NSString * show_url;
@property (nonatomic, retain) NSString * urll;
@property (nonatomic, retain) NSString * url_text;


@end
