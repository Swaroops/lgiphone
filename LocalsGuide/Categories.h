//
//  Categories.h
//  Visiting Ashland
//
//  Created by swaroop on 10/07/15.
//  Copyright (c) 2015 iDeveloper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Subcategory1;

@interface Categories : NSManagedObject


@property (nonatomic, retain) NSString * appid;
@property (nonatomic, retain) NSString * catId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * paid;
@property (nonatomic, retain) NSString * points;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * googletype;
@property (nonatomic, retain) NSString * external_url;
@property (nonatomic, retain) NSString * rss_url;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * listing_icon;
@property (nonatomic, retain) NSString * sub_exists;
@property (nonatomic, retain) NSString * ad;
@property (nonatomic, retain) NSString * link;
@property (nonatomic, retain) NSString * header_image;
//

//@property (nonatomic, retain) NSString * fav;

//@property (nonatomic, retain) Subcategory1 *subcategory;
@property (nonatomic, retain) NSArray* subcategory;


-(NSString*)getAppid;
-(NSString*)getCatID;
-(NSString*)getName;
-(NSString*)getPaid;
-(NSString*)getPoints;
-(NSString*)getType;
-(NSString*)getGoogleType;
-(NSString*)getExternalUrl;
-(NSString*)getRssUrl;
-(NSString*)getImage;
-(NSString*)getListingIcon;
-(NSString*)getSubExists;
-(NSString*)getAd;
-(NSString*)getLink;
//-(NSString*)getfav;//header_image
-(id)getSubcategory;
-(NSString*)getheader_image;

@end
