//
//  Deals.h
//  Visiting Ashland
//
//  Created by swaroop on 31/07/15.
//  Copyright (c) 2015 iDeveloper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Deals : NSManagedObject

@property (nonatomic, retain) NSString * appid;
@property (nonatomic, retain) NSString * deal_id;
@property (nonatomic, retain) NSArray  * image;
@property (nonatomic, retain) NSString * featured;
@property (nonatomic, retain) NSString * deal_title;
@property (nonatomic, retain) NSString * deal_subtitle;
@property (nonatomic, retain) NSString * deal_description;
@property (nonatomic, retain) NSString * deal_phone;
@property (nonatomic, retain) NSString * deal_web;
@property (nonatomic, retain) NSString * thumb;

@end
