//
//  Deals.m
//  Visiting Ashland
//
//  Created by swaroop on 31/07/15.
//  Copyright (c) 2015 iDeveloper. All rights reserved.
//

#import "Deals.h"


@implementation Deals

@dynamic appid;
@dynamic deal_id;
@dynamic image;
@dynamic featured;
@dynamic deal_title;
@dynamic deal_subtitle;
@dynamic deal_description;
@dynamic deal_phone;
@dynamic deal_web;
@dynamic thumb;

@end
