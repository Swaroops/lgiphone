//
//  Events.h
//  Visiting Ashland
//
//  Created by swaroop on 31/07/15.
//  Copyright (c) 2015 iDeveloper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Events : NSManagedObject

@property (nonatomic, retain) NSString * appid;
@property (nonatomic, retain) NSString * event_id;
@property (nonatomic, retain) NSArray * image;
@property (nonatomic, retain) NSString * featured;
@property (nonatomic, retain) NSString * event_title;
@property (nonatomic, retain) NSString * event_time;
@property (nonatomic, retain) NSString * event_address;
@property (nonatomic, retain) NSString * event_description;
@property (nonatomic, retain) NSString * thumb;
@property (nonatomic, retain) NSString * event_continue;
@property (nonatomic, retain) NSString * event_start_date;
@property (nonatomic, retain) NSString * event_expire_date;
@property (nonatomic, retain) NSString * event_expiration;
@end
