//
//  Gallery.h
//  Visiting Ashland
//
//  Created by swaroop on 30/07/15.
//  Copyright (c) 2015 iDeveloper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Gallery : NSManagedObject


@property(nonatomic,retain)   NSString * appid;
@property (nonatomic, retain) NSString * gall_description;
@property (nonatomic, retain) NSString * gall_id;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * thumb;
@property (nonatomic, retain) NSString * title;

@end
