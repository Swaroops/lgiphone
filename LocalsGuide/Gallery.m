//
//  Gallery.m
//  Visiting Ashland
//
//  Created by swaroop on 30/07/15.
//  Copyright (c) 2015 iDeveloper. All rights reserved.
//

#import "Gallery.h"


@implementation Gallery

@dynamic appid;
@dynamic gall_description;
@dynamic gall_id;
@dynamic image;
@dynamic thumb;
@dynamic title;

@end
