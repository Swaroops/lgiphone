//
//  PDMapCatCell.m
//  LocalsGuide
//
//  Created by swaroop on 06/03/14.
//  Copyright (c) 2014 iDeveloper. All rights reserved.
//

#import "PDMapCatCell.h"

@implementation PDMapCatCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)onValueChanged:(id)sender
{
    UISwitch *onoff = (UISwitch *) sender;
    NSString *tagVal = [NSString stringWithFormat:@"%d",onoff.tag];
    NSString *strVal = onoff.on ? @"Yes" : @"No";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:tagVal forKey:@"index"];
    [dict setObject:strVal forKey:@"switchValue"];
    [_responseTarget performSelector:_respondToMethod withObject:dict afterDelay:0];
}
@end
