

#import "UIImageView+AFNetworking.h"
#import "PDAppDelegate.h"
#import "PDViewController.h"
#import "PDWebViewController.h"
#import "PDSplashViewController.h"
#import "AsyncImageView.h"
#import "FTWCache.h"
#import "NSString+MD5.h"
#import "Reachability.h"
#import "PDInappHomeViewController.h"
#import "Apps.h"
#import "MKStoreKit.h"
#import "GMDCircleLoader.h"
#import "IMAGES_TO_CACHE.h"
#import "UIImageView+WebCache.h"
#import "PDGalleryDetailViewController.h"
#import "PhotoViewController.h"




#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

@implementation PDAppDelegate

{
    PDSplashViewController *splashController;
    NSArray*arAdDetails;
    
    UINavigationController *navig1;
}
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
CLLocation *location_updated;
NSArray *arrFooters;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
   
    
    
    NSArray *directories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documents = [directories firstObject];
    NSLog(@"DOCUMENTS > %@", documents);
    
           
    self.window.backgroundColor=[UIColor blackColor];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"HomeResponse"])
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"HomeResponse"];
    }
    [AsyncImageLoader sharedLoader].cache = [AsyncImageLoader defaultCache];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
  
    
    [self setStatusBarBackgroundColor:[UIColor clearColor]];
    
    [self.window makeKeyAndVisible];
    
    
    
    [[MKStoreKit sharedKit] startProductRequest];
    [[NSNotificationCenter defaultCenter] addObserverForName:kMKStoreKitProductsAvailableNotification
                                                      object:nil
                                                       queue:[[NSOperationQueue alloc] init]
                                                  usingBlock:^(NSNotification *note) {
                                                      
                                                      NSLog(@"Products available: %@", [[MKStoreKit sharedKit] availableProducts]);
                                                  }];
    
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kMKStoreKitProductPurchasedNotification
                                                      object:nil
                                                       queue:[[NSOperationQueue alloc] init]
                                                  usingBlock:^(NSNotification *note) {
                                                      
                                                      NSLog(@"Purchased/Subscribed to product with id: %@", [note object]);
                                                  }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kMKStoreKitRestoredPurchasesNotification
                                                      object:nil
                                                       queue:[[NSOperationQueue alloc] init]
                                                  usingBlock:^(NSNotification *note) {
                                                      
                                                      NSLog(@"Restored Purchases");
                                                  }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kMKStoreKitRestoringPurchasesFailedNotification
                                                      object:nil
                                                       queue:[[NSOperationQueue alloc] init]
                                                  usingBlock:^(NSNotification *note) {
                                                      
                                                      NSLog(@"Failed restoring purchases with error: %@", [note object]);
                                                  }];
    
    
    
    [self sizeCapDeleteAction];
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    if(![fileManager fileExistsAtPath:finalPath])
    {
        NSError *error;
        NSString * sourcePath = [[NSBundle mainBundle] pathForResource:@"LocalData" ofType:@"plist"];
        [fileManager copyItemAtPath:sourcePath toPath:finalPath error:&error];
    }
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = 250;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if (IS_OS_8_OR_LATER){
        [_locationManager requestWhenInUseAuthorization];}
    [_locationManager startUpdatingLocation];
    
    
 
    
    NSString *splashImgName=[[NSUserDefaults standardUserDefaults]objectForKey:@"splash1"];
    splashController = [[PDSplashViewController alloc]init];
    
    
    
    splashController.view.frame=CGRectMake(0, -50, 320, 568);
    [self.window addSubview:splashController.view];
    [self.window setRootViewController:splashController];
    
    
    if(splashImgName !=nil)
    {
        [splashController initWithSplashName:splashImgName];
        
        
    }
    
    if ([self reachable_app]&&splashImgName ==nil)
    {
        [self startWebService];
    }
   
   
    
    
    if ([self reachable_app]) {
        if(![[NSUserDefaults standardUserDefaults]objectForKey:@"forDistanceFromAdmin"])
        {
            [self testingSettingsWB];
        }

     
    }
    else
    {
        NSString *splashImgUrl =  [[NSUserDefaults standardUserDefaults]objectForKey:@"splash1"];
        
        
        [splashController initWithSplashName:splashImgUrl];
       
        
    }
    
    
    PDInappHomeViewController *viewController = [[PDInappHomeViewController alloc ]init];
    navig1 = [[UINavigationController alloc] initWithRootViewController:viewController];
    

    [self readListOfImagesToDownload];
    
    return YES;
    
    
    
}

-(void)sizeCapDeleteAction
{
    
    
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *mainPath    = [myPathList  objectAtIndex:0];
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSArray *fileArray = [fileMgr contentsOfDirectoryAtPath:mainPath error:nil];
    
  
    
    NSError* error;
    NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:mainPath error: &error];
    NSNumber *size = [fileDictionary objectForKey:NSFileSize];
    
    uint64_t fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:mainPath error:nil] fileSize];
    
    NSLog(@"size of file :%@",size);NSLog(@"size of file :%llu",fileSize);
    
    unsigned long long int fileSizeee = 0;
    
    fileSizeee = [self folderSize:mainPath];
    
    NSLog(@"fileSizeee :%llu",fileSizeee);
    
   
    
     NSString *string = [NSByteCountFormatter stringFromByteCount:fileSizeee countStyle:NSByteCountFormatterCountStyleFile];
    
    NSLog(@"cache size :%@",string);
    
    int sizeofCache = [string intValue];
    
    NSLog(@"sizeofCache :%ld",(long)sizeofCache);
    
   
    
    if(sizeofCache>100)
    {
        for (NSString *filename in fileArray)
        {
         [fileMgr removeItemAtPath:[mainPath stringByAppendingPathComponent:filename] error:NULL];
        }
    }
    
    
    
}

- (unsigned long long int)folderSize:(NSString *)folderPath {
    NSArray *filesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:folderPath error:nil];
    NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
    NSString *fileName;
    unsigned long long int fileSize = 0;
    
    while (fileName = [filesEnumerator nextObject]) {
        NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:[folderPath stringByAppendingPathComponent:fileName] error:nil];
        fileSize += [fileDictionary fileSize];
    }
    
    return fileSize;
}


- (void)setStatusBarBackgroundColor:(UIColor *)color {
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}


- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if (([self.window.rootViewController.presentedViewController isKindOfClass: [PDGalleryDetailViewController class]])||([self.window.rootViewController.presentedViewController isKindOfClass: [PhotoViewController class]]))
    {
        return UIInterfaceOrientationMaskAll;
    }
    else return UIInterfaceOrientationMaskPortrait;
}

-(void)testingSettingsWB
{
    
    NSString *wUrl=[NSString stringWithFormat:@"http://insightto.com/webservice/settings.php?id=%@&multi=yes",[DBOperations getMainAppid]];
    
    
    NSLog(@"url = %@",wUrl);
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(data==nil)
        {
            [self testingSettingsWB];
            return;
        }
        
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSString *distancein=[[[json objectForKey:@"settings"]objectAtIndex:0]objectForKey:@"distance_in"];
            
            NSLog(@"distancein :%@",distancein);
            
            if([distancein isEqualToString:@"km"])
            {
                [[NSUserDefaults standardUserDefaults]setObject:@"K" forKey:@"showDistanceIn"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTableView" object:self];
                
                 [[NSUserDefaults standardUserDefaults]setObject:@"forDistanceFromAdmin" forKey:@"forDistanceFromAdmin"];
                
            }
            
            
        });
        
    }];
    
    [dataTask resume];
    
}

-(void)startWebService
{
   
    
    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(webServiceResponse:);
    [webService startParsing:[NSString stringWithFormat:@"%@%@",SPLASH_WEBSERVICE,[DBOperations getMainAppid]]];//1
}

-(void)webServiceResponse:(NSData *) responseData
{
    
    
    
    NSError *er;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&er];
    NSString *splashImgUrl=[[[dict objectForKey:@"splash"]objectAtIndex:0]objectForKey:@"splash_img"];
    
    [[NSUserDefaults standardUserDefaults]setObject:splashImgUrl forKey:@"splash1"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [splashController initWithSplashName:splashImgUrl];
    
    
    
    
}

-(void)navigatetoInapp
{
    int64_t delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        
        PDInappHomeViewController *viewController = [[PDInappHomeViewController alloc ]init];
        navig1 = [[UINavigationController alloc] initWithRootViewController:viewController];
        self.window.rootViewController=navig1;
        
        
        
    });

}


- (void)goToSplashLoad:(id)sender {
    
   
    
}
-(BOOL)reachable_app {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    location_updated = [locations lastObject];
}




- (void)applicationWillResignActive:(UIApplication *)application
{
   
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    
    UIApplication  *app = [UIApplication sharedApplication];
    UIBackgroundTaskIdentifier bgTask = 0;
    
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
    }];
    
    NSTimeInterval ti = [[UIApplication sharedApplication]backgroundTimeRemaining];
    NSLog(@"backgroundTimeRemaining: %f", ti);
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
   
    [GMDCircleLoader resumeStart];
    
    [self readListOfImagesToDownload];

}

-(void)readListOfImagesToDownload
{
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *readContext = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"IMAGES_TO_CACHE" inManagedObjectContext:readContext];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *results = [readContext executeFetchRequest:fetchRequest error:&error];
    
    
    NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    SDImageCache *imageCache =[SDImageCache sharedImageCache];
    imageCache = [imageCache initWithNamespace:imageCacheFolder];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        
        for(IMAGES_TO_CACHE *object in results)
        {
            
            NSString *imageUrl=object.imageurl;
            
            NSURL *urlimg=[NSURL URLWithString:imageUrl];
            
            UIImageView*imagevw=[[UIImageView alloc]init];
            imagevw.image=[imageCache imageFromDiskCacheForKey:imageUrl];
            
            
            if (!imagevw.image)
            {
                
                
                
                NSData *data = [NSData dataWithContentsOfURL:urlimg];
                imagevw.image = [UIImage imageWithData:data];
                
            }
            else
            {
                
                
                
                PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
                NSManagedObjectContext *context1 = [appDelegate managedObjectContext];
                NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                NSEntityDescription *entity = [NSEntityDescription
                                               entityForName:@"IMAGES_TO_CACHE" inManagedObjectContext:context1];
                [fetchRequest setEntity:entity];
                
                fetchRequest.predicate = [NSPredicate predicateWithFormat:@"imageurl=%@",imageUrl];
                NSError *error;
                NSArray *listOfQUEToBeDeleted = [context1 executeFetchRequest:fetchRequest error:&error];
                IMAGES_TO_CACHE *currentQUE;
                for(currentQUE in listOfQUEToBeDeleted)
                {
                    [context1 deleteObject:currentQUE];
                }
                @try {
              
  
                if (![context1 save:&error]) {
                    NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                }
                    
                }
                @catch (NSException * e) {
                    
                    
                }
            }
            
        }
        
        
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
    });
    
    
    
    
    
    
}




- (void)applicationDidBecomeActive:(UIApplication *)application
{
  
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"firstLoading"];
    
}
- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
           
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Ashland Oregon" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

-(NSUInteger)calculate{
    
    //Check where you are making the sqlite
    NSURL *url = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Ashland Oregon.sqlite"];
    NSUInteger sizeOfData = [[NSData dataWithContentsOfURL:url] length];
    NSLog(@"sizeOfDAta in bytes %lu",(unsigned long)sizeOfData);
    
    return sizeOfData;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Ashland Oregon.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        
        abort();
    }
    
    return _persistentStoreCoordinator;
}
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
