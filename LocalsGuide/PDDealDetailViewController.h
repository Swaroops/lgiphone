
#import <UIKit/UIKit.h>

@interface PDDealDetailViewController : UIViewController <CLLocationManagerDelegate,GADBannerViewDelegate,UIScrollViewDelegate>
{
    GADBannerView *bannerView_;
    IBOutlet UILabel *lblNavTitle;
    IBOutlet UIButton *btnPhone;
    IBOutlet UIButton *btnWeb;
    IBOutlet UIScrollView *scrollView_;
    IBOutlet UIButton *btnFavorites;

}
@property (nonatomic, retain) CLLocationManager *locationManager;
-(void)getDealDetails:(NSDictionary *) dictDetails;
-(IBAction)onBack:(id)sender;
-(IBAction)onWebSite:(id)sender;
-(IBAction)onCall:(id)sender;
-(IBAction)onFavorite:(id)sender;
-(IBAction)onClick:(id)sender;

@property (nonatomic)BOOL isFromFav;
@property (weak, nonatomic) IBOutlet UIButton *mapItBtn;
@property (strong, nonatomic) IBOutlet UIView *vwDirection;
@property (weak, nonatomic) IBOutlet UIWebView *dirWebvw;
@property (weak, nonatomic) IBOutlet UIButton *closeDir;

@end
