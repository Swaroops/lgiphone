
#import "PDDealDetailViewController.h"
#import "UIImageView+AFNetworking.h"
#import "PDViewController.h"
#import "PDDealsViewController.h"
#import "PDMapViewController.h"
#import "PDEventsViewController.h"
#import "Reachability.h"
#import "UIImageView+WebCache.h"
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface PDDealDetailViewController ()
{
    BOOL isHotSpotAd;
    BOOL isFavorite;
    int banery;
    int scrollHeight;
    NSString *webAddress;
      UIScrollView *scrollView;
    UIPageControl*pageControl;
    NSMutableArray*scrollArray;
}
@end


@implementation PDDealDetailViewController
@synthesize mapItBtn,vwDirection,dirWebvw,closeDir,isFromFav;
CLLocation *location_updated;
NSDictionary *dictDealDetails;
NSString *adLink;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.navigationController.navigationBarHidden = YES;
    if (IS_IPHONE5) {
        [[NSBundle mainBundle] loadNibNamed:@"PDDealDetailViewController" owner:self options:Nil];
         banery = 470;
    }
    else {
        
        [[NSBundle mainBundle] loadNibNamed:@"PDDealDetailViewController4" owner:self options:Nil];
        banery = 380;
    }
    [super viewDidLoad];
    if([self reachable_dealDetail]){
         mapItBtn.hidden=NO;
    }
    else{
        mapItBtn.hidden=YES;
    }
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
   
    if([self reachable_dealDetail])
    if(![removAdStatus isEqualToString:@"purchased"])
    [self showAd];
    
    [self setDetails];
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = 250;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if (IS_OS_8_OR_LATER){
        [_locationManager requestWhenInUseAuthorization];}
    [_locationManager startUpdatingLocation];
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}
-(void)viewWillAppear:(BOOL)animated {
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        myMutableArrayAgain = [plistDict objectForKey:@"favoritesArray"];
    }
    
    if ([myMutableArrayAgain count] == 0) {
        isFavorite = NO;
    }
    for (NSDictionary *dict in myMutableArrayAgain) {
        if ([[dict objectForKey:@"Id"] isEqualToString:[dictDealDetails objectForKey:@"Id"]]&&[[dict objectForKey:@"deal_title"] isEqualToString:[dictDealDetails objectForKey:@"deal_title"]]) {
            NSLog(@"these are same man!!!");
            isFavorite = YES;
            break;
        }
        else {
            isFavorite = NO;
        }
    }
    if (isFavorite) {
        [btnFavorites setBackgroundImage:[UIImage imageNamed:@"starNoFav.png"] forState:UIControlStateNormal];
    }
    else {
        [btnFavorites setBackgroundImage:[UIImage imageNamed:@"starFav.png"] forState:UIControlStateNormal];
    }
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    location_updated = [locations lastObject];
    NSLog(@"updated coordinate are %@",location_updated);
    isHotSpotAd = NO;
    NSMutableArray *arrHotSpots = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"HotSpotArray"]];
    for (NSDictionary *dictDetails in arrHotSpots) {
        CLLocation *targetLocation = [[CLLocation alloc] initWithLatitude:[[dictDetails objectForKey:@"lat"] doubleValue] longitude:[[dictDetails objectForKey:@"long"] doubleValue]];
        CLLocationDistance maxRadius = [[dictDetails objectForKey:@"radius"] floatValue]; // in meters
        isHotSpotAd = ([location_updated distanceFromLocation:targetLocation] <= maxRadius)?YES:NO;
        if (isHotSpotAd) {
            [[NSUserDefaults standardUserDefaults] setObject:dictDetails forKey:@"HotSpotAd"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
        }
    }
    
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
    if([self reachable_dealDetail])
    if(![removAdStatus isEqualToString:@"purchased"])
    [self showAd];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
    if([self reachable_dealDetail])
    if(![removAdStatus isEqualToString:@"purchased"])
    [self showAd];
}

-(void)showAd
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CustomAd"] isEqualToString:@"NO"]) {
        [self setAdbannerWithKey:[[NSUserDefaults standardUserDefaults] objectForKey:@"admob_id"]];
    }
    else {
        banery= self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-50;
        NSDictionary *dictHotspot = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HotSpotAd"];
        if (isHotSpotAd && (![[dictHotspot objectForKey:@"image_detail"] isEqualToString:@"FALSE"])) {
            NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HotSpotAd"];
            if (![[dict objectForKey:@"image_detail"] isEqualToString:@"NA"]) {
                AsyncImageView *ad1Image = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, banery, 320, 50)];
                [self.view addSubview:ad1Image];
                UIButton *ad1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
                ad1Btn.frame = CGRectMake(0, banery, 320, 50);
                [ad1Btn addTarget:self action:@selector(loadAd1) forControlEvents:UIControlEventTouchUpInside];
                [self.view addSubview:ad1Btn];
                ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"image_detail"]];
                adLink = [dict objectForKey:@"link_detail"];
                
                if (IS_IPHONE5)
                    scrollHeight = 501;
                else
                    scrollHeight = 311;
                scrollView_.frame = CGRectMake(0, 68, 320, scrollHeight);
            }
        }
        else {
            NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"CustomAdDict"];
            if (![[dict objectForKey:@"ad_3"] isEqualToString:@"NA"]) {
                AsyncImageView *ad1Image = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, banery, 320, 50)];
                [self.view addSubview:ad1Image];
                UIButton *ad1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
                ad1Btn.frame = CGRectMake(0, banery, 320, 50);
                [ad1Btn addTarget:self action:@selector(loadAd1) forControlEvents:UIControlEventTouchUpInside];
                [self.view addSubview:ad1Btn];
                ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"ad_3"]];
                adLink = [dict objectForKey:@"link_3"];
                if (IS_IPHONE5)
                    scrollHeight = 501;
                else
                    scrollHeight = 311;
                scrollView_.frame = CGRectMake(0, 68, 320, scrollHeight);
            }
        }
    }
}
-(void)loadAd1
{
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:adLink]];
}
-(void)setAdbannerWithKey:(NSString *) key
{
    NSLog(@"%@",key);
    bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    bannerView_.adUnitID = key;
    bannerView_.rootViewController = self;
    bannerView_.delegate = self;
    [bannerView_ loadRequest:[GADRequest request]];
    bannerView_.frame = CGRectMake(0, banery, bannerView_.frame.size.width, bannerView_.frame.size.height);
    [self.view addSubview:bannerView_];
}
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    if (IS_IPHONE5)
        scrollHeight = 400;
    else
        scrollHeight = 311;
    scrollView_.frame = CGRectMake(0, 68, 320, scrollHeight);
}
-(void)setDetails
{
    
   
    NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    SDImageCache *imageCache =[SDImageCache sharedImageCache];
    imageCache = [imageCache initWithNamespace:imageCacheFolder];
    NSArray*arrays=[dictDealDetails objectForKey:@"image"];
   // NSLog(@"%ld",[arrays count]);
    @try{
    if ([arrays count]==1 )
    {
        AsyncImageView *imgMainView = [[AsyncImageView alloc] init];
        imgMainView.contentMode = UIViewContentModeScaleToFill;
        imgMainView.clipsToBounds = YES;
        NSString*str=[arrays objectAtIndex:0];
        // str= [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
      
        
        
        NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
        SDImageCache *imageCache =[SDImageCache sharedImageCache];
        imageCache = [imageCache initWithNamespace:imageCacheFolder];
        imgMainView.image= [imageCache imageFromDiskCacheForKey:str];
         AsyncImageView *imgDeal = [[AsyncImageView alloc] init];
         imgDeal.image= [imageCache imageFromDiskCacheForKey:str];
        if (!imgDeal.image)
        {
            
//            if([self reachable_dealDetail]){
//                
//                [imgDeal sd_setImageWithURL:[NSURL URLWithString:[arrays objectAtIndex:0]] placeholderImage:nil options:SDWebImageRefreshCached];
//                
//                [imgDeal setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[arrays objectAtIndex:0]]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *imageX) {
//                    NSLog(@"success");
//                    NSLog(@"%f %f ",imageX.size.height,imageX.size.width);
//                    [imgDeal setImage:imageX];
//                    
//                    
//                    float imgWidth=scrollView_.frame.size.width;
//                    float heightImage = (imageX.size.height*imgWidth)/imageX.size.width;
//                    
//                    
//                    imgDeal.frame = CGRectMake(0, 0, imgWidth, heightImage);
//                    
//                    imgDeal.image=imageX;
//                    [scrollView_ addSubview:imgDeal];
//                    
//                    [self setWithDict:dictDealDetails imageView:imgDeal];
//                    
//                } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
//                    NSLog(@"fail with %@ and %@",[error localizedDescription],[request URL]);
//                    [self setWithDict:dictDealDetails imageView:imgDeal];
//                }
//                 ];
//            }
            
            if(str)
            {
                // if([self reachable_eventDetail])
                if(![self offlineAvailable])
                {
                    
                    [imgDeal sd_setImageWithURL:[NSURL URLWithString:[arrays objectAtIndex:0]] placeholderImage:nil options:SDWebImageRefreshCached];
                    
                    [imgDeal setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[arrays objectAtIndex:0]]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *imageX) {
                        NSLog(@"success");
                        NSLog(@"%f %f ",imageX.size.height,imageX.size.width);
                        [imgDeal setImage:imageX];
                        
                        
                        float imgWidth=scrollView_.frame.size.width;
                        float heightImage = (imageX.size.height*imgWidth)/imageX.size.width;
                        
                        
                        imgDeal.frame = CGRectMake(0, 0, imgWidth, heightImage);
                        imgDeal.image=imageX;
                        
                        [scrollView_ addSubview:imgDeal];
                        [self setWithDict:dictDealDetails imageView:imgDeal];
                        
                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                        NSLog(@"fail with %@ and %@",[error localizedDescription],[request URL]);
                        [self setWithDict:dictDealDetails imageView:imgDeal];
                    }
                     ];
                }
                else
                {
                    {
                        NSString *appNameFolder = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
                        
                        NSArray* foo = [appNameFolder componentsSeparatedByString:@".zip"];
                        
                        NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
                        
                        
                        
                        
                        NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];
                        
                        NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];
                        
                        NSLog(@"firstBit folder :%@",firstBit);
                        NSLog(@"secondBit folder :%@",secondBit2);
                        
                        NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
                        
                        NSLog(@"str url :%@",str);
                        
                        NSString *filePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/deal_images",secondBit2]];
                        
                        NSArray* str3 = [str componentsSeparatedByString:@"deal_images/"];
                        
                        NSString* firstBit3 = [str3 objectAtIndex:0]; NSString* secondBit3 = [str3 objectAtIndex:1];
                        
                        NSLog(@"firstBit folder :%@",firstBit3);
                        NSLog(@"secondBit folder :%@",secondBit3);
                        
                        NSString* Path = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",secondBit3]];
                        
                        UIImage* image = [UIImage imageWithContentsOfFile:Path];
                        
                        imgDeal.image=image;
                        
                        float imgWidth=scrollView_.frame.size.width;
                        float heightImage = (image.size.height*imgWidth)/image.size.width;
                        
                        
                        imgDeal.frame = CGRectMake(0, 0, imgWidth, heightImage);
                        
                        // [self loadImageFromURL1:str image:tblHeaderImage];
                        
                        // [tblHeaderImage setImageWithURL:[NSURL URLWithString:str] placeholder:nil];
                        
                        
                    }
                    
                    [scrollView_ addSubview:imgDeal];
                    [self setWithDict:dictDealDetails imageView:imgDeal];
                }
                
            }
            else{
            UIImageView *imgH = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"noimage.png"]];
            imgH.frame = CGRectMake(20, 30, 280, 140);
            // [vwLoading removeFromSuperview];
            
            [scrollView_ addSubview:imgH];
            [self setWithDict:dictDealDetails imageView:imgH];
            }

        }
        else
        {
            float imgWidth=scrollView_.frame.size.width;
            float heightImage = (imgDeal.image.size.height*imgWidth)/imgDeal.image.size.width;
            
            
            imgDeal.frame = CGRectMake(0, 0, imgWidth, heightImage);
            
         
            [scrollView_ addSubview:imgDeal];
            [self setWithDict:dictDealDetails imageView:imgDeal];
        }
        
        
        
        
        
        
        
    }
    else if ([arrays count]>1){
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 250)];
        scrollView.showsHorizontalScrollIndicator=NO;
        scrollView.showsVerticalScrollIndicator=NO;
        scrollView.delegate=self;
        scrollView.tag=10;
        scrollView.pagingEnabled = YES;
        int k =0;
        scrollArray=[[NSMutableArray alloc]init];
         AsyncImageView *copyOfFirstImgView;
        AsyncImageView *imgMainView;
        for (NSString *imageURLString in [dictDealDetails objectForKey:@"image"]) {
            [scrollArray addObject:imageURLString];
            
            
            imgMainView = [[AsyncImageView alloc] initWithFrame:CGRectMake(k*self.view.frame.size.width, 0, self.view.frame.size.width, 250)];
            k++;
            imgMainView.contentMode = UIViewContentModeScaleToFill;
            imgMainView.clipsToBounds = YES;
            NSString*str=imageURLString;
           
           
           
            NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
            SDImageCache *imageCache =[SDImageCache sharedImageCache];
            imageCache = [imageCache initWithNamespace:imageCacheFolder];
            imgMainView.image= [imageCache imageFromDiskCacheForKey:str];
            
            if(k==1)
            {
                NSInteger imgCount=[[dictDealDetails objectForKey:@"image"] count];
                
                copyOfFirstImgView= [[AsyncImageView alloc] initWithFrame:CGRectMake(imgCount*self.view.frame.size.width, 0, self.view.frame.size.width, 250)];
                //copyOfFirstImgView.backgroundColor=[UIColor greenColor];
                //copyOfFirstImgView=imgMainView;
                copyOfFirstImgView.image=imgMainView.image;
                [scrollView addSubview:copyOfFirstImgView];
                
            }
            
            if (!imgMainView.image)
            {
                if ([self reachable_dealDetail]) {
                    [imgMainView sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil options:SDWebImageRefreshCached];
                    imgMainView.imageURL = [NSURL URLWithString:str];
                    if(k==1)
                        
                    {
                        [copyOfFirstImgView sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil options:SDWebImageRefreshCached];
                    }
                    

                    
                }
           
            }
            
            
            [scrollView addSubview:imgMainView];
            
        }
        [scrollView setContentSize:CGSizeMake((k+1) * self.view.frame.size.width, 250)];
        [scrollView_ addSubview:scrollView];
        UIWebView*playerWeb;
        if(([[dictDealDetails objectForKey:@"type"]isEqualToString:@"guided tour"])){
            NSString*htmlString=[NSString stringWithFormat:@"<div style='width:304px;height:200px;text-align:center;'><audio controls><source src='%@' type='audio/mpeg'></audio></div>",[dictDealDetails objectForKey:@"mp3"]];
            playerWeb=[[UIWebView alloc]init];
            playerWeb.frame=CGRectMake(0, scrollView.frame.size.height+scrollView.frame.origin.y, imgMainView.frame.size.width, 50);
            playerWeb.scrollView.scrollEnabled=NO;
            playerWeb.opaque=NO;
            playerWeb.backgroundColor=[UIColor colorWithRed:213.0f/255.0f green:213/255.0f blue:213/255.0f alpha:1];
            [playerWeb loadHTMLString:htmlString baseURL:nil];
            [scrollView_ addSubview:playerWeb];
            
            pageControl=[[UIPageControl alloc]initWithFrame:CGRectMake(0, playerWeb.frame.size.height+playerWeb.frame.origin.y,16*[arrays count]+1, 20)];
        }
        else{
            pageControl=[[UIPageControl alloc]initWithFrame:CGRectMake(0, scrollView.frame.size.height+scrollView.frame.origin.y,16*[arrays count]+1, 20)];
        }
        pageControl.currentPage=0;
        
        pageControl.pageIndicatorTintColor = [UIColor grayColor];
        pageControl.currentPageIndicatorTintColor = UIColorFromRGB(0xffbb03);
        pageControl.backgroundColor=[UIColor clearColor];
        pageControl.numberOfPages=[arrays count];
        [scrollView_ addSubview:pageControl];
        
        
        [self setWithDict:dictDealDetails imageView:imgMainView];
        
        
        
    }
        else
        {
            UIImageView *imgH = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"noimage.png"]];
            imgH.frame = CGRectMake(20, 30, 280, 140);
            // [vwLoading removeFromSuperview];
            [scrollView_ addSubview:imgH];
            [self setWithDict:dictDealDetails imageView:imgH];
            NSLog(@"error");
        }
        
    }
     @catch (NSException * e)
    { UIImageView *imgH = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"noimage.png"]];
        imgH.frame = CGRectMake(20, 30, 280, 140);
       // [vwLoading removeFromSuperview];
        [scrollView_ addSubview:imgH];
        [self setWithDict:dictDealDetails imageView:imgH];
        NSLog(@"error");
      
    }
  // imgDeal.image= [imageCache imageFromDiskCacheForKey:[dictDealDetails objectForKey:@"image"]];
   /*
    if (!imgDeal.image)
    {
    
        if([self reachable_dealDetail]){
    
            [imgDeal sd_setImageWithURL:[NSURL URLWithString:[dictDealDetails objectForKey:@"image"]] placeholderImage:nil options:SDWebImageRefreshCached];
            
            [imgDeal setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[dictDealDetails objectForKey:@"image"]]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *imageX) {
                NSLog(@"success");
                NSLog(@"%f %f ",imageX.size.height,imageX.size.width);
                [imgDeal setImage:imageX];
                
                
                float imgWidth=scrollView_.frame.size.width;
                float heightImage = (imageX.size.height*imgWidth)/imageX.size.width;
                
                
                imgDeal.frame = CGRectMake(0, 0, imgWidth, heightImage);
                
                
                [scrollView_ addSubview:imgDeal];
                [self setWithDict:dictDealDetails imageView:imgDeal];
                
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                NSLog(@"fail with %@ and %@",[error localizedDescription],[request URL]);
                [self setWithDict:dictDealDetails imageView:imgDeal];
            }
             ];
        }
    }
    else
    {
        float imgWidth=scrollView_.frame.size.width;
        float heightImage = (imgDeal.image.size.height*imgWidth)/imgDeal.image.size.width;
        
        
        imgDeal.frame = CGRectMake(0, 0, imgWidth, heightImage);
        
        
        [scrollView_ addSubview:imgDeal];
        [self setWithDict:dictDealDetails imageView:imgDeal];
    }
    */
    
   
}
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    //if (!pageControlBeingUsed) {
    if(scrollView.tag==10){
        
        // Switch the indicator when more than 50% of the previous/next page is visible
        CGPoint offset = scrollView.contentOffset;
        CGSize size = scrollView.contentSize;
        
        //       CGPoint newXY = CGPointMake(size.width-600, size.height-480);
        CGFloat pageWidth = scrollView.frame.size.width;
        long int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        //       long int Currpage=pageControl.numberOfPages;
        //
        
        if (offset.x > (size.width - (320+0.1)))
        {
            [scrollView setContentOffset:CGPointMake(0, 0)];
            
            pageControl.currentPage = 0;
        }
        else
            pageControl.currentPage = page;
        /* if (offset.x > size.width - 320){
         
         }
         else*/
        
        /*
         else  if (offset.x < 0) {
         [scrollView setContentOffset:CGPointMake(Currpage*pageWidth-320, 0)];
         // [scrollView setContentOffset:CGPointMake(Currpage*pageWidth-320, 0) animated:YES];
         }*/
        
        
        
        
        
    }
}
-(void)setWithDict:(NSDictionary *)dictResult
         imageView:(UIImageView *) img
{
    if(scrollHeight==0){
    if (IS_IPHONE5)
        scrollHeight = 501;
    else
        scrollHeight = 361;
    }
    
    NSString *strName = [dictDealDetails objectForKey:@"deal_title"];
    NSString *strAddress = [dictDealDetails objectForKey:@"deal_subtitle"];
    NSString *strDesc = [dictDealDetails objectForKey:@"deal_description"];
    lblNavTitle.text = @"Deals";

    scrollView_.frame = CGRectMake(0, 68, 320, scrollHeight);
    CGSize maximumLabelSize = CGSizeMake(280, FLT_MAX);
    CGSize expectedLabelSize = [strName sizeWithFont:[UIFont fontWithName:@"Verdana-Bold" size:20] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByClipping];
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(20, img.frame.size.height + img.frame.origin.y + 20, 280, expectedLabelSize.height)];
    lblTitle.numberOfLines = 25;
    lblTitle.backgroundColor = [UIColor clearColor];
    [lblTitle setFont:[UIFont fontWithName:@"Verdana-Bold" size:20]];
    [scrollView_ addSubview:lblTitle];
    lblTitle.text = strName;
    
    if(isFavorite)
      lblTitle.text=@"Favorites";
    
    
    CGSize expectedSubTitleSize = [strAddress sizeWithFont:[UIFont fontWithName:@"Verdana" size:15] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByClipping];
    UILabel *lblSubtitle = [[UILabel alloc] initWithFrame:CGRectMake(20, lblTitle.frame.origin.y + lblTitle.frame.size.height + 5, 280, expectedSubTitleSize.height)];
    lblSubtitle.numberOfLines = 50;
    lblSubtitle.backgroundColor = [UIColor clearColor];
    [lblSubtitle setFont:[UIFont fontWithName:@"Verdana" size:15]];
    lblSubtitle.text = strAddress;
    [scrollView_ addSubview:lblSubtitle];
    
    btnPhone.frame = CGRectMake(20, lblSubtitle.frame.size.height + lblSubtitle.frame.origin.y + 5, 280, 30);
    btnWeb.frame = CGRectMake(20, btnPhone.frame.origin.y + 25, 280, 30);
    
    CGSize expectedContentSize = [strDesc sizeWithFont:[UIFont fontWithName:@"Verdana" size:14] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByClipping];
    UILabel *lblDesc = [[UILabel alloc] initWithFrame:CGRectMake(20, btnWeb.frame.origin.y + 30, 280, expectedContentSize.height+39)];
    
    strDesc =[strDesc stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
    
    strDesc =[NSString stringWithFormat:@"<font face=\"verdana\" size=\"4\">%@<font>",strDesc];
    
    NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc] initWithData:[strDesc dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
  
    
    lblDesc.numberOfLines = 50;
    lblDesc.backgroundColor = [UIColor clearColor];
    [lblDesc setFont:[UIFont fontWithName:@"Verdana" size:13]];
      lblDesc.attributedText = attrStr;
   [scrollView_ addSubview:lblDesc];
    
    
    
    [btnPhone setTitle:[dictDealDetails objectForKey:@"deal_phone"] forState:UIControlStateNormal];
    
    webAddress=[dictDealDetails objectForKey:@"deal_web"];
    
    
    [btnWeb setTitle:webAddress forState:UIControlStateNormal];
    btnFavorites.frame = CGRectMake(btnWeb.frame.origin.x, lblDesc.frame.origin.y + lblDesc.frame.size.height + 20, 25, 25);
    [scrollView_ addSubview:btnFavorites];
    [scrollView_ addSubview:btnWeb];
    [scrollView_ addSubview:btnPhone];
    [scrollView_ setContentSize:CGSizeMake(scrollView_.frame.size.width, btnFavorites.frame.origin.y+btnFavorites.frame.size.height + 100)];
    
    if (isFromFav) {
        lblNavTitle.text=@"Favorites";
        
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getDealDetails:(NSDictionary *) dictDetails
{
    dictDealDetails = dictDetails;
}
#pragma mark - Button actions
-(IBAction)onWebSite:(id)sender
{
   // UIButton *btn = (UIButton *) sender;
    NSString *urlstr = webAddress;
    if ([[urlstr substringToIndex:3] isEqualToString:@"www"]) {
        urlstr = [urlstr stringByReplacingOccurrencesOfString:@"www" withString:@"http://www"];
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlstr]];
}
-(IBAction)onCall:(id)sender
{
    UIButton *btn = (UIButton *) sender;
    NSString *phoneNumber = btn.titleLabel.text; // dynamically assigned
    NSString *phoneURLString = [NSString stringWithFormat:@"telprompt://%@", phoneNumber];
    phoneURLString = [phoneURLString stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"phone url string = %@",phoneURLString);
    NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
    [[UIApplication sharedApplication] openURL:phoneURL];
    
}

-(IBAction)onBack:(id)sender;
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)onFavorite:(id)sender
{
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSMutableArray *arrFavorites;
    if (!isFavorite) {
        isFavorite = YES;
        [btnFavorites setBackgroundImage:[UIImage imageNamed:@"starNoFav"] forState:UIControlStateNormal];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSMutableDictionary *plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
            arrFavorites = [plistDict objectForKey:@"favoritesArray"];
            if ([arrFavorites count] == 0) {
                arrFavorites = [NSMutableArray new];
                NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dictDealDetails];
                [dictToSave setValue:@"deal" forKey:@"type"];
                [arrFavorites addObject:dictToSave];
            }
            else {
                NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dictDealDetails];
                [dictToSave setValue:@"deal" forKey:@"type"];
                [arrFavorites insertObject:dictToSave atIndex:0];
            }
            [plistDict setObject:arrFavorites forKey:@"favoritesArray"];
            [plistDict writeToFile:finalPath atomically:YES];
        }
    }
    else {
        isFavorite = NO;
        [btnFavorites setBackgroundImage:[UIImage imageNamed:@"starFav.png"] forState:UIControlStateNormal];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSMutableDictionary *plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
            arrFavorites = [plistDict objectForKey:@"favoritesArray"];
            int k =0;
            for (NSDictionary *dict in arrFavorites) {
                if ([[dict objectForKey:@"Id"] isEqualToString:[dictDealDetails objectForKey:@"Id"]]&&[[dict objectForKey:@"deal_title"] isEqualToString:[dictDealDetails objectForKey:@"deal_title"]]) {
                    [arrFavorites removeObjectAtIndex:k];
                    break;
                }
                k++;
            }
            [plistDict setObject:arrFavorites forKey:@"favoritesArray"];
            [plistDict writeToFile:finalPath atomically:YES];
        }
    }
}

-(IBAction)onClick:(id)sender
{
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSMutableArray *arrFavorites;
    
    {
        isFavorite = NO;
        [btnFavorites setBackgroundImage:[UIImage imageNamed:@"starFav.png"] forState:UIControlStateNormal];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSMutableDictionary *plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
            arrFavorites = [plistDict objectForKey:@"favoritesArray"];
            int k =0;
            for (NSDictionary *dict in arrFavorites) {
                if ([[dict objectForKey:@"Id"] isEqualToString:[dictDealDetails objectForKey:@"Id"]]&&[[dict objectForKey:@"deal_title"] isEqualToString:[dictDealDetails objectForKey:@"deal_title"]]) {
                    [arrFavorites removeObjectAtIndex:k];
                    break;
                }
                k++;
            }
            [plistDict setObject:arrFavorites forKey:@"favoritesArray"];
            [plistDict writeToFile:finalPath atomically:YES];
        }
    }
}


- (IBAction)onDirectionNew:(id)sender {
    NSString *str;
    if ([dictDealDetails objectForKey:@"geometry"]) {
        NSDictionary *dict5 = [dictDealDetails objectForKey:@"geometry"];
        NSDictionary *dictLocation = [dict5 objectForKey:@"location"];
        str = [NSString stringWithFormat:@"%@,%@",[dictLocation objectForKey:@"lat"],[dictLocation objectForKey:@"lng"]];
    }
    else {
        str = [NSString stringWithFormat:@"%@,%@",[dictDealDetails objectForKey:@"latitude"],[dictDealDetails objectForKey:@"longitude"]];
    }
    
    NSLog(@"%f, %f  %@",location_updated.coordinate.latitude, location_updated.coordinate.longitude,str);
    NSString *urlstr = [NSString stringWithFormat:@"https://maps.google.com/?saddr=%f,%f&daddr=%@",location_updated.coordinate.latitude,location_updated.coordinate.longitude,str];
    [self.view addSubview:vwDirection];
    NSURL *url = [NSURL URLWithString:urlstr];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [dirWebvw loadRequest:requestObj];
}
- (IBAction)onCloseDir:(id)sender {
    [vwDirection removeFromSuperview];
}

-(BOOL)offlineAvailable
{
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"100Percent%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]])
    {
        return NO;
    }
    else
        return YES;
}

-(BOOL)reachable_dealDetail {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}
@end
