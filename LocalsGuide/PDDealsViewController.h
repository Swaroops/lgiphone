

#import <UIKit/UIKit.h>


@interface PDDealsViewController : UIViewController <GADBannerViewDelegate>
{
    GADBannerView *bannerView_;
    IBOutlet UILabel *titleLabel;
    IBOutlet UITableView *tblDeals;
    IBOutlet UIActivityIndicatorView *indiProgress;
    IBOutlet UIButton *backBtn;
}
@property (nonatomic, retain) CLLocationManager *locationManager;
-(IBAction)onBack:(id)sender;
-(void)getDictionarydeals:(NSDictionary *) dictHere;
@end
