
#import <UIKit/UIKit.h>

@interface PDEventDetailViewController : UIViewController <CLLocationManagerDelegate, GADBannerViewDelegate,UIScrollViewDelegate>
{
    GADBannerView *bannerView_;
    IBOutlet UILabel *lblNavTitle;
    IBOutlet UIScrollView *scrollView_;
    IBOutlet UIButton *btnFavorites;
}
@property (nonatomic, retain) CLLocationManager *locationManager;
-(void)getEventDetails:(NSDictionary *) dictDetails;
@property (nonatomic)BOOL isFromFav;
-(IBAction)onBack:(id)sender;
-(IBAction)onFavorite:(id)sender;
-(IBAction)onClick:(id)sender;
@end
