
#import <UIKit/UIKit.h>

@interface PDEventsCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *lblName;
@property (nonatomic, retain) IBOutlet UILabel *lblTime;
@property (nonatomic, retain) IBOutlet UILabel *lblPlace;
@property (nonatomic, retain) IBOutlet UILabel *lblDesc;
@property (nonatomic, retain) IBOutlet UIImageView *imgEvent;
@end
