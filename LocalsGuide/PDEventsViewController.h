
#import <UIKit/UIKit.h>


@interface PDEventsViewController : UIViewController <GADBannerViewDelegate>
{
    GADBannerView *bannerView_;
    IBOutlet UILabel *titleLabel;
    IBOutlet UITableView *tblEvents;
    IBOutlet UIActivityIndicatorView *indiProgress;
    IBOutlet UIButton *backBtn;
    
    
    IBOutlet UIView *vwSettings;
    BOOL flagMenu;
    
    IBOutlet UIView *backgroundView;
    
    NSTimer *timm;
    
    NSInteger veryFirst;
    NSMutableArray *evenCatArr;
    IBOutlet UITableView *eventListTbl;
    
    NSString *classIds;
    UILabel *noDataLbl;
}

@property (nonatomic)BOOL isFromFav;
@property (nonatomic, retain) CLLocationManager *locationManager;
-(IBAction)onBack:(id)sender;
-(void)getDictionaryeve:(NSDictionary *) dictHere;
@end
