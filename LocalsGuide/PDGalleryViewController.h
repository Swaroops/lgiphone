//
//  PDGalleryViewController.h
//  Visiting Ashland
//
//  Created by swaroop on 03/07/15.
//  Copyright (c) 2015 iDeveloper. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDGalleryViewController : UIViewController<UIAlertViewDelegate>
{
    
}
-(void)getDictionary:(NSDictionary *) dict;
@property (weak, nonatomic) IBOutlet UILabel *healdingLbl;
@property(nonatomic,strong) IBOutlet UIImageView *backgroundImgv;
@end
