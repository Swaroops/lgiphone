
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface PDListViewController : UIViewController <CLLocationManagerDelegate>
{
    UITableView *tblList;
    IBOutlet UIImageView *imgTop;
    IBOutlet UIView *vwLoading;
    IBOutlet UIView *vwNotAvailable;
    IBOutlet UIButton *btnBack;
    GADBannerView *bannerView_;
    UIButton *fShare, *fPage, *tPage, *about;
    IBOutlet UILabel *lblTitleName;
   
    IBOutlet UIButton *mapBtn;
}
@property (nonatomic, retain) CLLocationManager *locationManager;

@property (nonatomic)BOOL isFromFavorite;
@property (nonatomic)BOOL isFromFav;

@property(nonatomic) BOOL *isSomethingEnabled;
@property(nonatomic) BOOL *isrefreshPull;

@property(nonatomic) BOOL *isfromVC;

-(IBAction)onBack:(id)sender;
-(void)getDictionary:(NSDictionary *) dictHere;
-(void)fixedWebServiceResponsee:(NSData *) responseData;
@end
