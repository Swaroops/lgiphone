

#import "PDListViewController.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "Reachability.h"
#import <QuartzCore/QuartzCore.h>
#import "PDAppDelegate.h"
#import "Businesses.h"
#import "UIImageView+WebCache.h"
#import "PDDetailViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "PDIntermediatePageViewController.h"
#import "UIImageView+JMImageCache.h"


@interface PDListViewController () <GADBannerViewDelegate, UITableViewDataSource, UITableViewDelegate>
{
    float tblHeight;
    NSMutableArray *arraySmpl;
    BOOL isLocationUpdated, isHotSpotAd, isGoogle;
    int banery;
    NSInteger fixedArrayCount;
    
    
    UIButton *buttonImg;
    UIImage *btnImage;
    UIImage *btnImageSel;
    
    BOOL isFavorite;
    
    UIRefreshControl *refreshControl;
    NSMutableArray *guidesNameArray;
    
    BOOL appName;
    
    NSString *strDistan;
    
    UIView *loaderView;
    UIImageView *splashSearch;
    NSInteger *indexe;
}
@end
@implementation PDListViewController
@synthesize isFromFavorite,isFromFav;
CLLocation *location_updated;
NSString *adLink;
BOOL isCategoryAd;
NSString *categoryAdImage, *categoryAdUrl;
NSDictionary *dictDetails;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}
-(BOOL)reachable1 {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}

-(BOOL)offlineAvailable
{
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]);
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"100Percent%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]])
    {
        return NO;
    }
    else
        return YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (void)testRefresh2:(UIRefreshControl *)refreshControlH
{
    _isrefreshPull = YES;
    
    if ([self reachable1])
        [refreshControl endRefreshing];
    // [self startFixedService];
    else
        [refreshControl endRefreshing];
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
   
    
    
    splashSearch = [[UIImageView alloc]init];
    
    splashSearch.frame=self.view.frame;
    
    
    
    appName=NO;
    
    isGoogle = NO;
    self.navigationController.navigationBarHidden = YES;
    imgTop = [[AsyncImageView alloc] init];
    
    if (IS_IPHONE5) {
        [[NSBundle mainBundle] loadNibNamed:@"PDListViewController" owner:self options:nil];
        
        banery=515;
        tblHeight = 455;
        
        
    }
    else {
        [[NSBundle mainBundle] loadNibNamed:@"PDListViewController4" owner:self options:nil];
        //banery = 382;
        banery=427;
        tblHeight = 361;
    }
    
    
    tblList = [[UITableView alloc] initWithFrame:CGRectMake(0, 67, 320, tblHeight)];
    
    
    
   
    CGRect frame=tblList.frame;
    frame.size.height=self.view.frame.size.height-self.navigationController.navigationBar.frame.size.height-21;
    tblList.frame = frame;
    
    NSLog(@"tblList frame : %@",NSStringFromCGRect(tblList.frame));
    
    tblList.backgroundColor = [UIColor whiteColor];//clearColor
    [self.view addSubview:tblList];
    tblList.dataSource = self;
    tblList.delegate = self;
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(testRefresh2:) forControlEvents:UIControlEventValueChanged];
    [tblList addSubview:refreshControl];
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = 250;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if (IS_OS_8_OR_LATER){
        [_locationManager requestWhenInUseAuthorization];}
    [_locationManager startUpdatingLocation];
    
    
    NSString*string = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"skipIntermediate%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
    
    NSLog(@"acurrentAppId :%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]);
    
    if (![self offlineAvailable]&&[self reachable1]) {
        [self startFixedService];
    }
    else if(_isSomethingEnabled && !_isfromVC &&[self reachable1])
    {
        [self startFixedService];
    }
    else if(_isfromVC && ![string isEqualToString:@"skipIntermediate"] &&[self reachable1])
    {
        [self startFixedService];
    }
    else
    {
        [self coreDataCodeRead_bus];
        
        [self testingcoredataread_bus];
    }
    //    [self startFixedService];
    NSMutableArray* myMutableArrayAgain = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"footerListArray"]];
    for (NSDictionary *dict in myMutableArrayAgain) {
        if ([[dict objectForKey:@"name"] isEqualToString:self.tabBarItem.title]) {
            NSLog(@"%@",dict);
            [self getDictionary:dict];
            btnBack.hidden = NO;
        }
    }
    lblTitleName.text = [dictDetails objectForKey:@"name"];
    
    if (!(lblTitleName.text == (id)[NSNull null] || lblTitleName.text.length == 0))
        
        [[NSUserDefaults standardUserDefaults] setObject:lblTitleName.text forKey:@"lbltitleMap"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if (!([[dictDetails objectForKey:@"listing_icon"] isEqualToString:@"FALSE"]||[[dictDetails objectForKey:@"listing_icon"] isEqualToString:@""]))
        imgTop.imageURL = [NSURL URLWithString:[dictDetails objectForKey:@"listing_icon"]];
    
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
    if ([self reachable1])
        if(![removAdStatus isEqualToString:@"purchased"])
            [self showAd];
    
    
    if(![self reachable1])
    {
        
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        NSData *data = [def objectForKey:@"offlineresultArray"];
        NSMutableArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        NSMutableArray *arr = [[NSMutableArray alloc]init];
        
        if([retrievedDictionary isKindOfClass:[NSDictionary class]])
        {
            
            
        }
        else
        {
           
        }
        
        NSDictionary* offData= [retrievedDictionary objectAtIndex:1];
        
        
        
        NSDictionary * bannerdict= [[NSUserDefaults standardUserDefaults] objectForKey:@"HomeResponse"];
        
        NSLog(@"bannerdict : %@",bannerdict);
        
        
        NSLog(@"offData : %@",offData);
        
        NSString*detailid = [dictDetails objectForKey:@"Id"];
        
        for(NSDictionary *dictDetailss in offData)
        {
            if([detailid isEqualToString:[NSString stringWithFormat:@"%@",[dictDetailss objectForKey:@"Id"]]])
            {
                
                if(![[dictDetailss objectForKey:@"fav_icon"] isEqualToString:@""])
                {
                    if([[dictDetailss objectForKey:@"fav_icon"] isEqualToString:@"heart"])
                    {
                        btnImage= [UIImage imageNamed:@"1.png"];
                        
                        btnImageSel= [UIImage imageNamed:@"2.png"];
                        
                    }
                    
                    else
                    {
                        btnImage= [UIImage imageNamed:@"star.png"];
                        
                        btnImageSel= [UIImage imageNamed:@"star_sel.png"];
                    }
                }
                else
                {
                    //test
                    // str =@"star";
                    //
                    NSString * str=[[NSUserDefaults standardUserDefaults]stringForKey:@"fav_icon"];
                    
                    if([str isEqualToString:@"star"])
                    {
                        btnImage= [UIImage imageNamed:@"star.png"];
                        
                        btnImageSel= [UIImage imageNamed:@"star_sel.png"];
                        
                    }
                    else
                    {
                        btnImage= [UIImage imageNamed:@"1.png"];
                        
                        btnImageSel= [UIImage imageNamed:@"2.png"];
                        
                        
                    }
                }
                
                
                
            }
            
        }
        
    }
    
    if(_isSomethingEnabled)
    {
        lblTitleName.text = @"Results";
        
        guidesNameArray = [[NSUserDefaults standardUserDefaults]objectForKey:@"guidesName"];
        
        mapBtn.hidden=YES;
        
        
        
        if (IS_IPHONE5) {
            
        }
        else {
            
        }
        
        if(_isSomethingEnabled)
        {
           
            
            CGRect frame=tblList.frame;
            frame.size.height=self.view.frame.size.height-self.navigationController.navigationBar.frame.size.height-21;
            
            
            tblList.frame = frame;
            
            
            NSLog(@"tbllist frame :%@",NSStringFromCGRect(tblList.frame));
            
        }
        
        
    }
    
}

- (IBAction)mapAction:(id)sender {
    
    PDMapViewController *viewController = [[PDMapViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:NO];
}

-(void)viewWillAppear:(BOOL)animated
{
    [splashSearch removeFromSuperview];
    
    [tblList reloadData];
    
   
}
-(void)showAd
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CustomAd"] isEqualToString:@"NO"]) {
        [self setAdbannerWithKey:[[NSUserDefaults standardUserDefaults] objectForKey:@"admob_id"]];
    }
    else {
        
        NSDictionary *dictHotspot = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HotSpotAd"];
        if (isHotSpotAd && (![[dictHotspot objectForKey:@"image_listing"] isEqualToString:@"FALSE"])) {
            
            AsyncImageView *ad1Image = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, banery, 320, 50)];
            [self.view addSubview:ad1Image];
            UIButton *ad1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
            ad1Btn.frame = CGRectMake(0, banery, 320, 50);
            [ad1Btn addTarget:self action:@selector(loadAd1) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:ad1Btn];
            
            
            NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HotSpotAd"];
            ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"image_listing"]];
            adLink = [dict objectForKey:@"link_listing"];
            if (IS_IPHONE5)
                tblHeight = 398;
            else
                tblHeight = 311;
            tblList.frame = CGRectMake(0, 67, 320, tblHeight);
        }
        else {
            if([[[NSUserDefaults standardUserDefaults] objectForKey:@"CatAdExist"] isEqualToString:@"YES"])
            {
                NSDictionary *dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"CatAd"];
                if (![[dict objectForKey:@"imageLink"] isEqualToString:@"NA"]) {
                    AsyncImageView *ad1Image = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, banery, 320, 50)];
                    [self.view addSubview:ad1Image];
                    UIButton *ad1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
                    ad1Btn.frame = CGRectMake(0, banery, 320, 50);
                    [ad1Btn addTarget:self action:@selector(loadAd1) forControlEvents:UIControlEventTouchUpInside];
                    [self.view addSubview:ad1Btn];
                    
                    ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"imageLink"]];
                    adLink = [dict objectForKey:@"webLink"];
                    if (IS_IPHONE5)
                        tblHeight = 398;
                    else
                        tblHeight = 311;
                    tblList.frame = CGRectMake(0, 67, 320, tblHeight);
                }
            }
            else {
                NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"CustomAdDict"];
                if (![[dict objectForKey:@"ad_2"] isEqualToString:@"NA"]) {
                    AsyncImageView *ad1Image = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, banery, 320, 50)];
                    [self.view addSubview:ad1Image];
                    UIButton *ad1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
                    ad1Btn.frame = CGRectMake(0, banery, 320, 50);
                    [ad1Btn addTarget:self action:@selector(loadAd1) forControlEvents:UIControlEventTouchUpInside];
                    [self.view addSubview:ad1Btn];
                    
                    ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"ad_2"]];
                    adLink = [dict objectForKey:@"link_2"];
                    if (IS_IPHONE5)
                        tblHeight = 398;
                    else
                        tblHeight = 311;
                    tblList.frame = CGRectMake(0, 67, 320, tblHeight);
                }
            }
        }
    }
    
    NSLog(@"tbllist frame showad :%@",NSStringFromCGRect(tblList.frame));
    
    if(_isSomethingEnabled)
    {
        // tblHeight = 756;
        // banery = 470;
        
        CGRect frame=tblList.frame;
        frame.size.height=self.view.frame.size.height-self.navigationController.navigationBar.frame.size.height-21;
        
        //  frame.size.height=756;
        tblList.frame = frame;
        
        
        NSLog(@"tbllist frame :%@",NSStringFromCGRect(tblList.frame));
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)getDictionary:(NSDictionary *) dictHere
{
    dictDetails = dictHere;
    
    
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    isLocationUpdated = YES;
    location_updated = [locations lastObject];
    isHotSpotAd = NO;
    NSMutableArray *arrHotSpots = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"HotSpotArray"]];
    for (NSDictionary *dictDetails in arrHotSpots) {
        CLLocation *targetLocation = [[CLLocation alloc] initWithLatitude:[[dictDetails objectForKey:@"lat"] doubleValue] longitude:[[dictDetails objectForKey:@"long"] doubleValue]];
        CLLocationDistance maxRadius = [[dictDetails objectForKey:@"radius"] floatValue]; // in meters
        isHotSpotAd = ([location_updated distanceFromLocation:targetLocation] <= maxRadius)?YES:NO;
        if (isHotSpotAd) {
            [[NSUserDefaults standardUserDefaults] setObject:dictDetails forKey:@"HotSpotAd"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
        }
    }
    
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
    if ([self reachable1])
        if(![removAdStatus isEqualToString:@"purchased"])
            [self showAd];
}
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
    if ([self reachable1])
        if(![removAdStatus isEqualToString:@"purchased"])
            [self showAd];
}
#pragma mark - Setting ad
-(void)setAdbannerWithKey:(NSString *) key
{
    bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    bannerView_.adUnitID = key;
    bannerView_.delegate = self;
    bannerView_.rootViewController = self;
    [bannerView_ loadRequest:[GADRequest request]];
    bannerView_.frame = CGRectMake(0, banery, bannerView_.frame.size.width, bannerView_.frame.size.height);
    
    if(!_isSomethingEnabled)
    {

        [self.view addSubview:bannerView_];
        
    }
}
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    if (IS_IPHONE5)
        tblHeight = 448;
    else
        tblHeight = 361;
    tblList.frame = CGRectMake(0, 67, 320, tblHeight);
    
    NSLog(@"tbllist frame adViewDidReceiveAd :%@",NSStringFromCGRect(tblList.frame));
    
    
    if(_isSomethingEnabled)
    {
        // tblHeight = 756;
        // banery = 470;
        
        CGRect frame=tblList.frame;
        frame.size.height=self.view.frame.size.height-self.navigationController.navigationBar.frame.size.height-21;
        
        //  frame.size.height=756;
        tblList.frame = frame;
        
        
        NSLog(@"tbllist frame :%@",NSStringFromCGRect(tblList.frame));
    }

}
-(void)loadAd1
{
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:adLink]];
}
#pragma mark - alert retry
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [self startFixedService];
    }
    if(alertView.tag==10057)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
#pragma mark - Web service
-(void)startFixedService
{
    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(fixedWebServiceResponsee:);
    
    NSString *lat= [[NSUserDefaults standardUserDefaults]objectForKey:@"doubleLatitude"];
    NSString *lon= [[NSUserDefaults standardUserDefaults]objectForKey:@"doubleLongitude"];
    NSLog(@"lat : %@ , long : %@",lat,lon);
    
    
    NSString *dist;
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"] isEqualToString:@"M"])
    {
        dist=@"M";
    }
    else{
        dist=@"K";
    }
    
    
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@%@&lat=%f&long=%f",LIST_URL,[dictDetails objectForKey:@"Id"],location_updated.coordinate.latitude,location_updated.coordinate.longitude]);
    
    [webService startParsing:[NSString stringWithFormat:@"%@%@&lat=%@&long=%@&distance_in=%@",LIST_URL,[dictDetails objectForKey:@"Id"],lat,lon,dist]];
    
  
    
    arraySmpl = [[NSMutableArray alloc] init];
    vwLoading.frame = CGRectMake(0, 65, 320, vwLoading.frame.size.height);
    vwLoading.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:vwLoading];
    
    if(_isrefreshPull)
    {
        [vwLoading removeFromSuperview];
        _isrefreshPull=NO;
    }
}
-(void)startGoogleWebService
{
    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(webServiceResponse:);
    NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%f,%f&rankby=distance&types=%@&sensor=false&key=%@",location_updated.coordinate.latitude,location_updated.coordinate.longitude,[dictDetails objectForKey:@"googletype"], GOOGLE_API_KEY];
    [webService startParsing:strUrl];
    isGoogle = YES;
}
-(void)fixedWebServiceResponsee:(NSData *) responseData
{
    [vwLoading removeFromSuperview];
    [refreshControl endRefreshing];
    [vwNotAvailable removeFromSuperview];
    NSError *e;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options: NSJSONReadingMutableContainers error: &e];
    NSArray *arrayResult;
    if(_isSomethingEnabled)
    {
        
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        NSData *data = [def objectForKey:@"searchData"];
        NSDictionary *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        NSDictionary *json = [[NSDictionary alloc] initWithDictionary:retrievedDictionary];
        
      
        
        arrayResult =[json objectForKey:@"business"];
        arraySmpl = [NSMutableArray arrayWithArray:arrayResult];
        fixedArrayCount = [arraySmpl count];
        
        NSString *keySer =[[NSUserDefaults standardUserDefaults]objectForKey:@"keywrd"];
        int i=0;
        NSMutableDictionary *strA=[NSMutableDictionary dictionary];
        
        NSMutableArray *arrMain = [[NSUserDefaults standardUserDefaults]objectForKey:@"guidesName"];
        for(NSString *str in arrMain)
        {
            BOOL isTheObjectThere;
            isTheObjectThere=false;
            NSArray *myArray = [[arrMain objectAtIndex:i] valueForKey:@"keywords"];
            
            if(![myArray isEqual:@""])
            {
                
                for(NSString *strArr in myArray)
                {
                    
                    NSString *strArr2 =   [strArr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    
                    
                    
                    NSLog(@"strArr :%@ keySer :%@",strArr2,keySer);
                    
                    if([strArr2 caseInsensitiveCompare:keySer] == NSOrderedSame)
                    {
                        NSLog(@"Matches");
                        isTheObjectThere=true;
                        break;
                    }
                }
                //isTheObjectThere = [myArray containsObject:keySer];
            }
            
            
            NSString *strAppname = [[arrMain objectAtIndex:i] valueForKey:@"app_name"];
            if([strAppname isEqualToString:@"Mount Shasta"])
                NSLog(@"strAppname :%@",strAppname);
            
            
            if([keySer caseInsensitiveCompare:strAppname]== NSOrderedSame||isTheObjectThere)//ignores casesensitive
            {
                strA =[arrMain objectAtIndex:i];
                NSLog(@"strA :%@",strA);
                
                [[NSUserDefaults standardUserDefaults]setObject:strA forKey:@"forspplashimageurlfromsearchtop"];
                
                break;
            }
            i++;
        }
        
        if([strA count]>1)
        {
            
            if(!_isfromVC)
            {
                
                appName=YES;
                
                
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                dict= [strA mutableCopy];
                
                [dict setObject: [dict objectForKey: @"app_name"] forKey: @"name"];
                [dict removeObjectForKey: @"app_name"];
                [dict removeObjectForKey: @"splash_image"];
                
                NSMutableDictionary *dict2 = [NSMutableDictionary dictionary];
                dict2= [dict mutableCopy];
                
                
                [dict setObject:dict2 forKey: @"test"];
                
                // ... and later ...
                
                id something = [dict objectForKey:@"Some Key"];
                NSLog(@"something :%@",something);
                
                
                
                NSMutableArray *ar = [[NSMutableArray alloc]init];
                ar=[dict objectForKey:@"test"];
                
                [arraySmpl insertObject:ar atIndex:0];
                
                //  arraySmpl = [NSMutableArray arrayWithArray:ar];
                fixedArrayCount = [arraySmpl count];
                
                [[NSUserDefaults standardUserDefaults]setObject:ar forKey:@"loadingsplashSearch"];
                NSLog(@"arraySmpl search :%@",arraySmpl);
            }
        }
        
        
       
        
    }
    else{
        
        arrayResult =[dict objectForKey:@"business"];
        arraySmpl = [NSMutableArray arrayWithArray:arrayResult];
        fixedArrayCount = [arraySmpl count];
        
    }
    
    if([[dictDetails objectForKey:@"type"] isEqualToString:@"location"])
        [self startGoogleWebService];
    else
    {
        
        int64_t delayInSeconds = 0.6;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            [tblList reloadData];
            
        });
        
        
        
    }
    
   
}

-(void)webServiceResponse:(NSData *) responseData
{
    [vwLoading removeFromSuperview];
    [vwNotAvailable removeFromSuperview];
    NSError *e;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options: NSJSONReadingMutableContainers error: &e];
    NSArray *arrayResult;
    arrayResult =[dict objectForKey:@"results"];
    [refreshControl endRefreshing];
    /*  if([arrayResult count]>0)
     [self coreDataCodeWrite_bus:arrayResult];*/
    [arraySmpl addObjectsFromArray:arrayResult];
    if ([arraySmpl count] < 1) {
        vwNotAvailable.frame = CGRectMake(0, 50, 320, vwNotAvailable.frame.size.height);
        [self.view addSubview:vwNotAvailable];
        return;
    }
    
    int64_t delayInSeconds = 0.6;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        [tblList reloadData];
        
    });
    // [tblList reloadData];
    
    if (([[dictDetails objectForKey:@"listing_icon"] isEqualToString:@"FALSE"]||[[dictDetails objectForKey:@"listing_icon"] isEqualToString:@""]))  {
        NSDictionary *dict5 = [arrayResult objectAtIndex:0];
        imgTop.imageURL = [NSURL URLWithString:[dict5 objectForKey:@"icon"]];
    }
}
-(IBAction)onBack:(id)sender
{
    
    
    int i;
    
    for (i=0; i<[self.tabBarController.tabBar.items count]; i++)
    {
        
        NSLog(@"title2 :%@",[self.tabBarController.tabBar.items objectAtIndex:i].title );
        NSString *title =[self.tabBarController.tabBar.items objectAtIndex:i].title;
        if([title isEqualToString:@"Home"])
        {
            [self.tabBarController setSelectedIndex:i];
            break;
            
        }
        
    }
    
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSString *)extractNumberFromText:(NSString *)text
{
    NSCharacterSet *nonDigitCharacterSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    return [[text componentsSeparatedByCharactersInSet:nonDigitCharacterSet] componentsJoinedByString:@""];
}

#pragma mark - UITableView delegate and datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == [arraySmpl count])
        return 20;
    return 100;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isGoogle) {
        return [arraySmpl count] + 1;
    }
    return [arraySmpl count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
    }
    if (indexPath.row == [arraySmpl count])
    {
        UIImageView *imgv = [[UIImageView alloc] initWithFrame:CGRectMake(108, 0, 104, 16)];
        imgv.image = [UIImage imageNamed:@"poweredbygoogle.png"];
        [cell.contentView addSubview:imgv];
        return cell;
    }
    
    if([arraySmpl count]<1)
    {
        
    }
    else
    {
        if(indexPath.row==0&&_isSomethingEnabled)
        {
            NSLog(@"row index 0");
            // NSDictionary *dict = [dictAllValues objectAtIndex:indexPath.row];
        }
        
        
        NSString*string = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"skipIntermediate%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
        
        if(_isSomethingEnabled &&[string isEqualToString:@"skipIntermediate"])
        {
            
            
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"dist_offline"  ascending:YES];
            NSArray *arrtemp3 = [arraySmpl sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
            // NSArray *arrVals = [arrtemp3 copy];
            arraySmpl=[NSMutableArray arrayWithArray:arrtemp3];
            
        }
        
        
        
        NSDictionary *dict = [arraySmpl objectAtIndex:indexPath.row];
        UIImageView *imgCellBg = [[UIImageView alloc] initWithFrame:CGRectMake(5, 2, 310, 96)];
        imgCellBg.image = [UIImage imageNamed:@"listbg.png"];
        [cell.contentView addSubview:imgCellBg];
        
        UIImageView *imgPlaceholder = [[UIImageView alloc] initWithFrame:CGRectMake(14, 19, 82, 62)];
        imgPlaceholder.image = [UIImage imageNamed:@"listPlaceholder.png"];
        //[cell.contentView addSubview:imgPlaceholder];
        if (indexPath.row < fixedArrayCount) {
            if (![[dict objectForKey:@"star_rating"] isEqualToString:@"no"]) {
                UIImageView *imgRating = [[UIImageView alloc] initWithFrame:CGRectMake(100, 73, 50, 8)];
                imgRating.image = [UIImage imageNamed:@"5star.png"];
                [cell.contentView addSubview:imgRating];
                
                if(indexPath.row==0&&_isSomethingEnabled&&appName)
                {
                    if(!_isfromVC)
                    {
                        CGRect frame=CGRectZero;
                        frame=imgRating.frame;
                        frame.origin.y=frame.origin.y-17;
                        imgRating.frame=frame;
                    }
                }
                
            }
        }
        if ([dict objectForKey:@"thumb"]) {
            if (![[dict objectForKey:@"thumb"] isEqualToString:@""]) {
                AsyncImageView *img = [[AsyncImageView alloc] init];
                // UIImageView *img = [[UIImageView alloc] init];
                img.frame = CGRectMake(8.5, 6, 88, 88);
                NSString*str=[dict objectForKey:@"thumb"];
               
                
                NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
                SDImageCache *imageCache =[SDImageCache sharedImageCache];
                imageCache = [imageCache initWithNamespace:imageCacheFolder];
                
                
                img.image=[imageCache imageFromDiskCacheForKey:str];
                
                if (!img.image)
                {
                    if (![self offlineAvailable]&& !_isSomethingEnabled)
                    {
                        
                        [img sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage:nil options:SDWebImageRefreshCached];
                        
                        img.imageURL = [NSURL URLWithString:str];
                        
                    }
                    
                    else
                        
                    {
                        
                        NSString *appNameFolder = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
                        
                        NSArray* foo = [appNameFolder componentsSeparatedByString:@".zip"];
                        
                        NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
                        
                        
                        
                        
                        NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];
                        
                        NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];
                        
                        NSLog(@"firstBit folder :%@",firstBit);
                        NSLog(@"secondBit folder :%@",secondBit2);
                        
                        NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
                        
                        NSLog(@"str url :%@",str);
                        
                        if(!secondBit2)
                        {
                            //guide_name //guide_id
                            NSString *namevalue = [dict objectForKey:@"guide_name"];
                            NSString *idvalue = [dict objectForKey:@"guide_id"];
                            
                            secondBit2=[NSString stringWithFormat:@"%@_images_%@",namevalue,idvalue];
                            
                            NSLog(@"secondBit2 2:%@",secondBit2);
                        }
                        
                        NSString *filePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/storeimages/thumb",secondBit2]];
                        
                        NSArray* str3 = [str componentsSeparatedByString:@"storeimages/thumb/"];
                        
                        NSString* firstBit3 = [str3 objectAtIndex:0]; NSString* secondBit3 = [str3 objectAtIndex:1];
                        
                        NSString *decoded = [secondBit3 stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                        
                        NSLog(@"firstBit folder :%@",firstBit3);
                        NSLog(@"secondBit folder :%@",secondBit3);
                        NSLog(@"decoded folder :%@",decoded);
                        
                        NSString* Path = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",decoded]];
                        
                        UIImage* image = [UIImage imageWithContentsOfFile:Path];
                        
                        img.image=image;
                        
                       
                        
                    }
                }
                
                img.layer.cornerRadius = 6.0;
                img.layer.masksToBounds = YES;
                
                [cell.contentView addSubview:img];
            }
            else {
                UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"noimage.png"]];
                img.frame = CGRectMake(8.5, 6, 88, 88);
                img.layer.cornerRadius=6.0;
                img.layer.masksToBounds = YES;
                [cell.contentView addSubview:img];
            }
        }
        else {
#define IMAGE_VIEW_TAG 99
            UIImageView *img = [[AsyncImageView alloc] init];
            img.frame = CGRectMake(8.5, 6, 88, 88);
            if ([dict objectForKey:@"icon"]) {
                if (([[dictDetails objectForKey:@"listing_icon"] isEqualToString:@"FALSE"]||[[dictDetails objectForKey:@"listing_icon"] isEqualToString:@""]))
                    img.imageURL = [NSURL URLWithString:[dict objectForKey:@"icon"]];
                else
                    img.imageURL = [NSURL URLWithString:[dictDetails objectForKey:@"listing_icon"]];
            }
            else{
                
                NSString *str =[dict objectForKey:@"image"];
                
                if([self reachable1]){
                    
                    
                    if(_isSomethingEnabled){
                        
                        NSArray *arr=[dict valueForKey:@"image"];
                        
                        
                        if((arr == nil || [arr isEqual:@""]))
                        {
                            img.image=[UIImage imageNamed:@"image1111.png"];
                            img.layer.cornerRadius=6.0;
                            img.layer.masksToBounds = YES;
                        }
                        else{
                            if(indexPath.row==0&&_isSomethingEnabled&&appName)
                            {
                                if(!_isfromVC)
                                {
                                    img.imageURL = [NSURL URLWithString:arr];
                                    img.frame = CGRectMake( 8.5, 6, 90, 90);
                                }
                            }
                            else
                                img.imageURL = [NSURL URLWithString:[arr objectAtIndex:0]];
                        }
                        
                       
                    }
                    
                    else if(![self offlineAvailable])
                    {
                        img.imageURL = [NSURL URLWithString:[dict objectForKey:@"image"]];
                    }
                    else
                    {
                        {
                            
                            NSString *appNameFolder = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
                            
                            NSArray* foo = [appNameFolder componentsSeparatedByString:@".zip"];
                            
                            NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
                            
                            
                            
                            
                            NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];
                            
                            NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];
                            
                            NSLog(@"firstBit folder :%@",firstBit);
                            NSLog(@"secondBit folder :%@",secondBit2);
                            
                            NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
                            
                            NSLog(@"str url :%@",str);
                            
                            NSString *filePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/storeimages/thumb",secondBit2]];
                            
                            NSArray* str3 = [str componentsSeparatedByString:@"storeimages/thumb/"];
                            
                            NSString* firstBit3 = [str3 objectAtIndex:0]; NSString* secondBit3 = [str3 objectAtIndex:1];
                            
                            NSString *decoded = [secondBit3 stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                            
                            NSLog(@"firstBit folder :%@",firstBit3);
                            NSLog(@"secondBit folder :%@",secondBit3);
                            NSLog(@"decoded folder :%@",decoded);
                            
                            NSString* Path = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",decoded]];
                            
                            UIImage* image = [UIImage imageWithContentsOfFile:Path];
                            
                            img.image=image;
                       
                            
                        }
                        
                        
                    }
                }
                else{
                   
                    
                    
                    @try {
                        
                        NSString *appNameFolder = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
                        
                        NSArray* foo = [appNameFolder componentsSeparatedByString:@".zip"];
                        
                        NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
                        
                        
                        
                        
                        NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];
                        
                        NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];
                        
                        NSLog(@"firstBit folder :%@",firstBit);
                        NSLog(@"secondBit folder :%@",secondBit2);
                        
                        NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
                        
                        NSLog(@"str url :%@",str);
                        
                        NSString *filePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/storeimages/thumb",secondBit2]];
                        
                        NSArray* str3 = [str componentsSeparatedByString:@"storeimages/thumb/"];
                        
                        NSString* firstBit3 = [str3 objectAtIndex:0]; NSString* secondBit3 = [str3 objectAtIndex:1];
                        
                        NSString *decoded = [secondBit3 stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                        
                        NSLog(@"firstBit folder :%@",firstBit3);
                        NSLog(@"secondBit folder :%@",secondBit3);
                        NSLog(@"decoded folder :%@",decoded);
                        
                        NSString* Path = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",decoded]];
                        
                        UIImage* image = [UIImage imageWithContentsOfFile:Path];
                        
                        img.image=image;
                        
                       
                    } @catch (NSException *exception) {
                        
                        
                        SDImageCache *imageCache =[SDImageCache sharedImageCache];
                        imageCache = [imageCache initWithNamespace:@"JMCache"];
                        
                        UIImageView*imagevw=[[UIImageView alloc]init];
                        imagevw.image=[imageCache imageFromDiskCacheForKey:str];
                        
                        NSString *string = [self extractNumberFromText:str];
                        
                        //  guideImage.image= imagevw.image;
                        
                        NSString *cacheDir = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
                        
                        NSLog(@"str url :%@",str);
                        
                        NSString *filePath = [cacheDir stringByAppendingPathComponent:[NSString stringWithFormat:@"JMCache"]];
                        
                        NSString* Path = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",string]];
                        
                        UIImage* image = [UIImage imageWithContentsOfFile:Path];
                        
                        img.image=image;
                        // guideImage.image=image;
                        
                        
                        
                    }
                    
                    
                }
            }
            img.tag = IMAGE_VIEW_TAG;
            img.layer.cornerRadius = 6.0;
            img.layer.masksToBounds = YES;
            [cell.contentView addSubview:img];
        }
        UILabel *titlelabel = [[UILabel alloc] init];
        titlelabel.frame = CGRectMake(100, 7, 180, 35);
        titlelabel.numberOfLines = 2;
        // [titlelabel setFont:[UIFont fontWithName:@"Verdana" size:13]];
        [titlelabel setFont:[UIFont systemFontOfSize:17]];
        titlelabel.text = [dict objectForKey:@"name"];
        [cell.contentView addSubview:titlelabel];
        
        UILabel *lblDesc = [[UILabel alloc] init ];
        
        //else{
        lblDesc.frame=CGRectMake(100, 40, 200, 30);
        // }
        lblDesc.numberOfLines = 2;
        lblDesc.backgroundColor = [UIColor clearColor];
        [lblDesc setFont:[UIFont systemFontOfSize:13]];
        if (indexPath.row < fixedArrayCount)
            lblDesc.text = [dict objectForKey:@"preview"];
        else
            lblDesc.text = [dict objectForKey:@"vicinity"];
        
        
       
        
        if(_isSomethingEnabled &&![string isEqualToString:@"skipIntermediate"]){
            
            lblDesc.text = [dict objectForKey:@"details2"];
            
            titlelabel.frame = CGRectMake(100, 17, 180, 35);
            lblDesc.frame=CGRectMake(100, 45, 200, 30);
            
            UILabel *guideName = [[UILabel alloc] init];
            guideName.frame=CGRectMake(100, 10, 190, 7);
            guideName.numberOfLines = 1;
            guideName.backgroundColor = [UIColor clearColor];
            [guideName setFont:[UIFont systemFontOfSize:8]];
            
            if(indexPath.row==0&&_isSomethingEnabled&&appName)
            {
                if(!_isfromVC)
                {
                    guideName.text =[NSString stringWithFormat:@"Official Guide"];
                    
                    lblDesc.frame=CGRectZero;
                }
                
            }
            else
            {
                guideName.text =[NSString stringWithFormat:@"Guide: %@",[dict objectForKey:@"guide_name"]];
            }
            [cell.contentView addSubview:guideName];
        }
        
        else if(_isSomethingEnabled &&[string isEqualToString:@"skipIntermediate"])
        {
            
            lblDesc.text = [dict objectForKey:@"preview"];
            
            titlelabel.frame = CGRectMake(100, 17, 180, 35);
            lblDesc.frame=CGRectMake(100, 45, 200, 30);
            
            UILabel *guideName = [[UILabel alloc] init];
            guideName.frame=CGRectMake(100, 10, 190, 7);
            guideName.numberOfLines = 1;
            guideName.backgroundColor = [UIColor clearColor];
            [guideName setFont:[UIFont systemFontOfSize:8]];
            
            if(indexPath.row==0&&_isSomethingEnabled&&appName)
            {
                if(!_isfromVC)
                {
                    guideName.text =[NSString stringWithFormat:@"Official Guide"];
                    
                    lblDesc.frame=CGRectZero;
                }
                
            }
            else
            {
                guideName.text =[NSString stringWithFormat:@"Guide: %@",[dict objectForKey:@"guide_name"]];
            }
            [cell.contentView addSubview:guideName];
        }
        
        
        [cell.contentView addSubview:lblDesc];
        //if([[dict objectForKey:@"distance"]floatValue]!=0){
        UILabel *lblDistance = [[UILabel alloc] initWithFrame:CGRectMake(100, 80, 150, 16)];
        lblDistance.backgroundColor = [UIColor clearColor];
        [lblDistance setFont:[UIFont fontWithName:@"Verdana" size:10]];
        [cell.contentView addSubview:lblDistance];
        
      
        
        CLLocation *plLocation;
        if ([dict objectForKey:@"geometry"]) {
            
            NSDictionary *dictLoc = [dict objectForKey:@"geometry"];
            NSDictionary *dictLatLng = [dictLoc objectForKey:@"location"];
            plLocation = [[CLLocation alloc] initWithLatitude:[[dictLatLng objectForKey:@"lat"] doubleValue] longitude:[[dictLatLng objectForKey:@"lng"] doubleValue]];
        }
        else {
            plLocation = [[CLLocation alloc] initWithLatitude:[[dict objectForKey:@"latitude"] doubleValue] longitude:[[dict objectForKey:@"longitude"] doubleValue]];
        }
        
        CLLocationDistance meters = [location_updated distanceFromLocation:plLocation];
        float actdist;
        // if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"KM"] isEqualToString:@"NO"])
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"] isEqualToString:@"M"])
        {
            actdist = meters * 0.000621371;
            NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
            [nf setMaximumFractionDigits:2];
            [nf setMinimumIntegerDigits:1];
            NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
            //int k = actdist;
            if (actdist <= 0){
                // lblDistance.text = [NSString stringWithFormat:@"0%@ miles",trimmed];
                if([[dict objectForKey:@"star_rating"] isEqualToString:@"no"]){
                    lblDesc.frame=CGRectMake(100, 40, 200, 30+16+8);
                }
                if (indexPath.row > fixedArrayCount) {
                    lblDesc.frame=CGRectMake(100, 40, 200, 30+16+8);
                }
            }
            else{
                lblDistance.text = [NSString stringWithFormat:@"%@ mi",trimmed];
            }
        }
        else {
            actdist = meters * 0.001;
            NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
            [nf setMaximumFractionDigits:2];
            NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
            //int k = actdist;
            if (actdist <= 0){
                // lblDistance.text = [NSString stringWithFormat:@"0%@ kilometers",trimmed];
                if([[dict objectForKey:@"star_rating"] isEqualToString:@"no"]){
                    lblDesc.frame=CGRectMake(100, 40, 200, 30+16+8);
                }
                if (indexPath.row < fixedArrayCount) {
                    lblDesc.frame=CGRectMake(100, 40, 200, 30+16+8);
                }
            }
            else{
                lblDistance.text = [NSString stringWithFormat:@"%@ km",trimmed];
            }
        }
        if([[dict objectForKey:@"latitude"]isEqualToString:@"0"]&&[[dict objectForKey:@"latitude"]isEqualToString:@"0"]){
            lblDistance.hidden=YES;
        }
        //}
        
        if(_isSomethingEnabled)
        {
            
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"] isEqualToString:@"M"])
            {
                // dist=@"M";
                
                
                actdist = meters * 0.000621371;
                NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
                [nf setMaximumFractionDigits:2];
                [nf setMinimumIntegerDigits:1];
                NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
                //int k = actdist;
                if (actdist <= 0){
                    // lblDistance.text = [NSString stringWithFormat:@"0%@ miles",trimmed];
                    if([[dict objectForKey:@"star_rating"] isEqualToString:@"no"]){
                        lblDesc.frame=CGRectMake(100, 40, 200, 30+16+8);
                    }
                    if (indexPath.row > fixedArrayCount) {
                        lblDesc.frame=CGRectMake(100, 40, 200, 30+16+8);
                    }
                }
                else{
                    lblDistance.text = [NSString stringWithFormat:@"%@ mi",trimmed];
                }
                
                
            }
            else{
                
                // dist=@"K";
                
                
                actdist = meters * 0.001;
                NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
                [nf setMaximumFractionDigits:2];
                NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
                //int k = actdist;
                if (actdist <= 0){
                    // lblDistance.text = [NSString stringWithFormat:@"0%@ kilometers",trimmed];
                    if([[dict objectForKey:@"star_rating"] isEqualToString:@"no"]){
                        lblDesc.frame=CGRectMake(100, 40, 200, 30+16+8);
                    }
                    if (indexPath.row < fixedArrayCount) {
                        lblDesc.frame=CGRectMake(100, 40, 200, 30+16+8);
                    }
                }
                else{
                    lblDistance.text = [NSString stringWithFormat:@"%@ km",trimmed];
                }
                
                
            }
            
        }
        
        
        UIImageView *imDetail = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"listing_arrow.png"]];// arrow_list.png
        imDetail.frame = CGRectMake(295, 40, 25, 25);
        [cell.contentView addSubview:imDetail];
        
        buttonImg = (UIButton *)[cell.contentView.subviews objectAtIndex:0];
        
        buttonImg =[[UIButton alloc]init];
        
        //if([self reachable1])
        {
            
            btnImage = [UIImage imageNamed:@"1.png"];
            
            btnImageSel = [UIImage imageNamed:@"2.png"];
            
         
            
            NSString * str=[[NSUserDefaults standardUserDefaults]stringForKey:@"fav_icon"];
            
            NSLog(@"str fav_icon : %@",str);
            
            
            if(![[dictDetails objectForKey:@"fav_icon"] isEqualToString:@""])
            {
                if([[dictDetails objectForKey:@"fav_icon"] isEqualToString:@"star"])
                {
                    btnImage= [UIImage imageNamed:@"star.png"];
                    
                    btnImageSel= [UIImage imageNamed:@"star_sel.png"];
                    
                }
                
                else
                {
                    btnImage= [UIImage imageNamed:@"1.png"];
                    
                    btnImageSel= [UIImage imageNamed:@"2.png"];
                }
            }
            else
            {
                //test
                // str =@"star";
                //
                if([str isEqualToString:@"star"])
                {
                    btnImage= [UIImage imageNamed:@"star.png"];
                    
                    btnImageSel= [UIImage imageNamed:@"star_sel.png"];
                    
                }
                else
                {
                    btnImage= [UIImage imageNamed:@"1.png"];
                    
                    btnImageSel= [UIImage imageNamed:@"2.png"];
                    
                    
                }
            }
            
        }
        
        buttonImg = [UIButton buttonWithType:UIButtonTypeCustom];
        
        buttonImg.frame = CGRectMake(10, 45, 50, 50);
        
        // [buttonImg setTitleEdgeInsets:UIEdgeInsetsMake(15, 15, 15, 15)];
        //top ,left , bottom ,right
        [buttonImg setImageEdgeInsets:UIEdgeInsetsMake(20, 5, 5, 20)];
        
        [[buttonImg imageView] setContentMode: UIViewContentModeScaleAspectFit];
        [buttonImg setImage:btnImage forState:UIControlStateNormal];
        
        
        //////
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            myMutableArrayAgain = [plistDict objectForKey:@"favoritesArray"];
        }
        
        if ([myMutableArrayAgain count] == 0) {
            isFavorite = NO;
        }
        for (NSDictionary *dic in myMutableArrayAgain) {
            
            if([[dictDetails objectForKey:@"type"]isEqualToString:@"location"])
            {
                if ([[dict objectForKey:@"name"] isEqualToString:[dic objectForKey:@"name"]])
                {
                    NSLog(@"these are same man!!!");
                    isFavorite = YES;
                    break;
                }
                else {
                    isFavorite = NO;
                }
                
            }
            
            else
            {
                
                if ([[dict objectForKey:@"Id"] isEqualToString:[dic objectForKey:@"Id"]]&&[[dict objectForKey:@"name"] isEqualToString:[dic objectForKey:@"name"]])
                {
                    NSLog(@"these are same man!!!");
                    isFavorite = YES;
                    break;
                }
                else {
                    isFavorite = NO;
                }
                
            }
        }
        
        if (isFavorite) {
            
            isFavorite = YES;
            [buttonImg setImage:btnImageSel forState:UIControlStateNormal];
        }
        else {
            isFavorite = NO;
            [buttonImg setImage:btnImage forState:UIControlStateNormal];
        }
        
        //////
        [self->buttonImg addTarget:self action:@selector(onFavbtnList:) forControlEvents:UIControlEventTouchUpInside];
        
        buttonImg.userInteractionEnabled = YES;
        
        buttonImg.tag = indexPath.row;
        
        NSLog(@"buttonImg tag vc: %ld",(long)buttonImg.tag);
        
        [cell.contentView addSubview:buttonImg];
        
        if([[dict objectForKey:@"add_to_favorites"]isEqualToString:@"yes"]){
            
            buttonImg.hidden=NO;
            
            
            
        }
        else{
            
            buttonImg.hidden=YES;
            if([[dictDetails objectForKey:@"type"]isEqualToString:@"location"])
            {
                buttonImg.hidden=NO;
            }
            
        }
        
        if(_isSomethingEnabled&&![string isEqualToString:@"skipIntermediate"])
        {
            if([self reachable1])
            {
                if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"] isEqualToString:@"M"])
                {
                    float dist;
                    
                    dist=[[dict objectForKey:@"distance"]floatValue];
                    actdist = dist * 0.000621371;
                    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
                    [nf setMaximumFractionDigits:2];
                    NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:dist]];
                    
                    if(actdist == 0)
                    {
                        lblDistance.text = [NSString stringWithFormat:@"%@ mi",trimmed];
                    }
                    
                    
                    else{
                        lblDistance.text = [NSString stringWithFormat:@"%@ mi",trimmed];
                    }
                    
                    
                }
                
                else
                {
                    float dist;
                    
                    dist=[[dict objectForKey:@"distance"]floatValue];
                    actdist = dist * 0.000621371;
                    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
                    [nf setMaximumFractionDigits:2];
                    NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:dist]];
                    
                    if(actdist == 0)
                    {
                        lblDistance.text = [NSString stringWithFormat:@"%@ km",trimmed];
                    }
                    
                    
                    else{
                        lblDistance.text = [NSString stringWithFormat:@"%@ km",trimmed];
                    }
                    
                    
                }
                CGRect frame=CGRectZero;
                frame=lblDistance.frame;
                frame.origin.y=frame.origin.y;
                lblDistance.frame=frame;
                
                //imgRating.frame=(100, 73, 50, 8)
                
            }
            
            else {
                
                NSString *lat1 = [[NSUserDefaults standardUserDefaults]objectForKey:@"doubleLatitude"];
                NSString *lat2 =[[NSUserDefaults standardUserDefaults]objectForKey:@"doubleLongitude"];
                
                double latdouble = [lat1 doubleValue];
                NSLog(@"latdouble: %f", latdouble);
                double londouble = [lat2 doubleValue];
                NSLog(@"londouble: %f", londouble);
                
                NSString *latOff;
                NSString *longoff;
                
                
                // for(NSString *str in array)
                
                latOff = [dict valueForKey:@"latitude"];
                longoff = [dict valueForKey:@"longitude"];
                
                double latdoubleoff = [latOff doubleValue];
                NSLog(@"latOff: %f", latdoubleoff);
                double londoubleoff = [longoff doubleValue];
                NSLog(@"longoff: %f", londoubleoff);
                
                
                
                CLLocation *locA = [[CLLocation alloc] initWithLatitude:latdouble longitude:londouble];
                
                CLLocation *locB = [[CLLocation alloc] initWithLatitude:latdoubleoff longitude:londoubleoff];
                
                CLLocationDistance distance = [locA distanceFromLocation:locB];
                
                NSLog(@"distance prox :%f",distance);
                
                
                
                int dist = (int)distance;
                
                float inMorK;
                
                {
                    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"] isEqualToString:@"M"])
                    {
                        // float dist = 0.0;
                        
                        inMorK =  [self convertMeterToMilesOrKilometer:dist isKM:NO];
                        
                        NSLog(@"inMorK :%f",inMorK);
                        
                        
                        dist= inMorK;
                        actdist = dist * 0.000621371;
                        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
                        [nf setMaximumFractionDigits:2];
                        NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:dist]];
                        
                        if(actdist == 0)
                        {
                            lblDistance.text = [NSString stringWithFormat:@"%@ mi",trimmed];
                        }
                        
                        
                        else{
                            lblDistance.text = [NSString stringWithFormat:@"%@ mi",trimmed];
                        }
                        
                        
                    }
                    
                    else
                    {
                        //float dist = 0.0;
                        
                        inMorK =  [self convertMeterToMilesOrKilometer:dist isKM:YES];
                        
                        NSLog(@"inMorK :%f",inMorK);
                        
                        
                        dist= inMorK;
                        
                        actdist = dist * 0.000621371;
                        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
                        [nf setMaximumFractionDigits:2];
                        NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:dist]];
                        
                        if(actdist == 0)
                        {
                            lblDistance.text = [NSString stringWithFormat:@"%@ km",trimmed];
                        }
                        
                        
                        else{
                            lblDistance.text = [NSString stringWithFormat:@"%@ km",trimmed];
                        }
                        
                        
                    }
                    CGRect frame=CGRectZero;
                    frame=lblDistance.frame;
                    frame.origin.y=frame.origin.y;
                    lblDistance.frame=frame;
                    
                    //imgRating.frame=(100, 73, 50, 8)
                    
                }
                
            }
        }
        
        else if(_isSomethingEnabled && [string isEqualToString:@"skipIntermediate"])
        {
            
            NSString *lat1 = [[NSUserDefaults standardUserDefaults]objectForKey:@"doubleLatitude"];
            NSString *lat2 =[[NSUserDefaults standardUserDefaults]objectForKey:@"doubleLongitude"];
            
            double latdouble = [lat1 doubleValue];
            NSLog(@"latdouble: %f", latdouble);
            double londouble = [lat2 doubleValue];
            NSLog(@"londouble: %f", londouble);
            
            NSString *latOff;
            NSString *longoff;
            
            
            // for(NSString *str in array)
            
            latOff = [dict valueForKey:@"latitude"];
            longoff = [dict valueForKey:@"longitude"];
            
            double latdoubleoff = [latOff doubleValue];
            NSLog(@"latOff: %f", latdoubleoff);
            double londoubleoff = [longoff doubleValue];
            NSLog(@"longoff: %f", londoubleoff);
            
            
            
            CLLocation *locA = [[CLLocation alloc] initWithLatitude:latdouble longitude:londouble];
            
            CLLocation *locB = [[CLLocation alloc] initWithLatitude:latdoubleoff longitude:londoubleoff];
            
            CLLocationDistance distance = [locA distanceFromLocation:locB];
            
            NSLog(@"distance prox :%f",distance);
            
            
            
            int dist = (int)distance;
            
            float inMorK;
            
            {
                if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"] isEqualToString:@"M"])
                {
                    // float dist = 0.0;
                    
                    inMorK =  [self convertMeterToMilesOrKilometer:dist isKM:NO];
                    
                    NSLog(@"inMorK :%f",inMorK);
                    
                    
                    dist= inMorK;
                    actdist = dist * 0.000621371;
                    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
                    [nf setMaximumFractionDigits:2];
                    NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:dist]];
                    
                    if(actdist == 0)
                    {
                        lblDistance.text = [NSString stringWithFormat:@"%@ mi",trimmed];
                    }
                    
                    
                    else{
                        lblDistance.text = [NSString stringWithFormat:@"%@ mi",trimmed];
                    }
                    
                    
                }
                
                else
                {
                    //float dist = 0.0;
                    
                    inMorK =  [self convertMeterToMilesOrKilometer:dist isKM:YES];
                    
                    NSLog(@"inMorK :%f",inMorK);
                    
                    
                    dist= inMorK;
                    
                    actdist = dist * 0.000621371;
                    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
                    [nf setMaximumFractionDigits:2];
                    NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:dist]];
                    
                    if(actdist == 0)
                    {
                        lblDistance.text = [NSString stringWithFormat:@"%@ km",trimmed];
                    }
                    
                    
                    else{
                        lblDistance.text = [NSString stringWithFormat:@"%@ km",trimmed];
                    }
                    
                    
                }
                CGRect frame=CGRectZero;
                frame=lblDistance.frame;
                frame.origin.y=frame.origin.y;
                lblDistance.frame=frame;
                
                //imgRating.frame=(100, 73, 50, 8)
                
            }
            
        }
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *dictTest = [arraySmpl objectAtIndex:indexPath.row];
    
    if(appName)
    [[NSUserDefaults standardUserDefaults] setObject:[dictTest objectForKey:@"id"] forKey:@"currentAppId"];
    
    NSString *distToDetailFromList;
    float dist = 0.0;
    
    
    {
        
        if(([dictTest objectForKey:@"geometry"]))
        {
            if([self reachable1])
            {
                CLLocation *plLocation;
                if ([dictTest objectForKey:@"geometry"]) {
                    
                    NSDictionary *dictLoc = [dictTest objectForKey:@"geometry"];
                    NSDictionary *dictLatLng = [dictLoc objectForKey:@"location"];
                    plLocation = [[CLLocation alloc] initWithLatitude:[[dictLatLng objectForKey:@"lat"] doubleValue] longitude:[[dictLatLng objectForKey:@"lng"] doubleValue]];
                }
                else {
                    plLocation = [[CLLocation alloc] initWithLatitude:[[dictTest objectForKey:@"latitude"] doubleValue] longitude:[[dictTest objectForKey:@"longitude"] doubleValue]];
                }
                
                CLLocationDistance meters = [location_updated distanceFromLocation:plLocation];
                float actdist;
                
                if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"] isEqualToString:@"M"])
                {
                    actdist = meters * 0.000621371;
                    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
                    [nf setMaximumFractionDigits:2];
                    [nf setMinimumIntegerDigits:1];
                    NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
                    //int k = actdist;
                    if (actdist <= 0){
                        // lblDistance.text = [NSString stringWithFormat:@"0%@ miles",trimmed];
                        if([[dictTest objectForKey:@"star_rating"] isEqualToString:@"no"]){
                            
                        }
                        if (indexPath.row > fixedArrayCount) {
                            
                        }
                    }
                    else{
                        distToDetailFromList = [NSString stringWithFormat:@"%@ mi",trimmed];
                    }
                }
                else {
                    actdist = meters * 0.001;
                    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
                    [nf setMaximumFractionDigits:2];
                    NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
                    //int k = actdist;
                    if (actdist <= 0){
                        // lblDistance.text = [NSString stringWithFormat:@"0%@ kilometers",trimmed];
                        if([[dictTest objectForKey:@"star_rating"] isEqualToString:@"no"]){
                            
                        }
                        if (indexPath.row < fixedArrayCount) {
                            
                        }
                    }
                    else{
                        distToDetailFromList = [NSString stringWithFormat:@"%@ km",trimmed];
                    }
                }
                
                
                
                
                [[NSUserDefaults standardUserDefaults]setObject:distToDetailFromList forKey:@"distToDetailFromList"];
                
                
                
                if(indexPath.row==0&&_isSomethingEnabled&&appName)
                {
                    
                    if([self offlineAvailable])
                    {
                        
                        if(!_isfromVC)
                            
                        {
                            [[NSUserDefaults standardUserDefaults]setObject:[[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"id"] forKey:@"currentAppId"];
                            [[NSUserDefaults standardUserDefaults]setObject:[[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"name"] forKey:@"currentAppName"];
                            [[NSNotificationCenter defaultCenter]postNotificationName:@"loadFooter" object:nil];
                            
                            
                            
                            [[NSNotificationCenter defaultCenter]postNotificationName:@"toloadFooterValuesFromSearch"  object:self];
                            
                            PDViewController *vc=[[PDViewController alloc]init];
                            vc.fromOfficialGuideSearch=YES;
                            // [self.navigationController pushViewController:vc animated:NO];
                            return;
                        }
                        
                    }
                    else
                    {
                        [[NSUserDefaults standardUserDefaults]setObject:[[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"id"] forKey:@"currentAppId"];
                        [[NSUserDefaults standardUserDefaults]setObject:[[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"name"] forKey:@"currentAppName"];
                        
                        if([[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"image"])
                            [[NSUserDefaults standardUserDefaults]setObject:[[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"image"] forKey:@"guideimageFromASearch"];
                        
                        if([[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"paid_type"])
                            [[NSUserDefaults standardUserDefaults]setObject:[[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"paid_type"] forKey:@"paid_typefromSearch"];
                        
                        
                        NSString *string = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"skipIntermediate%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
                        
                        //skipIntermediate
                        
                        if([string isEqualToString:@"skipIntermediate"])
                        {
                            PDViewController *vc=[[PDViewController alloc]init];
                            vc.fromOfficialGuideSearch=YES;
                            [[self navigationController] pushViewController:vc animated:YES];
                        }
                        else
                        {
                            PDIntermediatePageViewController *interPage = [[PDIntermediatePageViewController alloc]init];
                            interPage.fromSearchToInter=YES;
                            [[self navigationController] pushViewController:interPage animated:YES];
                            
                            return;
                        }
                        
                    }
                    
                }
                PDDetailViewController *detailViewController = [[PDDetailViewController alloc] init];
                if(_isSomethingEnabled)
                {
                    detailViewController.isfromSearch=YES;
                    
                    strDistan= [[arraySmpl objectAtIndex:indexPath.row]valueForKey:@"distance"];
                    
                    
                    
                    [[NSUserDefaults standardUserDefaults]setObject:strDistan forKey:@"distanceFromSearchForDetail"];
                }
                NSMutableDictionary *dict = [arraySmpl objectAtIndex:indexPath.row];
                NSString *sampText=lblTitleName.text;
                detailViewController.lblNameLabelStr=lblTitleName.text;
                if(_isSomethingEnabled)
                {
                    
                }
                else{
                    [dict setObject:[dictDetails objectForKey:@"listing_icon"] forKey:@"listing_icon"];
                }
                
                if([[dictDetails objectForKey:@"type"]isEqualToString:@"location"])
                    
                {
                    detailViewController.isTypeLocation=YES;
                }
                else
                {
                    detailViewController.isTypeLocation=NO;
                }
                
                [detailViewController getDictionary:dict];
                [detailViewController getArray:arraySmpl :indexPath.row];
                [self.navigationController pushViewController:detailViewController animated:YES];
                
                
                return;
            }
            else
                return;
        }
        
    }
    
    
    if(![self reachable1])
    {
        
        
        NSString *lat1 = [[NSUserDefaults standardUserDefaults]objectForKey:@"doubleLatitude"];
        NSString *lat2 =[[NSUserDefaults standardUserDefaults]objectForKey:@"doubleLongitude"];
        
        double latdouble = [lat1 doubleValue];
        NSLog(@"latdouble: %f", latdouble);
        double londouble = [lat2 doubleValue];
        NSLog(@"londouble: %f", londouble);
        
        NSString *latOff;
        NSString *longoff;
        
        
        // for(NSString *str in array)
        
        latOff = [dictTest valueForKey:@"latitude"];
        longoff = [dictTest valueForKey:@"longitude"];
        
        double latdoubleoff = [latOff doubleValue];
        NSLog(@"latOff: %f", latdoubleoff);
        double londoubleoff = [longoff doubleValue];
        NSLog(@"longoff: %f", londoubleoff);
        
        
        
        CLLocation *locA = [[CLLocation alloc] initWithLatitude:latdouble longitude:londouble];
        
        CLLocation *locB = [[CLLocation alloc] initWithLatitude:latdoubleoff longitude:londoubleoff];
        
        CLLocationDistance distance = [locA distanceFromLocation:locB];
        
        NSLog(@"distance prox :%f",distance);
        
        
        
        int dist = (int)distance;
        
        float inMorK;
        
        {
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"] isEqualToString:@"M"])
            {
                // float dist = 0.0;
                
                inMorK =  [self convertMeterToMilesOrKilometer:dist isKM:NO];
                
                NSLog(@"inMorK :%f",inMorK);
                
                
                dist= inMorK;
                
                NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
                [nf setMaximumFractionDigits:2];
                NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:dist]];
                
                {
                    distToDetailFromList = [NSString stringWithFormat:@"%@ mi",trimmed];
                }
                
                
            }
            
            else
            {
                //float dist = 0.0;
                
                inMorK =  [self convertMeterToMilesOrKilometer:dist isKM:YES];
                
                NSLog(@"inMorK :%f",inMorK);
                
                
                dist= inMorK;
                
                
                NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
                [nf setMaximumFractionDigits:2];
                NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:dist]];
                
                {
                    distToDetailFromList = [NSString stringWithFormat:@"%@ km",trimmed];
                }
                
                
            }
            
            
        }
        
        
        
    }
    else if(![self offlineAvailable]&&[self reachable1])
    {
        
       
        
        NSString *offlineGuideStr = [[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"offline_guide"];
        
      
        
        {
            if(indexPath.row==0&&_isSomethingEnabled&&appName)
            {
                
                
                [[NSUserDefaults standardUserDefaults]setObject:[[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"id"] forKey:@"currentAppId"];
                [[NSUserDefaults standardUserDefaults]setObject:[[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"name"] forKey:@"currentAppName"];
                
                if([[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"image"])
                    [[NSUserDefaults standardUserDefaults]setObject:[[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"image"] forKey:@"guideimageFromASearch"];
                
                if([[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"paid_type"])
                    [[NSUserDefaults standardUserDefaults]setObject:[[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"paid_type"] forKey:@"paid_typefromSearch"];
                
                {
                    PDIntermediatePageViewController *interPage = [[PDIntermediatePageViewController alloc]init];
                    interPage.fromSearchToInter=YES;
                    
                    if([[arraySmpl objectAtIndex:indexPath.row]valueForKey:@"zip_file_size"])
                        [[NSUserDefaults standardUserDefaults]setObject:[[arraySmpl objectAtIndex:indexPath.row]valueForKey:@"zip_file_size"] forKey:@"zipfilesizeFromlistto inter"];
                    
                    [[self navigationController] pushViewController:interPage animated:YES];
                    
                    return;
                }
                
                return;
            }
        }
        
        dist= [[dictTest objectForKey:@"distance"]floatValue];
        
        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
        [nf setMaximumFractionDigits:2];
        NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:dist]];
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"] isEqualToString:@"M"])
            
            
        {
            distToDetailFromList = [NSString stringWithFormat:@"%@ mi",trimmed];
        }
        else
        {
            distToDetailFromList = [NSString stringWithFormat:@"%@ km",trimmed];
        }
        
    }
    
    else if([self offlineAvailable]&&[self reachable1])
    {
        
   
        NSString *offlineGuideStr = [[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"offline_guide"];
        
     
            
        {
            if(indexPath.row==0&&_isSomethingEnabled&&appName)
            {
                
                
                [[NSUserDefaults standardUserDefaults]setObject:[[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"id"] forKey:@"currentAppId"];
                [[NSUserDefaults standardUserDefaults]setObject:[[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"name"] forKey:@"currentAppName"];
                
                if([[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"image"])
                    [[NSUserDefaults standardUserDefaults]setObject:[[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"image"] forKey:@"guideimageFromASearch"];
                
                if([[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"paid_type"])
                    [[NSUserDefaults standardUserDefaults]setObject:[[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"paid_type"] forKey:@"paid_typefromSearch"];
                
         
                
                loaderView= [[UIView alloc]initWithFrame:self.view.frame];
                loaderView.backgroundColor=[UIColor whiteColor];
                
                [self.view addSubview:loaderView];
                
                [self testloadSplashSearch];
                
             
                [self.view addSubview:splashSearch];
                
           
                
              return;
            }
        }

        dist= [[dictTest objectForKey:@"distance"]floatValue];
        
        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
        [nf setMaximumFractionDigits:2];
        NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:dist]];
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"] isEqualToString:@"M"])
            
            
        {
            distToDetailFromList = [NSString stringWithFormat:@"%@ mi",trimmed];
        }
        else
        {
            distToDetailFromList = [NSString stringWithFormat:@"%@ km",trimmed];
        }
        
    }
    
    else
    {
        {
            
            
            NSString *lat1 = [[NSUserDefaults standardUserDefaults]objectForKey:@"doubleLatitude"];
            NSString *lat2 =[[NSUserDefaults standardUserDefaults]objectForKey:@"doubleLongitude"];
            
            double latdouble = [lat1 doubleValue];
            NSLog(@"latdouble: %f", latdouble);
            double londouble = [lat2 doubleValue];
            NSLog(@"londouble: %f", londouble);
            
            NSString *latOff;
            NSString *longoff;
            
            
            // for(NSString *str in array)
            
            latOff = [dictTest valueForKey:@"latitude"];
            longoff = [dictTest valueForKey:@"longitude"];
            
            double latdoubleoff = [latOff doubleValue];
            NSLog(@"latOff: %f", latdoubleoff);
            double londoubleoff = [longoff doubleValue];
            NSLog(@"longoff: %f", londoubleoff);
            
            
            
            CLLocation *locA = [[CLLocation alloc] initWithLatitude:latdouble longitude:londouble];
            
            CLLocation *locB = [[CLLocation alloc] initWithLatitude:latdoubleoff longitude:londoubleoff];
            
            CLLocationDistance distance = [locA distanceFromLocation:locB];
            
            NSLog(@"distance prox :%f",distance);
            
            
            
            int dist = (int)distance;
            
            float inMorK;
            
            {
                if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"showDistanceIn"] isEqualToString:@"M"])
                {
                    // float dist = 0.0;
                    
                    inMorK =  [self convertMeterToMilesOrKilometer:dist isKM:NO];
                    
                    NSLog(@"inMorK :%f",inMorK);
                    
                    
                    dist= inMorK;
                    
                    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
                    [nf setMaximumFractionDigits:2];
                    NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:dist]];
                    
                    {
                        distToDetailFromList = [NSString stringWithFormat:@"%@ mi",trimmed];
                    }
                    
                    
                }
                
                else
                {
                    //float dist = 0.0;
                    
                    inMorK =  [self convertMeterToMilesOrKilometer:dist isKM:YES];
                    
                    NSLog(@"inMorK :%f",inMorK);
                    
                    
                    dist= inMorK;
                    
                    
                    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
                    [nf setMaximumFractionDigits:2];
                    NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:dist]];
                    
                    {
                        distToDetailFromList = [NSString stringWithFormat:@"%@ km",trimmed];
                    }
                    
                    
                }
                
                
            }
            
            
            
        }
    }
    
    [[NSUserDefaults standardUserDefaults]setObject:distToDetailFromList forKey:@"distToDetailFromList"];
    
    
    
    if(indexPath.row==0&&_isSomethingEnabled&&appName)
    {
        
        if([self offlineAvailable])
        {
            
            if(!_isfromVC)
                
            {
                [[NSUserDefaults standardUserDefaults]setObject:[[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"id"] forKey:@"currentAppId"];
                [[NSUserDefaults standardUserDefaults]setObject:[[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"name"] forKey:@"currentAppName"];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"loadFooter" object:nil];
                
                
                
                [[NSNotificationCenter defaultCenter]postNotificationName:@"toloadFooterValuesFromSearch"  object:self];
                
                PDViewController *vc=[[PDViewController alloc]init];
                vc.fromOfficialGuideSearch=YES;
                // [self.navigationController pushViewController:vc animated:NO];
                return;
            }
            
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setObject:[[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"id"] forKey:@"currentAppId"];
            [[NSUserDefaults standardUserDefaults]setObject:[[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"name"] forKey:@"currentAppName"];
            
            if([[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"image"])
                [[NSUserDefaults standardUserDefaults]setObject:[[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"image"] forKey:@"guideimageFromASearch"];
            
            if([[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"paid_type"])
                [[NSUserDefaults standardUserDefaults]setObject:[[arraySmpl objectAtIndex:indexPath.row]objectForKey:@"paid_type"] forKey:@"paid_typefromSearch"];
            
            
            NSString *string = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"skipIntermediate%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
            
            //skipIntermediate
            
            if([string isEqualToString:@"skipIntermediate"])
            {
                PDViewController *vc=[[PDViewController alloc]init];
                vc.fromOfficialGuideSearch=YES;
                [[self navigationController] pushViewController:vc animated:YES];
                
                return;
            }
            else
            {
                PDIntermediatePageViewController *interPage = [[PDIntermediatePageViewController alloc]init];
                interPage.fromSearchToInter=YES;
                
                if([[arraySmpl objectAtIndex:indexPath.row]valueForKey:@"zip_file_size"])
                   [[NSUserDefaults standardUserDefaults]setObject:[[arraySmpl objectAtIndex:indexPath.row]valueForKey:@"zip_file_size"] forKey:@"zipfilesizeFromlistto inter"];
                
                [[self navigationController] pushViewController:interPage animated:YES];
                
                return;
            }
            
        }
        
    }
    PDDetailViewController *detailViewController = [[PDDetailViewController alloc] init];
    if(_isSomethingEnabled)
    {
        detailViewController.isfromSearch=YES;
        
        strDistan= [[arraySmpl objectAtIndex:indexPath.row]valueForKey:@"distance"];
        
        
        
        [[NSUserDefaults standardUserDefaults]setObject:strDistan forKey:@"distanceFromSearchForDetail"];
    }
    NSMutableDictionary *dict = [arraySmpl objectAtIndex:indexPath.row];
    NSString *sampText=lblTitleName.text;
    detailViewController.lblNameLabelStr=lblTitleName.text;
    if(_isSomethingEnabled)
    {
        
    }
    else{
        [dict setObject:[dictDetails objectForKey:@"listing_icon"] forKey:@"listing_icon"];
    }
    
    if([[dictDetails objectForKey:@"type"]isEqualToString:@"location"])
        
    {
        detailViewController.isTypeLocation=YES;
    }
    else
    {
        detailViewController.isTypeLocation=NO;
    }

    
    [detailViewController getDictionary:dict];
    [detailViewController getArray:arraySmpl :indexPath.row];
    [self.navigationController pushViewController:detailViewController animated:YES];
}

//-(IBAction)onFavbtnList:(id)sender
-(void)onFavbtnList:(UIButton*)sender
{
    
    
    
    NSLog(@"sender tag : %ld",(long)sender.tag);
    
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        myMutableArrayAgain = [plistDict objectForKey:@"favoritesArray"];
    }
    
    if ([myMutableArrayAgain count] == 0) {
        isFavorite = NO;
    }
    
    NSDictionary *dict = [arraySmpl objectAtIndex:sender.tag];
    for (NSDictionary *dic in myMutableArrayAgain) {
        
        if([[dictDetails objectForKey:@"type"]isEqualToString:@"location"])
        {
            if ([[dict objectForKey:@"name"] isEqualToString:[dic objectForKey:@"name"]])
            {
                NSLog(@"these are same man!!!");
                isFavorite = YES;
                break;
            }
            else {
                isFavorite = NO;
            }
            
        }
        
        else
        {
            
            if ([[dict objectForKey:@"Id"] isEqualToString:[dic objectForKey:@"Id"]]&&[[dict objectForKey:@"name"] isEqualToString:[dic objectForKey:@"name"]]) {
                NSLog(@"these are same man!!!");
                isFavorite = YES;
                break;
            }
            else {
                isFavorite = NO;
            }
        }
    }
    
    
    
    
    if (!isFavorite)
    {
        
        isFavorite = YES;
        
        NSLog(@"buttonImg tag : %ld",(long)buttonImg.tag);
        
        UIImage *btnImage2 = [UIImage imageNamed:@"2.png"];
        NSMutableDictionary *dict3 = [arraySmpl objectAtIndex:sender.tag];
        [sender setImage:btnImageSel forState:UIControlStateNormal];
        PDDetailViewController *dVC = [[PDDetailViewController alloc]init];
        [dVC getDictionary:dict3];
        [dVC getArray:arraySmpl :sender.tag];
        [dVC onFavorite:sender];
    }
    
    else
    {
        isFavorite = NO;
        NSLog(@"buttonImg tag : %ld",(long)buttonImg.tag);
        
        UIImage *btnImage2 = [UIImage imageNamed:@"1.png"];
        NSMutableDictionary *dict3 = [arraySmpl objectAtIndex:sender.tag];
        [sender setImage:btnImage forState:UIControlStateNormal];
        PDDetailViewController *dVC = [[PDDetailViewController alloc]init];
        [dVC getDictionary:dict3];
        [dVC getArray:arraySmpl :sender.tag];
        [dVC onClick:sender];
        
    }
   
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}
-(void)coreDataCodeWrite_bus:(NSArray*)queArray{
    
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    int i;
    NSManagedObjectContext *context1 = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Businesses" inManagedObjectContext:context1];
    [fetchRequest setEntity:entity];
    //fetchRequest.predicate = [NSPredicate predicateWithFormat:@"catId == %@",[[queArray objectAtIndex:0] objectForKey:@"Id"]];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSError *error;
    
    if(([[dictDetails objectForKey:@"type"] isEqualToString:@"location"]||[[dictDetails objectForKey:@"type"] isEqualToString:@"fixed"]||[[dictDetails objectForKey:@"type"] isEqualToString:@"guided tour"]||[[dictDetails objectForKey:@"type"] isEqualToString:@"ordered list"]||[[dictDetails objectForKey:@"type"] isEqualToString:@"normal"]))
    {
        NSPredicate * parentIdPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"cat_id==%@",[dictDetails objectForKey:@"Id"]]];
        
        [fetchRequest setPredicate:parentIdPredicate];
    }
    
    NSArray *listOfQUEToBeDeleted = [context1 executeFetchRequest:fetchRequest error:&error];
    Businesses *currentQUE;
    for(currentQUE in listOfQUEToBeDeleted)
    {
        [context1 deleteObject:currentQUE];
    }
    
    
    //    for(currentQUE.options in listOfQUEToBeDeleted){
    //
    //         [context1 deleteObject:currentQUE.options];
    //    }
    
    if (![context1 save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    @try {
        for (i=0; i<[queArray count]; i++) {
            
            
            
            NSManagedObjectContext *context = [appDelegate managedObjectContext];
            NSManagedObject *Que = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"Businesses"
                                    inManagedObjectContext:context];
            
            
            NSString *appid=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
            
            [Que setValue:appid forKey:@"appid"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"Id"]forKey:@"busId"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"name"]forKey:@"name"];
            [Que setValue:[dictDetails objectForKey:@"Id"]forKey:@"cat_id"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"address"]forKey:@"address"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"preview"]forKey:@"preview"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"details"]forKey:@"details"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"phone"]forKey:@"phone"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"website"]forKey:@"website"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"latitude"]forKey:@"latitude"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"longitude"]forKey:@"longitude"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"featured"]forKey:@"featured"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"star_rating"]forKey:@"star_rating"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"add_to_favorites"]forKey:@"add_to_favorites"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"share_listing"]forKey:@"share_listing"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"fixed_order"]forKey:@"fixed_order"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"show_call"]forKey:@"show_call"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"show_web"]forKey:@"show_web"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"website_text"]forKey:@"website_text"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"email_address"]forKey:@"email_address"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"show_email"]forKey:@"show_email"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"email_text"]forKey:@"email_text"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"disp_address"]forKey:@"disp_address"];
            
            if([[queArray objectAtIndex:i] objectForKey:@"guide_id"])
                [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"guide_id"]forKey:@"guide_id"];
            
            if([[queArray objectAtIndex:i] objectForKey:@"guide_name"])
                [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"guide_name"]forKey:@"guide_name"];
            
            
            NSMutableArray*muts=[[NSMutableArray alloc]init];
            @try {
                
                
                for(NSString*str in [[queArray objectAtIndex:i] objectForKey:@"image"] ){
                    
                    NSString*str1=str;
                    // str1= [str1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    [muts addObject:str1];
                    if(str1.length>0)
                    {
                        
                    }
                }
            }
            @catch (NSException *exception) {
                NSString*str = [[queArray objectAtIndex:i] objectForKey:@"image"];
                [muts addObject:str];
            }
            
            
            [Que setValue:muts forKey:@"image"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"type"]forKey:@"type"];
            
            NSString*str=[[queArray objectAtIndex:i] objectForKey:@"thumb"];
            // str= [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            /* if(str.length>0)
             {
             [self insertImagesToDownload:str];
             [self sdImageCaching:str];
             }*/
            [Que setValue:str forKey:@"thumb"];
            [Que setValue:[[queArray objectAtIndex:i] objectForKey:@"weight"]forKey:@"weight"];
            
            
            NSError *error;
            if (![context save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
            else{
                //            [self performSelectorInBackground:@selector(updateProgress:) withObject:[NSNumber numberWithFloat:i]];
                // [self stopCircleLoader4];
            }
            
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        
        
    }
    // [progressView removeFromSuperview];
    // [self coreDataCheck];
    
}
-(void)testingcoredataread_bus
{
    NSMutableArray*   array  = [NSMutableArray array];
    ///////////array3//////////
    
    NSError *error3;
    PDAppDelegate *appDelegate3 = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context3 = [appDelegate3 managedObjectContext];
    NSFetchRequest *fetchRequest3 = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity3 = [NSEntityDescription
                                    entityForName:@"Businesses" inManagedObjectContext:context3];
    fetchRequest3.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest3 setEntity:entity3];
    
    int l;
    
    
    
    
    {
        
        
        //[NSString stringWithFormat:@"cat_id4==%@",[dictDetails objectForKey:@"Id"]]
        
        NSPredicate * parentIdPredicate3 = [NSPredicate predicateWithFormat:@"(%K CONTAINS %@) OR (%K CONTAINS %@) OR (%K CONTAINS %@) OR (%K CONTAINS %@) OR (%K CONTAINS %@) OR (%K CONTAINS %@) OR (%K CONTAINS %@) OR (%K CONTAINS %@) OR (%K CONTAINS %@) OR (%K CONTAINS %@)",@"cat_id",[dictDetails objectForKey:@"Id"],@"cat_id2",[dictDetails objectForKey:@"Id"],@"cat_id3",[dictDetails objectForKey:@"Id"],@"cat_id4",[dictDetails objectForKey:@"Id"],@"cat_id5",[dictDetails objectForKey:@"Id"],@"cat_id6",[dictDetails objectForKey:@"Id"],@"cat_id7",[dictDetails objectForKey:@"Id"],@"cat_id8",[dictDetails objectForKey:@"Id"],@"cat_id9",[dictDetails objectForKey:@"Id"],@"cat_id10",[dictDetails objectForKey:@"Id"]];
        
        [fetchRequest3 setPredicate:parentIdPredicate3];
    }
    
    
    
    NSArray *fetchedObjects3 = [context3 executeFetchRequest:fetchRequest3 error:&error3];
    
    
    
    
    
    
    
    NSLog( @"error %@",error3);
    
    
    NSMutableArray*   array3  = [NSMutableArray array];
    
    for (Businesses *manuf in fetchedObjects3) {
        NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
        
        
        
        {
            
            // @try
            // {
            
            if(manuf.busId)
                [tempManufacturerDictionary setObject:manuf.busId forKey:@"Id"];
            if(manuf.image)
                [tempManufacturerDictionary setObject:manuf.image forKey:@"image"];
            if(manuf.name)
                [tempManufacturerDictionary setObject:manuf.name forKey:@"name"];
            if(manuf.cat_id)
                [tempManufacturerDictionary setObject:manuf.cat_id forKey:@"cat_id"];
            if(manuf.cat_id2)
                [tempManufacturerDictionary setObject:manuf.cat_id2 forKey:@"cat_id2"];
            if(manuf.cat_id3)
                [tempManufacturerDictionary setObject:manuf.cat_id3 forKey:@"cat_id3"];
            if(manuf.cat_id4)
                [tempManufacturerDictionary setObject:manuf.cat_id4 forKey:@"cat_id4"];
            if(manuf.cat_id5)
                [tempManufacturerDictionary setObject:manuf.cat_id5 forKey:@"cat_id5"];
            if(manuf.cat_id6)
                [tempManufacturerDictionary setObject:manuf.cat_id6 forKey:@"cat_id6"];
            if(manuf.cat_id7)
                [tempManufacturerDictionary setObject:manuf.cat_id7 forKey:@"cat_id7"];
            if(manuf.cat_id8)
                [tempManufacturerDictionary setObject:manuf.cat_id8 forKey:@"cat_id8"];
            if(manuf.cat_id9)
                [tempManufacturerDictionary setObject:manuf.cat_id9 forKey:@"cat_id9"];
            if(manuf.cat_id10)
                [tempManufacturerDictionary setObject:manuf.cat_id10 forKey:@"cat_id10"];
            if(manuf.address)
                [tempManufacturerDictionary setObject:manuf.address forKey:@"address"];
            if(manuf.preview)
                [tempManufacturerDictionary setObject:manuf.preview forKey:@"preview"];
            if(manuf.details)
                [tempManufacturerDictionary setObject:manuf.details forKey:@"details"];
            if(manuf.phone)
                [tempManufacturerDictionary setObject:manuf.phone forKey:@"phone"];
            if(manuf.website)
                [tempManufacturerDictionary setObject:manuf.website forKey:@"website"];
            if(manuf.latitude)
                [tempManufacturerDictionary setObject:manuf.latitude forKey:@"latitude"];
            if(manuf.longitude)
                [tempManufacturerDictionary setObject:manuf.longitude forKey:@"longitude"];
            if(manuf.featured)
                [tempManufacturerDictionary setObject:manuf.featured forKey:@"featured"];
            if(manuf.star_rating)
                [tempManufacturerDictionary setObject:manuf.star_rating forKey:@"star_rating"];
            if(manuf.type)
                [tempManufacturerDictionary setObject:manuf.type forKey:@"type"];
            if(manuf.thumb)
                [tempManufacturerDictionary setObject:manuf.thumb forKey:@"thumb"];
            if(manuf.weight)
                [tempManufacturerDictionary setObject:manuf.weight forKey:@"weight"];
            if(manuf.add_to_favorites)
                [tempManufacturerDictionary setObject:manuf.add_to_favorites forKey:@"add_to_favorites"];
            if(manuf.share_listing)
                [tempManufacturerDictionary setObject:manuf.share_listing forKey:@"share_listing"];
            if(manuf.fixed_order)
                [tempManufacturerDictionary setObject:manuf.fixed_order forKey:@"fixed_order"];
            if(manuf.show_call)
                [tempManufacturerDictionary setObject:manuf.show_call forKey:@"show_call"];
            if(manuf.show_web)
                [tempManufacturerDictionary setObject:manuf.show_web forKey:@"show_web"];
            if(manuf.website_text)
                [tempManufacturerDictionary setObject:manuf.website_text forKey:@"website_text"];
            if(manuf.email_address)
                [tempManufacturerDictionary setObject:manuf.email_address forKey:@"email_address"];
            if(manuf.show_email)
                [tempManufacturerDictionary setObject:manuf.show_email forKey:@"show_email"];
            if(manuf.email_text)
                [tempManufacturerDictionary setObject:manuf.email_text forKey:@"email_text"];
            if(manuf.disp_address)
                [tempManufacturerDictionary setObject:manuf.disp_address forKey:@"disp_address"];
            
            // }
            //  @catch (NSException *exception) {
            
            // }
        }
        
        
        
        CLLocation *plLocation= [[CLLocation alloc] initWithLatitude:[manuf.latitude doubleValue] longitude:[manuf.longitude doubleValue]];
        CLLocationDistance meters = [location_updated distanceFromLocation:plLocation];
        float actdist;
        actdist = meters * 0.000621371;
        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
        [nf setMaximumFractionDigits:2];
        NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
        [tempManufacturerDictionary setObject:trimmed forKey:@"dist_offline"];
        
        [array3 addObject:tempManufacturerDictionary];
        
        
        
    }
    
    
    
    NSSortDescriptor *descriptor3 = [[NSSortDescriptor alloc] initWithKey:@"dist_offline"  ascending:YES];
    NSArray *arrtemp33 = [array3 sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor3,nil]];
    
    array3=[NSMutableArray arrayWithArray:arrtemp33];
    
   
    [array addObjectsFromArray:array3];
    
   


}

-(void)bus_testcatid
{
    
    NSMutableArray*   array  = [NSMutableArray array];
    
   
    
    NSError *error1;
    PDAppDelegate *appDelegate1 = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context1 = [appDelegate1 managedObjectContext];
    NSFetchRequest *fetchRequest1 = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity1 = [NSEntityDescription
                                    entityForName:@"Businesses" inManagedObjectContext:context1];
    fetchRequest1.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest1 setEntity:entity1];
    
    int j;
    
    
    
    
    {
        NSPredicate * parentIdPredicate1 = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"cat_id2==%@",[dictDetails objectForKey:@"Id"]]];
        
        [fetchRequest1 setPredicate:parentIdPredicate1];
    }
    
    
    
    NSArray *fetchedObjects1 = [context1 executeFetchRequest:fetchRequest1 error:&error1];
    
    
    
    
    
    
    
    NSLog( @"error %@",error1);
    
    
    NSMutableArray*   array1  = [NSMutableArray array];
    
    for (Businesses *manuf in fetchedObjects1) {
        NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
        
        
        
        {
            [tempManufacturerDictionary setObject:manuf.busId forKey:@"Id"];
            [tempManufacturerDictionary setObject:manuf.image forKey:@"image"];
            [tempManufacturerDictionary setObject:manuf.name forKey:@"name"];
            [tempManufacturerDictionary setObject:manuf.cat_id forKey:@"cat_id"];
            
            [tempManufacturerDictionary setObject:manuf.cat_id2 forKey:@"cat_id2"];
            [tempManufacturerDictionary setObject:manuf.cat_id3 forKey:@"cat_id3"];
            [tempManufacturerDictionary setObject:manuf.cat_id4 forKey:@"cat_id4"];
            
            [tempManufacturerDictionary setObject:manuf.cat_id5 forKey:@"cat_id5"];
            [tempManufacturerDictionary setObject:manuf.cat_id6 forKey:@"cat_id6"];
            [tempManufacturerDictionary setObject:manuf.cat_id7 forKey:@"cat_id7"];
            [tempManufacturerDictionary setObject:manuf.cat_id8 forKey:@"cat_id8"];
            [tempManufacturerDictionary setObject:manuf.cat_id9 forKey:@"cat_id9"];
            [tempManufacturerDictionary setObject:manuf.cat_id10 forKey:@"cat_id10"];
            
            [tempManufacturerDictionary setObject:manuf.address forKey:@"address"];
            
            
            if(manuf.preview)
                [tempManufacturerDictionary setObject:manuf.preview forKey:@"preview"];
            
            [tempManufacturerDictionary setObject:manuf.details forKey:@"details"];
            if(manuf.phone)
                [tempManufacturerDictionary setObject:manuf.phone forKey:@"phone"];
            [tempManufacturerDictionary setObject:manuf.website forKey:@"website"];
            [tempManufacturerDictionary setObject:manuf.latitude forKey:@"latitude"];
            [tempManufacturerDictionary setObject:manuf.longitude forKey:@"longitude"];
            [tempManufacturerDictionary setObject:manuf.featured forKey:@"featured"];
            [tempManufacturerDictionary setObject:manuf.star_rating forKey:@"star_rating"];
            [tempManufacturerDictionary setObject:manuf.type forKey:@"type"];
            [tempManufacturerDictionary setObject:manuf.thumb forKey:@"thumb"];
            [tempManufacturerDictionary setObject:manuf.weight forKey:@"weight"];
            [tempManufacturerDictionary setObject:manuf.add_to_favorites forKey:@"add_to_favorites"];
            [tempManufacturerDictionary setObject:manuf.share_listing forKey:@"share_listing"];
            [tempManufacturerDictionary setObject:manuf.fixed_order forKey:@"fixed_order"];
            [tempManufacturerDictionary setObject:manuf.show_call forKey:@"show_call"];
            [tempManufacturerDictionary setObject:manuf.show_web forKey:@"show_web"];
            [tempManufacturerDictionary setObject:manuf.website_text forKey:@"website_text"];
            
            [tempManufacturerDictionary setObject:manuf.email_address forKey:@"email_address"];
            [tempManufacturerDictionary setObject:manuf.show_email forKey:@"show_email"];
            [tempManufacturerDictionary setObject:manuf.email_text forKey:@"email_text"];
            [tempManufacturerDictionary setObject:manuf.disp_address forKey:@"disp_address"];
        }
        
        
        
        CLLocation *plLocation= [[CLLocation alloc] initWithLatitude:[manuf.latitude doubleValue] longitude:[manuf.longitude doubleValue]];
        CLLocationDistance meters = [location_updated distanceFromLocation:plLocation];
        float actdist;
        actdist = meters * 0.000621371;
        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
        [nf setMaximumFractionDigits:2];
        NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
        [tempManufacturerDictionary setObject:trimmed forKey:@"dist_offline"];
        
        [array1 addObject:tempManufacturerDictionary];
        
        
        
    }
    
    NSSortDescriptor *descriptor1 = [[NSSortDescriptor alloc] initWithKey:@"dist_offline"  ascending:YES];
    NSArray *arrtemp31 = [array1 sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor1,nil]];
    // NSArray *arrVals = [arrtemp3 copy];
    array1=[NSMutableArray arrayWithArray:arrtemp31];
    
    // array=[NSMutableArray arrayWithArray:array1];
    [array addObjectsFromArray:array1];
    
    /////////////////////////////////////////
    
    NSError *error2;
    PDAppDelegate *appDelegate2 = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context2 = [appDelegate2 managedObjectContext];
    NSFetchRequest *fetchRequest2 = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity2 = [NSEntityDescription
                                    entityForName:@"Businesses" inManagedObjectContext:context2];
    fetchRequest2.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest2 setEntity:entity2];
    
    int k;
    
    
    
    
    {
        NSPredicate * parentIdPredicate2 = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"cat_id3==%@",[dictDetails objectForKey:@"Id"]]];
        
        [fetchRequest2 setPredicate:parentIdPredicate2];
    }
    
    
    
    NSArray *fetchedObjects2 = [context2 executeFetchRequest:fetchRequest2 error:&error2];
    
    
    
    
    
    
    
    NSLog( @"error %@",error2);
    
    
    NSMutableArray*   array2  = [NSMutableArray array];
    
    for (Businesses *manuf in fetchedObjects2) {
        NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
        
        
        
        {
            
            // @try
            // {
            
            if(manuf.busId)
                [tempManufacturerDictionary setObject:manuf.busId forKey:@"Id"];
            if(manuf.image)
                [tempManufacturerDictionary setObject:manuf.image forKey:@"image"];
            if(manuf.name)
                [tempManufacturerDictionary setObject:manuf.name forKey:@"name"];
            if(manuf.cat_id)
                [tempManufacturerDictionary setObject:manuf.cat_id forKey:@"cat_id"];
            if(manuf.cat_id2)
                [tempManufacturerDictionary setObject:manuf.cat_id2 forKey:@"cat_id2"];
            if(manuf.cat_id3)
                [tempManufacturerDictionary setObject:manuf.cat_id3 forKey:@"cat_id3"];
            if(manuf.cat_id4)
                [tempManufacturerDictionary setObject:manuf.cat_id4 forKey:@"cat_id4"];
            if(manuf.cat_id5)
                [tempManufacturerDictionary setObject:manuf.cat_id5 forKey:@"cat_id5"];
            if(manuf.cat_id6)
                [tempManufacturerDictionary setObject:manuf.cat_id6 forKey:@"cat_id6"];
            if(manuf.cat_id7)
                [tempManufacturerDictionary setObject:manuf.cat_id7 forKey:@"cat_id7"];
            if(manuf.cat_id8)
                [tempManufacturerDictionary setObject:manuf.cat_id8 forKey:@"cat_id8"];
            if(manuf.cat_id9)
                [tempManufacturerDictionary setObject:manuf.cat_id9 forKey:@"cat_id9"];
            if(manuf.cat_id10)
                [tempManufacturerDictionary setObject:manuf.cat_id10 forKey:@"cat_id10"];
            if(manuf.address)
                [tempManufacturerDictionary setObject:manuf.address forKey:@"address"];
            if(manuf.preview)
                [tempManufacturerDictionary setObject:manuf.preview forKey:@"preview"];
            if(manuf.details)
                [tempManufacturerDictionary setObject:manuf.details forKey:@"details"];
            if(manuf.phone)
                [tempManufacturerDictionary setObject:manuf.phone forKey:@"phone"];
            if(manuf.website)
                [tempManufacturerDictionary setObject:manuf.website forKey:@"website"];
            if(manuf.latitude)
                [tempManufacturerDictionary setObject:manuf.latitude forKey:@"latitude"];
            if(manuf.longitude)
                [tempManufacturerDictionary setObject:manuf.longitude forKey:@"longitude"];
            if(manuf.featured)
                [tempManufacturerDictionary setObject:manuf.featured forKey:@"featured"];
            if(manuf.star_rating)
                [tempManufacturerDictionary setObject:manuf.star_rating forKey:@"star_rating"];
            if(manuf.type)
                [tempManufacturerDictionary setObject:manuf.type forKey:@"type"];
            if(manuf.thumb)
                [tempManufacturerDictionary setObject:manuf.thumb forKey:@"thumb"];
            if(manuf.weight)
                [tempManufacturerDictionary setObject:manuf.weight forKey:@"weight"];
            if(manuf.add_to_favorites)
                [tempManufacturerDictionary setObject:manuf.add_to_favorites forKey:@"add_to_favorites"];
            if(manuf.share_listing)
                [tempManufacturerDictionary setObject:manuf.share_listing forKey:@"share_listing"];
            if(manuf.fixed_order)
                [tempManufacturerDictionary setObject:manuf.fixed_order forKey:@"fixed_order"];
            if(manuf.show_call)
                [tempManufacturerDictionary setObject:manuf.show_call forKey:@"show_call"];
            if(manuf.show_web)
                [tempManufacturerDictionary setObject:manuf.show_web forKey:@"show_web"];
            if(manuf.website_text)
                [tempManufacturerDictionary setObject:manuf.website_text forKey:@"website_text"];
            if(manuf.email_address)
                [tempManufacturerDictionary setObject:manuf.email_address forKey:@"email_address"];
            if(manuf.show_email)
                [tempManufacturerDictionary setObject:manuf.show_email forKey:@"show_email"];
            if(manuf.email_text)
                [tempManufacturerDictionary setObject:manuf.email_text forKey:@"email_text"];
            if(manuf.disp_address)
                [tempManufacturerDictionary setObject:manuf.disp_address forKey:@"disp_address"];
            
            // }
            //  @catch (NSException *exception) {
            
            // }
        }
        
        
        
        
        CLLocation *plLocation= [[CLLocation alloc] initWithLatitude:[manuf.latitude doubleValue] longitude:[manuf.longitude doubleValue]];
        CLLocationDistance meters = [location_updated distanceFromLocation:plLocation];
        float actdist;
        actdist = meters * 0.000621371;
        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
        [nf setMaximumFractionDigits:2];
        NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
        [tempManufacturerDictionary setObject:trimmed forKey:@"dist_offline"];
        
        [array2 addObject:tempManufacturerDictionary];
        
        
        
    }
    
    
    
    NSSortDescriptor *descriptor2 = [[NSSortDescriptor alloc] initWithKey:@"dist_offline"  ascending:YES];
    NSArray *arrtemp32 = [array2 sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor2,nil]];
    // NSArray *arrVals = [arrtemp3 copy];
    array2=[NSMutableArray arrayWithArray:arrtemp32];
    
    // array=[NSMutableArray arrayWithArray:array1];
    [array addObjectsFromArray:array2];
    
    ///////////array3//////////
    
    NSError *error3;
    PDAppDelegate *appDelegate3 = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context3 = [appDelegate3 managedObjectContext];
    NSFetchRequest *fetchRequest3 = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity3 = [NSEntityDescription
                                    entityForName:@"Businesses" inManagedObjectContext:context3];
    fetchRequest3.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest3 setEntity:entity3];
    
    int l;
    
    
    
    
    {
        NSPredicate * parentIdPredicate3 = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"cat_id4==%@",[dictDetails objectForKey:@"Id"]]];
        
        [fetchRequest3 setPredicate:parentIdPredicate3];
    }
    
    
    
    NSArray *fetchedObjects3 = [context3 executeFetchRequest:fetchRequest3 error:&error3];
    
    
    
    
    
    
    
    NSLog( @"error %@",error3);
    
    
    NSMutableArray*   array3  = [NSMutableArray array];
    
    for (Businesses *manuf in fetchedObjects3) {
        NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
        
        
        
        {
            
            // @try
            // {
            
            if(manuf.busId)
                [tempManufacturerDictionary setObject:manuf.busId forKey:@"Id"];
            if(manuf.image)
                [tempManufacturerDictionary setObject:manuf.image forKey:@"image"];
            if(manuf.name)
                [tempManufacturerDictionary setObject:manuf.name forKey:@"name"];
            if(manuf.cat_id)
                [tempManufacturerDictionary setObject:manuf.cat_id forKey:@"cat_id"];
            if(manuf.cat_id2)
                [tempManufacturerDictionary setObject:manuf.cat_id2 forKey:@"cat_id2"];
            if(manuf.cat_id3)
                [tempManufacturerDictionary setObject:manuf.cat_id3 forKey:@"cat_id3"];
            if(manuf.cat_id4)
                [tempManufacturerDictionary setObject:manuf.cat_id4 forKey:@"cat_id4"];
            if(manuf.cat_id5)
                [tempManufacturerDictionary setObject:manuf.cat_id5 forKey:@"cat_id5"];
            if(manuf.cat_id6)
                [tempManufacturerDictionary setObject:manuf.cat_id6 forKey:@"cat_id6"];
            if(manuf.cat_id7)
                [tempManufacturerDictionary setObject:manuf.cat_id7 forKey:@"cat_id7"];
            if(manuf.cat_id8)
                [tempManufacturerDictionary setObject:manuf.cat_id8 forKey:@"cat_id8"];
            if(manuf.cat_id9)
                [tempManufacturerDictionary setObject:manuf.cat_id9 forKey:@"cat_id9"];
            if(manuf.cat_id10)
                [tempManufacturerDictionary setObject:manuf.cat_id10 forKey:@"cat_id10"];
            if(manuf.address)
                [tempManufacturerDictionary setObject:manuf.address forKey:@"address"];
            if(manuf.preview)
                [tempManufacturerDictionary setObject:manuf.preview forKey:@"preview"];
            if(manuf.details)
                [tempManufacturerDictionary setObject:manuf.details forKey:@"details"];
            if(manuf.phone)
                [tempManufacturerDictionary setObject:manuf.phone forKey:@"phone"];
            if(manuf.website)
                [tempManufacturerDictionary setObject:manuf.website forKey:@"website"];
            if(manuf.latitude)
                [tempManufacturerDictionary setObject:manuf.latitude forKey:@"latitude"];
            if(manuf.longitude)
                [tempManufacturerDictionary setObject:manuf.longitude forKey:@"longitude"];
            if(manuf.featured)
                [tempManufacturerDictionary setObject:manuf.featured forKey:@"featured"];
            if(manuf.star_rating)
                [tempManufacturerDictionary setObject:manuf.star_rating forKey:@"star_rating"];
            if(manuf.type)
                [tempManufacturerDictionary setObject:manuf.type forKey:@"type"];
            if(manuf.thumb)
                [tempManufacturerDictionary setObject:manuf.thumb forKey:@"thumb"];
            if(manuf.weight)
                [tempManufacturerDictionary setObject:manuf.weight forKey:@"weight"];
            if(manuf.add_to_favorites)
                [tempManufacturerDictionary setObject:manuf.add_to_favorites forKey:@"add_to_favorites"];
            if(manuf.share_listing)
                [tempManufacturerDictionary setObject:manuf.share_listing forKey:@"share_listing"];
            if(manuf.fixed_order)
                [tempManufacturerDictionary setObject:manuf.fixed_order forKey:@"fixed_order"];
            if(manuf.show_call)
                [tempManufacturerDictionary setObject:manuf.show_call forKey:@"show_call"];
            if(manuf.show_web)
                [tempManufacturerDictionary setObject:manuf.show_web forKey:@"show_web"];
            if(manuf.website_text)
                [tempManufacturerDictionary setObject:manuf.website_text forKey:@"website_text"];
            if(manuf.email_address)
                [tempManufacturerDictionary setObject:manuf.email_address forKey:@"email_address"];
            if(manuf.show_email)
                [tempManufacturerDictionary setObject:manuf.show_email forKey:@"show_email"];
            if(manuf.email_text)
                [tempManufacturerDictionary setObject:manuf.email_text forKey:@"email_text"];
            if(manuf.disp_address)
                [tempManufacturerDictionary setObject:manuf.disp_address forKey:@"disp_address"];
            
            // }
            //  @catch (NSException *exception) {
            
            // }
        }
        
        
        
        CLLocation *plLocation= [[CLLocation alloc] initWithLatitude:[manuf.latitude doubleValue] longitude:[manuf.longitude doubleValue]];
        CLLocationDistance meters = [location_updated distanceFromLocation:plLocation];
        float actdist;
        actdist = meters * 0.000621371;
        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
        [nf setMaximumFractionDigits:2];
        NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
        [tempManufacturerDictionary setObject:trimmed forKey:@"dist_offline"];
        
        [array3 addObject:tempManufacturerDictionary];
        
        
        
    }
    
    
    
    NSSortDescriptor *descriptor3 = [[NSSortDescriptor alloc] initWithKey:@"dist_offline"  ascending:YES];
    NSArray *arrtemp33 = [array3 sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor3,nil]];
    // NSArray *arrVals = [arrtemp3 copy];
    array3=[NSMutableArray arrayWithArray:arrtemp33];
    
    // array=[NSMutableArray arrayWithArray:array1];
    [array addObjectsFromArray:array3];
    
    
    ////////////////////////////////////////
    
    /////////////cat5
    /////////////////////////////////////////
    
    NSError *error5;
    PDAppDelegate *appDelegate5 = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context5 = [appDelegate5 managedObjectContext];
    NSFetchRequest *fetchRequest5 = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity5 = [NSEntityDescription
                                    entityForName:@"Businesses" inManagedObjectContext:context5];
    fetchRequest5.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest5 setEntity:entity5];
    
    // int k;
    
    
    
    
    {
        NSPredicate * parentIdPredicate5 = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"cat_id5==%@",[dictDetails objectForKey:@"Id"]]];
        
        [fetchRequest5 setPredicate:parentIdPredicate5];
    }
    
    
    
    NSArray *fetchedObjects5 = [context5 executeFetchRequest:fetchRequest5 error:&error5];
    
    
    
    
    
    
    
    NSLog( @"error %@",error5);
    
    
    NSMutableArray*   array5  = [NSMutableArray array];
    
    for (Businesses *manuf in fetchedObjects5) {
        NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
        
        
        
        {
            
            // @try
            // {
            
            if(manuf.busId)
                [tempManufacturerDictionary setObject:manuf.busId forKey:@"Id"];
            if(manuf.image)
                [tempManufacturerDictionary setObject:manuf.image forKey:@"image"];
            if(manuf.name)
                [tempManufacturerDictionary setObject:manuf.name forKey:@"name"];
            if(manuf.cat_id)
                [tempManufacturerDictionary setObject:manuf.cat_id forKey:@"cat_id"];
            if(manuf.cat_id2)
                [tempManufacturerDictionary setObject:manuf.cat_id2 forKey:@"cat_id2"];
            if(manuf.cat_id3)
                [tempManufacturerDictionary setObject:manuf.cat_id3 forKey:@"cat_id3"];
            if(manuf.cat_id4)
                [tempManufacturerDictionary setObject:manuf.cat_id4 forKey:@"cat_id4"];
            if(manuf.cat_id5)
                [tempManufacturerDictionary setObject:manuf.cat_id5 forKey:@"cat_id5"];
            if(manuf.cat_id6)
                [tempManufacturerDictionary setObject:manuf.cat_id6 forKey:@"cat_id6"];
            if(manuf.cat_id7)
                [tempManufacturerDictionary setObject:manuf.cat_id7 forKey:@"cat_id7"];
            if(manuf.cat_id8)
                [tempManufacturerDictionary setObject:manuf.cat_id8 forKey:@"cat_id8"];
            if(manuf.cat_id9)
                [tempManufacturerDictionary setObject:manuf.cat_id9 forKey:@"cat_id9"];
            if(manuf.cat_id10)
                [tempManufacturerDictionary setObject:manuf.cat_id10 forKey:@"cat_id10"];
            if(manuf.address)
                [tempManufacturerDictionary setObject:manuf.address forKey:@"address"];
            if(manuf.preview)
                [tempManufacturerDictionary setObject:manuf.preview forKey:@"preview"];
            if(manuf.details)
                [tempManufacturerDictionary setObject:manuf.details forKey:@"details"];
            if(manuf.phone)
                [tempManufacturerDictionary setObject:manuf.phone forKey:@"phone"];
            if(manuf.website)
                [tempManufacturerDictionary setObject:manuf.website forKey:@"website"];
            if(manuf.latitude)
                [tempManufacturerDictionary setObject:manuf.latitude forKey:@"latitude"];
            if(manuf.longitude)
                [tempManufacturerDictionary setObject:manuf.longitude forKey:@"longitude"];
            if(manuf.featured)
                [tempManufacturerDictionary setObject:manuf.featured forKey:@"featured"];
            if(manuf.star_rating)
                [tempManufacturerDictionary setObject:manuf.star_rating forKey:@"star_rating"];
            if(manuf.type)
                [tempManufacturerDictionary setObject:manuf.type forKey:@"type"];
            if(manuf.thumb)
                [tempManufacturerDictionary setObject:manuf.thumb forKey:@"thumb"];
            if(manuf.weight)
                [tempManufacturerDictionary setObject:manuf.weight forKey:@"weight"];
            if(manuf.add_to_favorites)
                [tempManufacturerDictionary setObject:manuf.add_to_favorites forKey:@"add_to_favorites"];
            if(manuf.share_listing)
                [tempManufacturerDictionary setObject:manuf.share_listing forKey:@"share_listing"];
            if(manuf.fixed_order)
                [tempManufacturerDictionary setObject:manuf.fixed_order forKey:@"fixed_order"];
            if(manuf.show_call)
                [tempManufacturerDictionary setObject:manuf.show_call forKey:@"show_call"];
            if(manuf.show_web)
                [tempManufacturerDictionary setObject:manuf.show_web forKey:@"show_web"];
            if(manuf.website_text)
                [tempManufacturerDictionary setObject:manuf.website_text forKey:@"website_text"];
            if(manuf.email_address)
                [tempManufacturerDictionary setObject:manuf.email_address forKey:@"email_address"];
            if(manuf.show_email)
                [tempManufacturerDictionary setObject:manuf.show_email forKey:@"show_email"];
            if(manuf.email_text)
                [tempManufacturerDictionary setObject:manuf.email_text forKey:@"email_text"];
            if(manuf.disp_address)
                [tempManufacturerDictionary setObject:manuf.disp_address forKey:@"disp_address"];
            
            // }
            //  @catch (NSException *exception) {
            
            // }
        }
        
        
        
        CLLocation *plLocation= [[CLLocation alloc] initWithLatitude:[manuf.latitude doubleValue] longitude:[manuf.longitude doubleValue]];
        CLLocationDistance meters = [location_updated distanceFromLocation:plLocation];
        float actdist;
        actdist = meters * 0.000621371;
        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
        [nf setMaximumFractionDigits:2];
        NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
        [tempManufacturerDictionary setObject:trimmed forKey:@"dist_offline"];
        
        [array5 addObject:tempManufacturerDictionary];
        
        
        
    }
    
    
    
    NSSortDescriptor *descriptor5 = [[NSSortDescriptor alloc] initWithKey:@"dist_offline"  ascending:YES];
    NSArray *arrtemp35 = [array5 sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor5,nil]];
    // NSArray *arrVals = [arrtemp3 copy];
    array5=[NSMutableArray arrayWithArray:arrtemp35];
    
    // array=[NSMutableArray arrayWithArray:array1];
    [array addObjectsFromArray:array5];
    
    //////////////
    
    
    /////////////cat6
    /////////////////////////////////////////
    
    NSError *error6;
    PDAppDelegate *appDelegate6 = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context6 = [appDelegate6 managedObjectContext];
    NSFetchRequest *fetchRequest6 = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity6 = [NSEntityDescription
                                    entityForName:@"Businesses" inManagedObjectContext:context6];
    fetchRequest6.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest6 setEntity:entity6];
    
    // int k;
    
    
    
    
    {
        NSPredicate * parentIdPredicate6 = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"cat_id6==%@",[dictDetails objectForKey:@"Id"]]];
        
        [fetchRequest6 setPredicate:parentIdPredicate6];
    }
    
    
    
    NSArray *fetchedObjects6 = [context6 executeFetchRequest:fetchRequest6 error:&error6];
    
    
    
    
    
    
    
    NSLog( @"error %@",error6);
    
    
    NSMutableArray*   array6  = [NSMutableArray array];
    
    for (Businesses *manuf in fetchedObjects6) {
        NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
        
        
        
        {
            
            // @try
            // {
            
            if(manuf.busId)
                [tempManufacturerDictionary setObject:manuf.busId forKey:@"Id"];
            if(manuf.image)
                [tempManufacturerDictionary setObject:manuf.image forKey:@"image"];
            if(manuf.name)
                [tempManufacturerDictionary setObject:manuf.name forKey:@"name"];
            if(manuf.cat_id)
                [tempManufacturerDictionary setObject:manuf.cat_id forKey:@"cat_id"];
            if(manuf.cat_id2)
                [tempManufacturerDictionary setObject:manuf.cat_id2 forKey:@"cat_id2"];
            if(manuf.cat_id3)
                [tempManufacturerDictionary setObject:manuf.cat_id3 forKey:@"cat_id3"];
            if(manuf.cat_id4)
                [tempManufacturerDictionary setObject:manuf.cat_id4 forKey:@"cat_id4"];
            if(manuf.cat_id5)
                [tempManufacturerDictionary setObject:manuf.cat_id5 forKey:@"cat_id5"];
            if(manuf.cat_id6)
                [tempManufacturerDictionary setObject:manuf.cat_id6 forKey:@"cat_id6"];
            if(manuf.cat_id7)
                [tempManufacturerDictionary setObject:manuf.cat_id7 forKey:@"cat_id7"];
            if(manuf.cat_id8)
                [tempManufacturerDictionary setObject:manuf.cat_id8 forKey:@"cat_id8"];
            if(manuf.cat_id9)
                [tempManufacturerDictionary setObject:manuf.cat_id9 forKey:@"cat_id9"];
            if(manuf.cat_id10)
                [tempManufacturerDictionary setObject:manuf.cat_id10 forKey:@"cat_id10"];
            if(manuf.address)
                [tempManufacturerDictionary setObject:manuf.address forKey:@"address"];
            if(manuf.preview)
                [tempManufacturerDictionary setObject:manuf.preview forKey:@"preview"];
            if(manuf.details)
                [tempManufacturerDictionary setObject:manuf.details forKey:@"details"];
            if(manuf.phone)
                [tempManufacturerDictionary setObject:manuf.phone forKey:@"phone"];
            if(manuf.website)
                [tempManufacturerDictionary setObject:manuf.website forKey:@"website"];
            if(manuf.latitude)
                [tempManufacturerDictionary setObject:manuf.latitude forKey:@"latitude"];
            if(manuf.longitude)
                [tempManufacturerDictionary setObject:manuf.longitude forKey:@"longitude"];
            if(manuf.featured)
                [tempManufacturerDictionary setObject:manuf.featured forKey:@"featured"];
            if(manuf.star_rating)
                [tempManufacturerDictionary setObject:manuf.star_rating forKey:@"star_rating"];
            if(manuf.type)
                [tempManufacturerDictionary setObject:manuf.type forKey:@"type"];
            if(manuf.thumb)
                [tempManufacturerDictionary setObject:manuf.thumb forKey:@"thumb"];
            if(manuf.weight)
                [tempManufacturerDictionary setObject:manuf.weight forKey:@"weight"];
            if(manuf.add_to_favorites)
                [tempManufacturerDictionary setObject:manuf.add_to_favorites forKey:@"add_to_favorites"];
            if(manuf.share_listing)
                [tempManufacturerDictionary setObject:manuf.share_listing forKey:@"share_listing"];
            if(manuf.fixed_order)
                [tempManufacturerDictionary setObject:manuf.fixed_order forKey:@"fixed_order"];
            if(manuf.show_call)
                [tempManufacturerDictionary setObject:manuf.show_call forKey:@"show_call"];
            if(manuf.show_web)
                [tempManufacturerDictionary setObject:manuf.show_web forKey:@"show_web"];
            if(manuf.website_text)
                [tempManufacturerDictionary setObject:manuf.website_text forKey:@"website_text"];
            if(manuf.email_address)
                [tempManufacturerDictionary setObject:manuf.email_address forKey:@"email_address"];
            if(manuf.show_email)
                [tempManufacturerDictionary setObject:manuf.show_email forKey:@"show_email"];
            if(manuf.email_text)
                [tempManufacturerDictionary setObject:manuf.email_text forKey:@"email_text"];
            if(manuf.disp_address)
                [tempManufacturerDictionary setObject:manuf.disp_address forKey:@"disp_address"];
            
            // }
            //  @catch (NSException *exception) {
            
            // }
        }
        
        
        
        CLLocation *plLocation= [[CLLocation alloc] initWithLatitude:[manuf.latitude doubleValue] longitude:[manuf.longitude doubleValue]];
        CLLocationDistance meters = [location_updated distanceFromLocation:plLocation];
        float actdist;
        actdist = meters * 0.000621371;
        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
        [nf setMaximumFractionDigits:2];
        NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
        [tempManufacturerDictionary setObject:trimmed forKey:@"dist_offline"];
        
        [array6 addObject:tempManufacturerDictionary];
        
        
        
    }
    
    
    
    NSSortDescriptor *descriptor6 = [[NSSortDescriptor alloc] initWithKey:@"dist_offline"  ascending:YES];
    NSArray *arrtemp36 = [array6 sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor6,nil]];
    // NSArray *arrVals = [arrtemp3 copy];
    array6=[NSMutableArray arrayWithArray:arrtemp36];
    
    // array=[NSMutableArray arrayWithArray:array1];
    [array addObjectsFromArray:array6];
    
    //////////////
    
    /////////////cat7
    /////////////////////////////////////////
    
    NSError *error7;
    PDAppDelegate *appDelegate7 = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context7 = [appDelegate7 managedObjectContext];
    NSFetchRequest *fetchRequest7 = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity7 = [NSEntityDescription
                                    entityForName:@"Businesses" inManagedObjectContext:context7];
    fetchRequest7.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest7 setEntity:entity7];
    
    // int k;
    
    
    
    
    {
        NSPredicate * parentIdPredicate7 = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"cat_id7==%@",[dictDetails objectForKey:@"Id"]]];
        
        [fetchRequest7 setPredicate:parentIdPredicate7];
    }
    
    
    
    NSArray *fetchedObjects7 = [context7 executeFetchRequest:fetchRequest7 error:&error7];
    
    
    
    
    
    
    
    NSLog( @"error %@",error7);
    
    
    NSMutableArray*   array7  = [NSMutableArray array];
    
    for (Businesses *manuf in fetchedObjects7) {
        NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
        
        
        
        {
            
            // @try
            // {
            
            if(manuf.busId)
                [tempManufacturerDictionary setObject:manuf.busId forKey:@"Id"];
            if(manuf.image)
                [tempManufacturerDictionary setObject:manuf.image forKey:@"image"];
            if(manuf.name)
                [tempManufacturerDictionary setObject:manuf.name forKey:@"name"];
            if(manuf.cat_id)
                [tempManufacturerDictionary setObject:manuf.cat_id forKey:@"cat_id"];
            if(manuf.cat_id2)
                [tempManufacturerDictionary setObject:manuf.cat_id2 forKey:@"cat_id2"];
            if(manuf.cat_id3)
                [tempManufacturerDictionary setObject:manuf.cat_id3 forKey:@"cat_id3"];
            if(manuf.cat_id4)
                [tempManufacturerDictionary setObject:manuf.cat_id4 forKey:@"cat_id4"];
            if(manuf.cat_id5)
                [tempManufacturerDictionary setObject:manuf.cat_id5 forKey:@"cat_id5"];
            if(manuf.cat_id6)
                [tempManufacturerDictionary setObject:manuf.cat_id6 forKey:@"cat_id6"];
            if(manuf.cat_id7)
                [tempManufacturerDictionary setObject:manuf.cat_id7 forKey:@"cat_id7"];
            if(manuf.cat_id8)
                [tempManufacturerDictionary setObject:manuf.cat_id8 forKey:@"cat_id8"];
            if(manuf.cat_id9)
                [tempManufacturerDictionary setObject:manuf.cat_id9 forKey:@"cat_id9"];
            if(manuf.cat_id10)
                [tempManufacturerDictionary setObject:manuf.cat_id10 forKey:@"cat_id10"];
            if(manuf.address)
                [tempManufacturerDictionary setObject:manuf.address forKey:@"address"];
            if(manuf.preview)
                [tempManufacturerDictionary setObject:manuf.preview forKey:@"preview"];
            if(manuf.details)
                [tempManufacturerDictionary setObject:manuf.details forKey:@"details"];
            if(manuf.phone)
                [tempManufacturerDictionary setObject:manuf.phone forKey:@"phone"];
            if(manuf.website)
                [tempManufacturerDictionary setObject:manuf.website forKey:@"website"];
            if(manuf.latitude)
                [tempManufacturerDictionary setObject:manuf.latitude forKey:@"latitude"];
            if(manuf.longitude)
                [tempManufacturerDictionary setObject:manuf.longitude forKey:@"longitude"];
            if(manuf.featured)
                [tempManufacturerDictionary setObject:manuf.featured forKey:@"featured"];
            if(manuf.star_rating)
                [tempManufacturerDictionary setObject:manuf.star_rating forKey:@"star_rating"];
            if(manuf.type)
                [tempManufacturerDictionary setObject:manuf.type forKey:@"type"];
            if(manuf.thumb)
                [tempManufacturerDictionary setObject:manuf.thumb forKey:@"thumb"];
            if(manuf.weight)
                [tempManufacturerDictionary setObject:manuf.weight forKey:@"weight"];
            if(manuf.add_to_favorites)
                [tempManufacturerDictionary setObject:manuf.add_to_favorites forKey:@"add_to_favorites"];
            if(manuf.share_listing)
                [tempManufacturerDictionary setObject:manuf.share_listing forKey:@"share_listing"];
            if(manuf.fixed_order)
                [tempManufacturerDictionary setObject:manuf.fixed_order forKey:@"fixed_order"];
            if(manuf.show_call)
                [tempManufacturerDictionary setObject:manuf.show_call forKey:@"show_call"];
            if(manuf.show_web)
                [tempManufacturerDictionary setObject:manuf.show_web forKey:@"show_web"];
            if(manuf.website_text)
                [tempManufacturerDictionary setObject:manuf.website_text forKey:@"website_text"];
            if(manuf.email_address)
                [tempManufacturerDictionary setObject:manuf.email_address forKey:@"email_address"];
            if(manuf.show_email)
                [tempManufacturerDictionary setObject:manuf.show_email forKey:@"show_email"];
            if(manuf.email_text)
                [tempManufacturerDictionary setObject:manuf.email_text forKey:@"email_text"];
            if(manuf.disp_address)
                [tempManufacturerDictionary setObject:manuf.disp_address forKey:@"disp_address"];
            
            // }
            //  @catch (NSException *exception) {
            
            // }
        }
        
        
        
        CLLocation *plLocation= [[CLLocation alloc] initWithLatitude:[manuf.latitude doubleValue] longitude:[manuf.longitude doubleValue]];
        CLLocationDistance meters = [location_updated distanceFromLocation:plLocation];
        float actdist;
        actdist = meters * 0.000621371;
        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
        [nf setMaximumFractionDigits:2];
        NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
        [tempManufacturerDictionary setObject:trimmed forKey:@"dist_offline"];
        
        [array7 addObject:tempManufacturerDictionary];
        
        
        
    }
    
    
    
    NSSortDescriptor *descriptor7 = [[NSSortDescriptor alloc] initWithKey:@"dist_offline"  ascending:YES];
    NSArray *arrtemp37 = [array7 sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor7,nil]];
    // NSArray *arrVals = [arrtemp3 copy];
    array7=[NSMutableArray arrayWithArray:arrtemp37];
    
    // array=[NSMutableArray arrayWithArray:array1];
    [array addObjectsFromArray:array7];
    
    //////////////
    
    
    /////////////cat8
    /////////////////////////////////////////
    
    NSError *error8;
    PDAppDelegate *appDelegate8 = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context8 = [appDelegate8 managedObjectContext];
    NSFetchRequest *fetchRequest8 = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity8 = [NSEntityDescription
                                    entityForName:@"Businesses" inManagedObjectContext:context8];
    fetchRequest8.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest8 setEntity:entity8];
    
    // int k;
    
    
    
    
    {
        NSPredicate * parentIdPredicate8 = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"cat_id8==%@",[dictDetails objectForKey:@"Id"]]];
        
        [fetchRequest8 setPredicate:parentIdPredicate8];
    }
    
    
    
    NSArray *fetchedObjects8 = [context8 executeFetchRequest:fetchRequest8 error:&error8];
    
    
    
    
    
    
    
    NSLog( @"error %@",error8);
    
    
    NSMutableArray*   array8  = [NSMutableArray array];
    
    for (Businesses *manuf in fetchedObjects8) {
        NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
        
        
        
        {
            
            // @try
            // {
            
            if(manuf.busId)
                [tempManufacturerDictionary setObject:manuf.busId forKey:@"Id"];
            if(manuf.image)
                [tempManufacturerDictionary setObject:manuf.image forKey:@"image"];
            if(manuf.name)
                [tempManufacturerDictionary setObject:manuf.name forKey:@"name"];
            if(manuf.cat_id)
                [tempManufacturerDictionary setObject:manuf.cat_id forKey:@"cat_id"];
            if(manuf.cat_id2)
                [tempManufacturerDictionary setObject:manuf.cat_id2 forKey:@"cat_id2"];
            if(manuf.cat_id3)
                [tempManufacturerDictionary setObject:manuf.cat_id3 forKey:@"cat_id3"];
            if(manuf.cat_id4)
                [tempManufacturerDictionary setObject:manuf.cat_id4 forKey:@"cat_id4"];
            if(manuf.cat_id5)
                [tempManufacturerDictionary setObject:manuf.cat_id5 forKey:@"cat_id5"];
            if(manuf.cat_id6)
                [tempManufacturerDictionary setObject:manuf.cat_id6 forKey:@"cat_id6"];
            if(manuf.cat_id7)
                [tempManufacturerDictionary setObject:manuf.cat_id7 forKey:@"cat_id7"];
            if(manuf.cat_id8)
                [tempManufacturerDictionary setObject:manuf.cat_id8 forKey:@"cat_id8"];
            if(manuf.cat_id9)
                [tempManufacturerDictionary setObject:manuf.cat_id9 forKey:@"cat_id9"];
            if(manuf.cat_id10)
                [tempManufacturerDictionary setObject:manuf.cat_id10 forKey:@"cat_id10"];
            if(manuf.address)
                [tempManufacturerDictionary setObject:manuf.address forKey:@"address"];
            if(manuf.preview)
                [tempManufacturerDictionary setObject:manuf.preview forKey:@"preview"];
            if(manuf.details)
                [tempManufacturerDictionary setObject:manuf.details forKey:@"details"];
            if(manuf.phone)
                [tempManufacturerDictionary setObject:manuf.phone forKey:@"phone"];
            if(manuf.website)
                [tempManufacturerDictionary setObject:manuf.website forKey:@"website"];
            if(manuf.latitude)
                [tempManufacturerDictionary setObject:manuf.latitude forKey:@"latitude"];
            if(manuf.longitude)
                [tempManufacturerDictionary setObject:manuf.longitude forKey:@"longitude"];
            if(manuf.featured)
                [tempManufacturerDictionary setObject:manuf.featured forKey:@"featured"];
            if(manuf.star_rating)
                [tempManufacturerDictionary setObject:manuf.star_rating forKey:@"star_rating"];
            if(manuf.type)
                [tempManufacturerDictionary setObject:manuf.type forKey:@"type"];
            if(manuf.thumb)
                [tempManufacturerDictionary setObject:manuf.thumb forKey:@"thumb"];
            if(manuf.weight)
                [tempManufacturerDictionary setObject:manuf.weight forKey:@"weight"];
            if(manuf.add_to_favorites)
                [tempManufacturerDictionary setObject:manuf.add_to_favorites forKey:@"add_to_favorites"];
            if(manuf.share_listing)
                [tempManufacturerDictionary setObject:manuf.share_listing forKey:@"share_listing"];
            if(manuf.fixed_order)
                [tempManufacturerDictionary setObject:manuf.fixed_order forKey:@"fixed_order"];
            if(manuf.show_call)
                [tempManufacturerDictionary setObject:manuf.show_call forKey:@"show_call"];
            if(manuf.show_web)
                [tempManufacturerDictionary setObject:manuf.show_web forKey:@"show_web"];
            if(manuf.website_text)
                [tempManufacturerDictionary setObject:manuf.website_text forKey:@"website_text"];
            if(manuf.email_address)
                [tempManufacturerDictionary setObject:manuf.email_address forKey:@"email_address"];
            if(manuf.show_email)
                [tempManufacturerDictionary setObject:manuf.show_email forKey:@"show_email"];
            if(manuf.email_text)
                [tempManufacturerDictionary setObject:manuf.email_text forKey:@"email_text"];
            if(manuf.disp_address)
                [tempManufacturerDictionary setObject:manuf.disp_address forKey:@"disp_address"];
            
            // }
            //  @catch (NSException *exception) {
            
            // }
        }
        
        
        
        CLLocation *plLocation= [[CLLocation alloc] initWithLatitude:[manuf.latitude doubleValue] longitude:[manuf.longitude doubleValue]];
        CLLocationDistance meters = [location_updated distanceFromLocation:plLocation];
        float actdist;
        actdist = meters * 0.000621371;
        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
        [nf setMaximumFractionDigits:2];
        NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
        [tempManufacturerDictionary setObject:trimmed forKey:@"dist_offline"];
        
        [array8 addObject:tempManufacturerDictionary];
        
        
        
    }
    
    
    
    NSSortDescriptor *descriptor8 = [[NSSortDescriptor alloc] initWithKey:@"dist_offline"  ascending:YES];
    NSArray *arrtemp38 = [array8 sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor8,nil]];
    // NSArray *arrVals = [arrtemp3 copy];
    array8=[NSMutableArray arrayWithArray:arrtemp38];
    
    // array=[NSMutableArray arrayWithArray:array1];
    [array addObjectsFromArray:array8];
    
    //////////////
    
    /////////////cat9
    /////////////////////////////////////////
    
    NSError *error9;
    PDAppDelegate *appDelegate9 = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context9 = [appDelegate9 managedObjectContext];
    NSFetchRequest *fetchRequest9 = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity9 = [NSEntityDescription
                                    entityForName:@"Businesses" inManagedObjectContext:context9];
    fetchRequest9.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest9 setEntity:entity9];
    
    // int k;
    
    
    
    
    {
        NSPredicate * parentIdPredicate9 = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"cat_id9==%@",[dictDetails objectForKey:@"Id"]]];
        
        [fetchRequest9 setPredicate:parentIdPredicate9];
    }
    
    
    
    NSArray *fetchedObjects9 = [context9 executeFetchRequest:fetchRequest9 error:&error9];
    
    
    
    
    
    
    
    NSLog( @"error %@",error9);
    
    
    NSMutableArray*   array9  = [NSMutableArray array];
    
    for (Businesses *manuf in fetchedObjects9) {
        NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
        
        
        
        {
            
            // @try
            // {
            
            if(manuf.busId)
                [tempManufacturerDictionary setObject:manuf.busId forKey:@"Id"];
            if(manuf.image)
                [tempManufacturerDictionary setObject:manuf.image forKey:@"image"];
            if(manuf.name)
                [tempManufacturerDictionary setObject:manuf.name forKey:@"name"];
            if(manuf.cat_id)
                [tempManufacturerDictionary setObject:manuf.cat_id forKey:@"cat_id"];
            if(manuf.cat_id2)
                [tempManufacturerDictionary setObject:manuf.cat_id2 forKey:@"cat_id2"];
            if(manuf.cat_id3)
                [tempManufacturerDictionary setObject:manuf.cat_id3 forKey:@"cat_id3"];
            if(manuf.cat_id4)
                [tempManufacturerDictionary setObject:manuf.cat_id4 forKey:@"cat_id4"];
            if(manuf.cat_id5)
                [tempManufacturerDictionary setObject:manuf.cat_id5 forKey:@"cat_id5"];
            if(manuf.cat_id6)
                [tempManufacturerDictionary setObject:manuf.cat_id6 forKey:@"cat_id6"];
            if(manuf.cat_id7)
                [tempManufacturerDictionary setObject:manuf.cat_id7 forKey:@"cat_id7"];
            if(manuf.cat_id8)
                [tempManufacturerDictionary setObject:manuf.cat_id8 forKey:@"cat_id8"];
            if(manuf.cat_id9)
                [tempManufacturerDictionary setObject:manuf.cat_id9 forKey:@"cat_id9"];
            if(manuf.cat_id10)
                [tempManufacturerDictionary setObject:manuf.cat_id10 forKey:@"cat_id10"];
            if(manuf.address)
                [tempManufacturerDictionary setObject:manuf.address forKey:@"address"];
            if(manuf.preview)
                [tempManufacturerDictionary setObject:manuf.preview forKey:@"preview"];
            if(manuf.details)
                [tempManufacturerDictionary setObject:manuf.details forKey:@"details"];
            if(manuf.phone)
                [tempManufacturerDictionary setObject:manuf.phone forKey:@"phone"];
            if(manuf.website)
                [tempManufacturerDictionary setObject:manuf.website forKey:@"website"];
            if(manuf.latitude)
                [tempManufacturerDictionary setObject:manuf.latitude forKey:@"latitude"];
            if(manuf.longitude)
                [tempManufacturerDictionary setObject:manuf.longitude forKey:@"longitude"];
            if(manuf.featured)
                [tempManufacturerDictionary setObject:manuf.featured forKey:@"featured"];
            if(manuf.star_rating)
                [tempManufacturerDictionary setObject:manuf.star_rating forKey:@"star_rating"];
            if(manuf.type)
                [tempManufacturerDictionary setObject:manuf.type forKey:@"type"];
            if(manuf.thumb)
                [tempManufacturerDictionary setObject:manuf.thumb forKey:@"thumb"];
            if(manuf.weight)
                [tempManufacturerDictionary setObject:manuf.weight forKey:@"weight"];
            if(manuf.add_to_favorites)
                [tempManufacturerDictionary setObject:manuf.add_to_favorites forKey:@"add_to_favorites"];
            if(manuf.share_listing)
                [tempManufacturerDictionary setObject:manuf.share_listing forKey:@"share_listing"];
            if(manuf.fixed_order)
                [tempManufacturerDictionary setObject:manuf.fixed_order forKey:@"fixed_order"];
            if(manuf.show_call)
                [tempManufacturerDictionary setObject:manuf.show_call forKey:@"show_call"];
            if(manuf.show_web)
                [tempManufacturerDictionary setObject:manuf.show_web forKey:@"show_web"];
            if(manuf.website_text)
                [tempManufacturerDictionary setObject:manuf.website_text forKey:@"website_text"];
            if(manuf.email_address)
                [tempManufacturerDictionary setObject:manuf.email_address forKey:@"email_address"];
            if(manuf.show_email)
                [tempManufacturerDictionary setObject:manuf.show_email forKey:@"show_email"];
            if(manuf.email_text)
                [tempManufacturerDictionary setObject:manuf.email_text forKey:@"email_text"];
            if(manuf.disp_address)
                [tempManufacturerDictionary setObject:manuf.disp_address forKey:@"disp_address"];
            
            // }
            //  @catch (NSException *exception) {
            
            // }
        }
        
        
        
        CLLocation *plLocation= [[CLLocation alloc] initWithLatitude:[manuf.latitude doubleValue] longitude:[manuf.longitude doubleValue]];
        CLLocationDistance meters = [location_updated distanceFromLocation:plLocation];
        float actdist;
        actdist = meters * 0.000621371;
        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
        [nf setMaximumFractionDigits:2];
        NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
        [tempManufacturerDictionary setObject:trimmed forKey:@"dist_offline"];
        
        [array9 addObject:tempManufacturerDictionary];
        
        
        
    }
    
    
    
    NSSortDescriptor *descriptor9 = [[NSSortDescriptor alloc] initWithKey:@"dist_offline"  ascending:YES];
    NSArray *arrtemp39 = [array9 sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor9,nil]];
    // NSArray *arrVals = [arrtemp3 copy];
    array9=[NSMutableArray arrayWithArray:arrtemp39];
    
    // array=[NSMutableArray arrayWithArray:array1];
    [array addObjectsFromArray:array9];
    
    //////////////
    
    
    /////////////cat10
    /////////////////////////////////////////
    
    NSError *error10;
    PDAppDelegate *appDelegate10 = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context10 = [appDelegate10 managedObjectContext];
    NSFetchRequest *fetchRequest10 = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity10 = [NSEntityDescription
                                     entityForName:@"Businesses" inManagedObjectContext:context10];
    fetchRequest10.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest10 setEntity:entity10];
    
    // int k;
    
    
    
    
    {
        NSPredicate * parentIdPredicate10 = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"cat_id10==%@",[dictDetails objectForKey:@"Id"]]];
        
        [fetchRequest10 setPredicate:parentIdPredicate10];
    }
    
    
    
    NSArray *fetchedObjects10 = [context10 executeFetchRequest:fetchRequest10 error:&error10];
    
    
    
    
    
    
    
    NSLog( @"error %@",error10);
    
    
    NSMutableArray*   array10  = [NSMutableArray array];
    
    for (Businesses *manuf in fetchedObjects10) {
        NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
        
        
        
        {
            
            // @try
            // {
            
            if(manuf.busId)
                [tempManufacturerDictionary setObject:manuf.busId forKey:@"Id"];
            if(manuf.image)
                [tempManufacturerDictionary setObject:manuf.image forKey:@"image"];
            if(manuf.name)
                [tempManufacturerDictionary setObject:manuf.name forKey:@"name"];
            if(manuf.cat_id)
                [tempManufacturerDictionary setObject:manuf.cat_id forKey:@"cat_id"];
            if(manuf.cat_id2)
                [tempManufacturerDictionary setObject:manuf.cat_id2 forKey:@"cat_id2"];
            if(manuf.cat_id3)
                [tempManufacturerDictionary setObject:manuf.cat_id3 forKey:@"cat_id3"];
            if(manuf.cat_id4)
                [tempManufacturerDictionary setObject:manuf.cat_id4 forKey:@"cat_id4"];
            if(manuf.cat_id5)
                [tempManufacturerDictionary setObject:manuf.cat_id5 forKey:@"cat_id5"];
            if(manuf.cat_id6)
                [tempManufacturerDictionary setObject:manuf.cat_id6 forKey:@"cat_id6"];
            if(manuf.cat_id7)
                [tempManufacturerDictionary setObject:manuf.cat_id7 forKey:@"cat_id7"];
            if(manuf.cat_id8)
                [tempManufacturerDictionary setObject:manuf.cat_id8 forKey:@"cat_id8"];
            if(manuf.cat_id9)
                [tempManufacturerDictionary setObject:manuf.cat_id9 forKey:@"cat_id9"];
            if(manuf.cat_id10)
                [tempManufacturerDictionary setObject:manuf.cat_id10 forKey:@"cat_id10"];
            if(manuf.address)
                [tempManufacturerDictionary setObject:manuf.address forKey:@"address"];
            if(manuf.preview)
                [tempManufacturerDictionary setObject:manuf.preview forKey:@"preview"];
            if(manuf.details)
                [tempManufacturerDictionary setObject:manuf.details forKey:@"details"];
            if(manuf.phone)
                [tempManufacturerDictionary setObject:manuf.phone forKey:@"phone"];
            if(manuf.website)
                [tempManufacturerDictionary setObject:manuf.website forKey:@"website"];
            if(manuf.latitude)
                [tempManufacturerDictionary setObject:manuf.latitude forKey:@"latitude"];
            if(manuf.longitude)
                [tempManufacturerDictionary setObject:manuf.longitude forKey:@"longitude"];
            if(manuf.featured)
                [tempManufacturerDictionary setObject:manuf.featured forKey:@"featured"];
            if(manuf.star_rating)
                [tempManufacturerDictionary setObject:manuf.star_rating forKey:@"star_rating"];
            if(manuf.type)
                [tempManufacturerDictionary setObject:manuf.type forKey:@"type"];
            if(manuf.thumb)
                [tempManufacturerDictionary setObject:manuf.thumb forKey:@"thumb"];
            if(manuf.weight)
                [tempManufacturerDictionary setObject:manuf.weight forKey:@"weight"];
            if(manuf.add_to_favorites)
                [tempManufacturerDictionary setObject:manuf.add_to_favorites forKey:@"add_to_favorites"];
            if(manuf.share_listing)
                [tempManufacturerDictionary setObject:manuf.share_listing forKey:@"share_listing"];
            if(manuf.fixed_order)
                [tempManufacturerDictionary setObject:manuf.fixed_order forKey:@"fixed_order"];
            if(manuf.show_call)
                [tempManufacturerDictionary setObject:manuf.show_call forKey:@"show_call"];
            if(manuf.show_web)
                [tempManufacturerDictionary setObject:manuf.show_web forKey:@"show_web"];
            if(manuf.website_text)
                [tempManufacturerDictionary setObject:manuf.website_text forKey:@"website_text"];
            if(manuf.email_address)
                [tempManufacturerDictionary setObject:manuf.email_address forKey:@"email_address"];
            if(manuf.show_email)
                [tempManufacturerDictionary setObject:manuf.show_email forKey:@"show_email"];
            if(manuf.email_text)
                [tempManufacturerDictionary setObject:manuf.email_text forKey:@"email_text"];
            if(manuf.disp_address)
                [tempManufacturerDictionary setObject:manuf.disp_address forKey:@"disp_address"];
            
            // }
            //  @catch (NSException *exception) {
            
            // }
        }
        
        
        
        CLLocation *plLocation= [[CLLocation alloc] initWithLatitude:[manuf.latitude doubleValue] longitude:[manuf.longitude doubleValue]];
        CLLocationDistance meters = [location_updated distanceFromLocation:plLocation];
        float actdist;
        actdist = meters * 0.000621371;
        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
        [nf setMaximumFractionDigits:2];
        NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
        [tempManufacturerDictionary setObject:trimmed forKey:@"dist_offline"];
        
        [array10 addObject:tempManufacturerDictionary];
        
        
        
    }
    
    
    
    NSSortDescriptor *descriptor10 = [[NSSortDescriptor alloc] initWithKey:@"dist_offline"  ascending:YES];
    NSArray *arrtemp310 = [array10 sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor10,nil]];
    // NSArray *arrVals = [arrtemp3 copy];
    array10=[NSMutableArray arrayWithArray:arrtemp310];
    
    // array=[NSMutableArray arrayWithArray:array1];
    [array addObjectsFromArray:array10];
    
    //////////////

}

-(void)coreDataCodeRead_bus{
    
    NSError *error;
    PDAppDelegate *appDelegate = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Businesses" inManagedObjectContext:context];
  
    [fetchRequest setEntity:entity];
   
    int i;
    
    if(_isSomethingEnabled &&!_isfromVC)
        
    {
     
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        
   
        
        NSMutableArray*   array  = [NSMutableArray array];
        
        NSLog(@"fetchedObjects count :%lu",(unsigned long)[fetchedObjects count]);
        
        for (Businesses *manuf in fetchedObjects) {
            NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
            
            //if(![manuf.busId isEqualToString:@"10860"])
            {
                
                {
                    
                    [tempManufacturerDictionary setObject:manuf.busId forKey:@"Id"];
                    if(manuf.image)
                        [tempManufacturerDictionary setObject:manuf.image forKey:@"image"];
                    if(manuf.name)
                        [tempManufacturerDictionary setObject:manuf.name forKey:@"name"];
                    
                    [tempManufacturerDictionary setObject:manuf.cat_id forKey:@"cat_id"];
                    
                    [tempManufacturerDictionary setObject:manuf.cat_id2 forKey:@"cat_id2"];
                    [tempManufacturerDictionary setObject:manuf.cat_id3 forKey:@"cat_id3"];
                    [tempManufacturerDictionary setObject:manuf.cat_id4 forKey:@"cat_id4"];
                    
                    [tempManufacturerDictionary setObject:manuf.cat_id5 forKey:@"cat_id5"];
                    [tempManufacturerDictionary setObject:manuf.cat_id6 forKey:@"cat_id6"];
                    [tempManufacturerDictionary setObject:manuf.cat_id7 forKey:@"cat_id7"];
                    [tempManufacturerDictionary setObject:manuf.cat_id8 forKey:@"cat_id8"];
                    [tempManufacturerDictionary setObject:manuf.cat_id9 forKey:@"cat_id9"];
                    [tempManufacturerDictionary setObject:manuf.cat_id10 forKey:@"cat_id10"];
                    
                    [tempManufacturerDictionary setObject:manuf.address forKey:@"address"];
                    if(manuf.preview)
                        [tempManufacturerDictionary setObject:manuf.preview forKey:@"preview"];
                    
                    if(manuf.details)
                        [tempManufacturerDictionary setObject:manuf.details forKey:@"details"];
                    if(manuf.phone)
                        [tempManufacturerDictionary setObject:manuf.phone forKey:@"phone"];
                    [tempManufacturerDictionary setObject:manuf.website forKey:@"website"];
                    [tempManufacturerDictionary setObject:manuf.latitude forKey:@"latitude"];
                    [tempManufacturerDictionary setObject:manuf.longitude forKey:@"longitude"];
                    [tempManufacturerDictionary setObject:manuf.featured forKey:@"featured"];
                    [tempManufacturerDictionary setObject:manuf.star_rating forKey:@"star_rating"];
                    [tempManufacturerDictionary setObject:manuf.type forKey:@"type"];
                    [tempManufacturerDictionary setObject:manuf.thumb forKey:@"thumb"];
                    [tempManufacturerDictionary setObject:manuf.weight forKey:@"weight"];
                    [tempManufacturerDictionary setObject:manuf.add_to_favorites forKey:@"add_to_favorites"];
                    [tempManufacturerDictionary setObject:manuf.share_listing forKey:@"share_listing"];
                    [tempManufacturerDictionary setObject:manuf.fixed_order forKey:@"fixed_order"];
                    [tempManufacturerDictionary setObject:manuf.show_call forKey:@"show_call"];
                    [tempManufacturerDictionary setObject:manuf.show_web forKey:@"show_web"];
                    [tempManufacturerDictionary setObject:manuf.website_text forKey:@"website_text"];
                    
                    [tempManufacturerDictionary setObject:manuf.email_address forKey:@"email_address"];
                    [tempManufacturerDictionary setObject:manuf.show_email forKey:@"show_email"];
                    [tempManufacturerDictionary setObject:manuf.email_text forKey:@"email_text"];
                    [tempManufacturerDictionary setObject:manuf.disp_address forKey:@"disp_address"];
                    
                    [tempManufacturerDictionary setObject:manuf.guide_id forKey:@"guide_id"];
                    
                    [tempManufacturerDictionary setObject:manuf.guide_name forKey:@"guide_name"];
                    
                    
                    
                }
                
                
              
                CLLocation *plLocation= [[CLLocation alloc] initWithLatitude:[manuf.latitude doubleValue] longitude:[manuf.longitude doubleValue]];
                CLLocationDistance meters = [location_updated distanceFromLocation:plLocation];
                float actdist;
                actdist = meters * 0.000621371;
                NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
                [nf setMaximumFractionDigits:2];
                NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
                [tempManufacturerDictionary setObject:trimmed forKey:@"dist_offline"];
                
                [array addObject:tempManufacturerDictionary];
                
                
                
                NSLog(@"dictDetails id : %@",[dictDetails objectForKey:@"Id"]);
                NSLog(@"manuf.cat_id :%@",manuf.cat_id);
                NSLog(@"manuf.cat_id2 :%@",manuf.cat_id2);
                
             
                
            }
            
        }
        
        
        
        
        NSMutableArray *mmu = [[NSMutableArray alloc]initWithArray:array];
        arraySmpl =mmu;
        
        NSLog(@"For Search DAta");
        
        
        if(_isSomethingEnabled)
        {
            NSString *stt = [[NSUserDefaults standardUserDefaults]objectForKey:@"searchBar.text"];
            
            NSString *lower = [stt lowercaseString];
            //NSString *higher = [stt uppercaseString];
            
            //  {
            // NSString* filterr = @"%K CONTAINS %@";
            
            NSPredicate* predicate2 =  [NSPredicate predicateWithFormat:@"(%K CONTAINS %@) OR (%K CONTAINS %@) OR (%K CONTAINS %@) OR (%K CONTAINS %@) OR (%K CONTAINS %@) OR (%K CONTAINS %@)",@"address",stt,@"address",lower,@"name",stt,@"name",lower,@"details",stt,@"details",lower];
            
            NSArray* filteredData2 = [arraySmpl filteredArrayUsingPredicate:predicate2];
            
            NSLog(@"filteredData2 count :%lu",(unsigned long)[filteredData2 count]);
            
            //                NSPredicate* predicate = [NSPredicate predicateWithFormat:filterr, @"address", lower];
            //                NSArray* filteredData = [arraySmpl filteredArrayUsingPredicate:predicate];
            //
            //                NSLog(@"filteredData count :%lu",(unsigned long)[filteredData count]);
            
            //  }
            
            /* NSString* filter = @"%K CONTAINS %@";
             NSPredicate* predicate = [NSPredicate predicateWithFormat:filter, @"details", lower];
             NSArray* filteredData = [arraySmpl filteredArrayUsingPredicate:predicate];
             
             NSLog(@"filteredData :%@",filteredData);*/
            
            
            if ([array containsObject:stt])
            {
                NSUInteger indexOfTheObject = [array indexOfObject:stt];
                NSLog(@"%@", [array objectAtIndex:indexOfTheObject]);
                
                // NSMutableArray *mmu = [[NSMutableArray alloc]initWithArray:array];
                //  arraySmpl =mmu;
                
            }
            
            else
                
            {
                
                
            }
            
            NSMutableArray *mmu = [[NSMutableArray alloc]initWithArray:filteredData2];
            arraySmpl =mmu;
            
            // if([arraySmpl count]>0)
            {
                
                
                NSString *keySer =[[NSUserDefaults standardUserDefaults]objectForKey:@"searchBar.text"];
                
                int i=0;
                NSMutableDictionary *strA=[NSMutableDictionary dictionary];
                
                NSMutableArray *arrMain = [[NSUserDefaults standardUserDefaults]objectForKey:@"guidesName"];
                for(NSString *str in arrMain)
                {
                    BOOL isTheObjectThere;
                    isTheObjectThere=false;
                    NSArray *myArray = [[arrMain objectAtIndex:i] valueForKey:@"keywords"];
                    
                    if(![myArray isEqual:@""])
                    {
                        
                        for(NSString *strArr in myArray)
                        {
                            
                            NSString *strArr2 =   [strArr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                            
                            
                            
                            NSLog(@"strArr :%@ keySer :%@",strArr2,keySer);
                            
                            if([strArr2 caseInsensitiveCompare:keySer] == NSOrderedSame)
                            {
                                NSLog(@"Matches");
                                isTheObjectThere=true;
                                break;
                            }
                        }
                        //isTheObjectThere = [myArray containsObject:keySer];
                    }
                    
                    
                    NSString *strAppname = [[arrMain objectAtIndex:i] valueForKey:@"app_name"];
                    if([strAppname isEqualToString:@"Mount Shasta"])
                        NSLog(@"strAppname :%@",strAppname);
                    
                    
                    if([keySer caseInsensitiveCompare:strAppname]== NSOrderedSame||isTheObjectThere)//ignores casesensitive
                    {
                        strA =[arrMain objectAtIndex:i];
                        NSLog(@"strA :%@",strA);
                        
                        
                        break;
                    }
                    i++;
                }
                
                if([strA count]>1)
                {
                    
                    if(!_isfromVC)
                    {
                        
                        appName=YES;
                        
                        
                        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                        dict= [strA mutableCopy];
                        
                        [dict setObject: [dict objectForKey: @"app_name"] forKey: @"name"];
                        [dict removeObjectForKey: @"app_name"];
                        [dict removeObjectForKey: @"splash_image"];
                        
                        NSMutableDictionary *dict2 = [NSMutableDictionary dictionary];
                        dict2= [dict mutableCopy];
                        
                        
                        [dict setObject:dict2 forKey: @"test"];
                        
                        // ... and later ...
                        
                        id something = [dict objectForKey:@"Some Key"];
                        NSLog(@"something :%@",something);
                        
                        
                        
                        NSMutableArray *ar = [[NSMutableArray alloc]init];
                        ar=[dict objectForKey:@"test"];
                        
                        [arraySmpl insertObject:ar atIndex:0];
                        
                        
                        if(ar)
                        {
                            appName =YES;
                        }
                        //  arraySmpl = [NSMutableArray arrayWithArray:ar];
                        fixedArrayCount = [arraySmpl count];
                        
                        [[NSUserDefaults standardUserDefaults]setObject:ar forKey:@"loadingsplashSearch"];
                        //NSLog(@"arraySmpl search :%@",arraySmpl);
                    }
                }
                
            }
            
            if([arraySmpl count]>0)
            {
                
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                                message:[NSString stringWithFormat:@"No Results Found"]
                                                               delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                alert.tag=10057;
                [alert show];
                
            }
        }
        
        return;
    }
    
    if(_isSomethingEnabled &&_isfromVC)
    {
        //        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"catid" ascending:YES];
        
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        
        //        NSString* filter = @"%K CONTAINS %@";
        //        NSPredicate* predicate = [NSPredicate predicateWithFormat:filter, @"SELF", @"a"];
        //        NSArray* filteredData = [fetchedObjects filteredArrayUsingPredicate:predicate];
        //
        
        
        
        NSMutableArray*   array  = [NSMutableArray array];
        
        NSLog(@"fetchedObjects count :%lu",(unsigned long)[fetchedObjects count]);
        
        for (Businesses *manuf in fetchedObjects) {
            NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
            
            //if(![manuf.busId isEqualToString:@"10860"])
            {
                
                {
                    
                    [tempManufacturerDictionary setObject:manuf.busId forKey:@"Id"];
                    if(manuf.image)
                        [tempManufacturerDictionary setObject:manuf.image forKey:@"image"];
                    if(manuf.name)
                        [tempManufacturerDictionary setObject:manuf.name forKey:@"name"];
                    
                    [tempManufacturerDictionary setObject:manuf.cat_id forKey:@"cat_id"];
                    
                    [tempManufacturerDictionary setObject:manuf.cat_id2 forKey:@"cat_id2"];
                    [tempManufacturerDictionary setObject:manuf.cat_id3 forKey:@"cat_id3"];
                    [tempManufacturerDictionary setObject:manuf.cat_id4 forKey:@"cat_id4"];
                    
                    [tempManufacturerDictionary setObject:manuf.cat_id5 forKey:@"cat_id5"];
                    [tempManufacturerDictionary setObject:manuf.cat_id6 forKey:@"cat_id6"];
                    [tempManufacturerDictionary setObject:manuf.cat_id7 forKey:@"cat_id7"];
                    [tempManufacturerDictionary setObject:manuf.cat_id8 forKey:@"cat_id8"];
                    [tempManufacturerDictionary setObject:manuf.cat_id9 forKey:@"cat_id9"];
                    [tempManufacturerDictionary setObject:manuf.cat_id10 forKey:@"cat_id10"];
                    
                    [tempManufacturerDictionary setObject:manuf.address forKey:@"address"];
                    if(manuf.preview)
                        [tempManufacturerDictionary setObject:manuf.preview forKey:@"preview"];
                    
                    if(manuf.details)
                        [tempManufacturerDictionary setObject:manuf.details forKey:@"details"];
                    if(manuf.phone)
                        [tempManufacturerDictionary setObject:manuf.phone forKey:@"phone"];
                    [tempManufacturerDictionary setObject:manuf.website forKey:@"website"];
                    [tempManufacturerDictionary setObject:manuf.latitude forKey:@"latitude"];
                    [tempManufacturerDictionary setObject:manuf.longitude forKey:@"longitude"];
                    [tempManufacturerDictionary setObject:manuf.featured forKey:@"featured"];
                    [tempManufacturerDictionary setObject:manuf.star_rating forKey:@"star_rating"];
                    [tempManufacturerDictionary setObject:manuf.type forKey:@"type"];
                    [tempManufacturerDictionary setObject:manuf.thumb forKey:@"thumb"];
                    [tempManufacturerDictionary setObject:manuf.weight forKey:@"weight"];
                    [tempManufacturerDictionary setObject:manuf.add_to_favorites forKey:@"add_to_favorites"];
                    [tempManufacturerDictionary setObject:manuf.share_listing forKey:@"share_listing"];
                    [tempManufacturerDictionary setObject:manuf.fixed_order forKey:@"fixed_order"];
                    [tempManufacturerDictionary setObject:manuf.show_call forKey:@"show_call"];
                    [tempManufacturerDictionary setObject:manuf.show_web forKey:@"show_web"];
                    [tempManufacturerDictionary setObject:manuf.website_text forKey:@"website_text"];
                    
                    [tempManufacturerDictionary setObject:manuf.email_address forKey:@"email_address"];
                    [tempManufacturerDictionary setObject:manuf.show_email forKey:@"show_email"];
                    [tempManufacturerDictionary setObject:manuf.email_text forKey:@"email_text"];
                    [tempManufacturerDictionary setObject:manuf.disp_address forKey:@"disp_address"];
                    
                    [tempManufacturerDictionary setObject:manuf.guide_id forKey:@"guide_id"];
                    
                    [tempManufacturerDictionary setObject:manuf.guide_name forKey:@"guide_name"];
                    
                    
                    
                }
                
                
                
                CLLocation *plLocation= [[CLLocation alloc] initWithLatitude:[manuf.latitude doubleValue] longitude:[manuf.longitude doubleValue]];
                CLLocationDistance meters = [location_updated distanceFromLocation:plLocation];
                float actdist;
                actdist = meters * 0.000621371;
                NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
                [nf setMaximumFractionDigits:2];
                NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
                [tempManufacturerDictionary setObject:trimmed forKey:@"dist_offline"];
                
                NSString *cAPiD;
                cAPiD=[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"];
                
                if([manuf.guide_id isEqualToString:cAPiD])
                    [array addObject:tempManufacturerDictionary];
                
                
                
                NSLog(@"dictDetails id : %@",[dictDetails objectForKey:@"Id"]);
                NSLog(@"manuf.cat_id :%@",manuf.cat_id);
                NSLog(@"manuf.cat_id2 :%@",manuf.cat_id2);
                
                
            }
            
        }
        
        
        
        
        NSMutableArray *mmu = [[NSMutableArray alloc]initWithArray:array];
        arraySmpl =mmu;
        
        NSLog(@"For Search DAta");
        
        
        if(_isSomethingEnabled)
        {
            NSString *stt = [[NSUserDefaults standardUserDefaults]objectForKey:@"searchBar.text"];
            
            NSString *lower = [stt lowercaseString];
           
            
            NSPredicate* predicate2 =  [NSPredicate predicateWithFormat:@"(%K CONTAINS %@) OR (%K CONTAINS %@) OR (%K CONTAINS %@) OR (%K CONTAINS %@) OR (%K CONTAINS %@) OR (%K CONTAINS %@)",@"address",stt,@"address",lower,@"name",stt,@"name",lower,@"details",stt,@"details",lower];
            
            NSArray* filteredData2 = [arraySmpl filteredArrayUsingPredicate:predicate2];
            
            NSLog(@"filteredData2 count :%lu",(unsigned long)[filteredData2 count]);
            
           
            
            if ([array containsObject:stt])
            {
                NSUInteger indexOfTheObject = [array indexOfObject:stt];
                NSLog(@"%@", [array objectAtIndex:indexOfTheObject]);
                
               
                
            }
            
            else
                
            {
                
                
            }
            
            NSMutableArray *mmu = [[NSMutableArray alloc]initWithArray:filteredData2];
            arraySmpl =mmu;
            
           
            {
                
                
                NSString *keySer =[[NSUserDefaults standardUserDefaults]objectForKey:@"searchBar.text"];
                
                int i=0;
                NSMutableDictionary *strA=[NSMutableDictionary dictionary];
                
                NSMutableArray *arrMain = [[NSUserDefaults standardUserDefaults]objectForKey:@"guidesName"];
                for(NSString *str in arrMain)
                {
                    BOOL isTheObjectThere;
                    isTheObjectThere=false;
                    NSArray *myArray = [[arrMain objectAtIndex:i] valueForKey:@"keywords"];
                    
                    if(![myArray isEqual:@""])
                    {
                        
                        for(NSString *strArr in myArray)
                        {
                            
                            NSString *strArr2 =   [strArr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                            
                            
                            
                            NSLog(@"strArr :%@ keySer :%@",strArr2,keySer);
                            
                            if([strArr2 caseInsensitiveCompare:keySer] == NSOrderedSame)
                            {
                                NSLog(@"Matches");
                                isTheObjectThere=true;
                                break;
                            }
                        }
                        //isTheObjectThere = [myArray containsObject:keySer];
                    }
                    
                    
                    NSString *strAppname = [[arrMain objectAtIndex:i] valueForKey:@"app_name"];
                    if([strAppname isEqualToString:@"Mount Shasta"])
                        NSLog(@"strAppname :%@",strAppname);
                    
                    
                    if([keySer caseInsensitiveCompare:strAppname]== NSOrderedSame||isTheObjectThere)//ignores casesensitive
                    {
                        strA =[arrMain objectAtIndex:i];
                        NSLog(@"strA :%@",strA);
                        
                        
                        break;
                    }
                    i++;
                }
                
                if([strA count]>1)
                {
                    
                    if(!_isfromVC)
                    {
                        
                        appName=YES;
                        
                        
                        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                        dict= [strA mutableCopy];
                        
                        [dict setObject: [dict objectForKey: @"app_name"] forKey: @"name"];
                        [dict removeObjectForKey: @"app_name"];
                        [dict removeObjectForKey: @"splash_image"];
                        
                        NSMutableDictionary *dict2 = [NSMutableDictionary dictionary];
                        dict2= [dict mutableCopy];
                        
                        
                        [dict setObject:dict2 forKey: @"test"];
                        
                        // ... and later ...
                        
                        id something = [dict objectForKey:@"Some Key"];
                        NSLog(@"something :%@",something);
                        
                        
                        
                        NSMutableArray *ar = [[NSMutableArray alloc]init];
                        ar=[dict objectForKey:@"test"];
                        
                        [arraySmpl insertObject:ar atIndex:0];
                        
                        
                        if(ar)
                        {
                            appName =YES;
                        }
                        //  arraySmpl = [NSMutableArray arrayWithArray:ar];
                        fixedArrayCount = [arraySmpl count];
                        
                        [[NSUserDefaults standardUserDefaults]setObject:ar forKey:@"loadingsplashSearch"];
                        //NSLog(@"arraySmpl search :%@",arraySmpl);
                    }
                }
                
            }
            
            if([arraySmpl count]>0)
            {
                
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                                message:[NSString stringWithFormat:@"No Results Found"]
                                                               delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                alert.tag=10057;
                [alert show];
                
            }
        }
        
        return;
    }
    
    
    if(!([[dictDetails objectForKey:@"type"] isEqualToString:@"event"]))
    {
        NSPredicate * parentIdPredicate = [NSPredicate predicateWithFormat:@"cat_id==%@",[dictDetails objectForKey:@"Id"]];//Id
        
        
        
        [fetchRequest setPredicate:parentIdPredicate];
        
        NSLog(@"fetchRequest :%@",fetchRequest);
    }
    
    
    //        }
    
    //fetchRequest.sortDescriptors = @[descriptor];
    //[fetchRequest setPredicate:compountPredicate];
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
    
    
    
    
    
    NSLog( @"error %@",error);
    
    
    NSMutableArray*   array  = [NSMutableArray array];
    
    NSLog(@"fetchedObjects count :%lu",(unsigned long)[fetchedObjects count]);
    
    for (Businesses *manuf in fetchedObjects) {
        NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
        
        
        {
            
            
            {
                
                [tempManufacturerDictionary setObject:manuf.busId forKey:@"Id"];
                if(manuf.image)
                    [tempManufacturerDictionary setObject:manuf.image forKey:@"image"];
                if(manuf.name)
                    [tempManufacturerDictionary setObject:manuf.name forKey:@"name"];
                if(manuf.cat_id)
                    [tempManufacturerDictionary setObject:manuf.cat_id forKey:@"cat_id"];
                
                [tempManufacturerDictionary setObject:manuf.cat_id2 forKey:@"cat_id2"];
                [tempManufacturerDictionary setObject:manuf.cat_id3 forKey:@"cat_id3"];
                [tempManufacturerDictionary setObject:manuf.cat_id4 forKey:@"cat_id4"];
                
                [tempManufacturerDictionary setObject:manuf.cat_id5 forKey:@"cat_id5"];
                [tempManufacturerDictionary setObject:manuf.cat_id6 forKey:@"cat_id6"];
                [tempManufacturerDictionary setObject:manuf.cat_id7 forKey:@"cat_id7"];
                [tempManufacturerDictionary setObject:manuf.cat_id8 forKey:@"cat_id8"];
                [tempManufacturerDictionary setObject:manuf.cat_id9 forKey:@"cat_id9"];
                [tempManufacturerDictionary setObject:manuf.cat_id10 forKey:@"cat_id10"];
                
                [tempManufacturerDictionary setObject:manuf.address forKey:@"address"];
                if(manuf.preview)
                    [tempManufacturerDictionary setObject:manuf.preview forKey:@"preview"];
                
                if(manuf.details)
                    [tempManufacturerDictionary setObject:manuf.details forKey:@"details"];
                if(manuf.phone)
                    [tempManufacturerDictionary setObject:manuf.phone forKey:@"phone"];
                [tempManufacturerDictionary setObject:manuf.website forKey:@"website"];
                [tempManufacturerDictionary setObject:manuf.latitude forKey:@"latitude"];
                [tempManufacturerDictionary setObject:manuf.longitude forKey:@"longitude"];
                [tempManufacturerDictionary setObject:manuf.featured forKey:@"featured"];
                [tempManufacturerDictionary setObject:manuf.star_rating forKey:@"star_rating"];
                [tempManufacturerDictionary setObject:manuf.type forKey:@"type"];
                [tempManufacturerDictionary setObject:manuf.thumb forKey:@"thumb"];
                [tempManufacturerDictionary setObject:manuf.weight forKey:@"weight"];
                [tempManufacturerDictionary setObject:manuf.add_to_favorites forKey:@"add_to_favorites"];
                [tempManufacturerDictionary setObject:manuf.share_listing forKey:@"share_listing"];
                [tempManufacturerDictionary setObject:manuf.fixed_order forKey:@"fixed_order"];
                [tempManufacturerDictionary setObject:manuf.show_call forKey:@"show_call"];
                [tempManufacturerDictionary setObject:manuf.show_web forKey:@"show_web"];
                [tempManufacturerDictionary setObject:manuf.website_text forKey:@"website_text"];
                
                [tempManufacturerDictionary setObject:manuf.email_address forKey:@"email_address"];
                [tempManufacturerDictionary setObject:manuf.show_email forKey:@"show_email"];
                [tempManufacturerDictionary setObject:manuf.email_text forKey:@"email_text"];
                [tempManufacturerDictionary setObject:manuf.disp_address forKey:@"disp_address"];
                
            }
            
          
            
            CLLocation *plLocation= [[CLLocation alloc] initWithLatitude:[manuf.latitude doubleValue] longitude:[manuf.longitude doubleValue]];
            CLLocationDistance meters = [location_updated distanceFromLocation:plLocation];
            float actdist;
            actdist = meters * 0.000621371;
            NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
            [nf setMaximumFractionDigits:2];
            NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
            [tempManufacturerDictionary setObject:trimmed forKey:@"dist_offline"];
            
            [array addObject:tempManufacturerDictionary];
            
            
            
            NSLog(@"dictDetails id : %@",[dictDetails objectForKey:@"Id"]);
            NSLog(@"manuf.cat_id :%@",manuf.cat_id);
            NSLog(@"manuf.cat_id2 :%@",manuf.cat_id2);
          
            
        }
        
    }
    
    if(_isSomethingEnabled)
        
    {
        
        
        
        NSMutableArray *mmu = [[NSMutableArray alloc]initWithArray:array];
        arraySmpl =mmu;
        
        NSLog(@"For Search DAta");
        
        return;
    }
    
    
    
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"dist_offline"  ascending:YES];
    NSArray *arrtemp3 = [array sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
   
    array=[NSMutableArray arrayWithArray:arrtemp3];
    
   
    
    
    {
    
    NSError *error3;
    PDAppDelegate *appDelegate3 = (PDAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context3 = [appDelegate3 managedObjectContext];
    NSFetchRequest *fetchRequest3 = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity3 = [NSEntityDescription
                                    entityForName:@"Businesses" inManagedObjectContext:context3];
    fetchRequest3.predicate = [NSPredicate predicateWithFormat:@"appid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    [fetchRequest3 setEntity:entity3];
    
    int l;
    
    
    
    
    {
        
        
       
        
        NSPredicate * parentIdPredicate3 = [NSPredicate predicateWithFormat:@"(%K CONTAINS %@) OR (%K CONTAINS %@) OR (%K CONTAINS %@) OR (%K CONTAINS %@) OR (%K CONTAINS %@) OR (%K CONTAINS %@) OR (%K CONTAINS %@) OR (%K CONTAINS %@) OR (%K CONTAINS %@)",@"cat_id2",[dictDetails objectForKey:@"Id"],@"cat_id3",[dictDetails objectForKey:@"Id"],@"cat_id4",[dictDetails objectForKey:@"Id"],@"cat_id5",[dictDetails objectForKey:@"Id"],@"cat_id6",[dictDetails objectForKey:@"Id"],@"cat_id7",[dictDetails objectForKey:@"Id"],@"cat_id8",[dictDetails objectForKey:@"Id"],@"cat_id9",[dictDetails objectForKey:@"Id"],@"cat_id10",[dictDetails objectForKey:@"Id"]];
        
        [fetchRequest3 setPredicate:parentIdPredicate3];
    }
    
    
    
    NSArray *fetchedObjects3 = [context3 executeFetchRequest:fetchRequest3 error:&error3];
    
    
    
    
    
    
    
    NSLog( @"error %@",error3);
    
    
    NSMutableArray*   array3  = [NSMutableArray array];
    
    for (Businesses *manuf in fetchedObjects3) {
        NSMutableDictionary *tempManufacturerDictionary = [NSMutableDictionary dictionary];
        
        
        
        {
            
           
            
            if(manuf.busId)
                [tempManufacturerDictionary setObject:manuf.busId forKey:@"Id"];
            if(manuf.image)
                [tempManufacturerDictionary setObject:manuf.image forKey:@"image"];
            if(manuf.name)
                [tempManufacturerDictionary setObject:manuf.name forKey:@"name"];
            if(manuf.cat_id)
                [tempManufacturerDictionary setObject:manuf.cat_id forKey:@"cat_id"];
            if(manuf.cat_id2)
                [tempManufacturerDictionary setObject:manuf.cat_id2 forKey:@"cat_id2"];
            if(manuf.cat_id3)
                [tempManufacturerDictionary setObject:manuf.cat_id3 forKey:@"cat_id3"];
            if(manuf.cat_id4)
                [tempManufacturerDictionary setObject:manuf.cat_id4 forKey:@"cat_id4"];
            if(manuf.cat_id5)
                [tempManufacturerDictionary setObject:manuf.cat_id5 forKey:@"cat_id5"];
            if(manuf.cat_id6)
                [tempManufacturerDictionary setObject:manuf.cat_id6 forKey:@"cat_id6"];
            if(manuf.cat_id7)
                [tempManufacturerDictionary setObject:manuf.cat_id7 forKey:@"cat_id7"];
            if(manuf.cat_id8)
                [tempManufacturerDictionary setObject:manuf.cat_id8 forKey:@"cat_id8"];
            if(manuf.cat_id9)
                [tempManufacturerDictionary setObject:manuf.cat_id9 forKey:@"cat_id9"];
            if(manuf.cat_id10)
                [tempManufacturerDictionary setObject:manuf.cat_id10 forKey:@"cat_id10"];
            if(manuf.address)
                [tempManufacturerDictionary setObject:manuf.address forKey:@"address"];
            if(manuf.preview)
                [tempManufacturerDictionary setObject:manuf.preview forKey:@"preview"];
            if(manuf.details)
                [tempManufacturerDictionary setObject:manuf.details forKey:@"details"];
            if(manuf.phone)
                [tempManufacturerDictionary setObject:manuf.phone forKey:@"phone"];
            if(manuf.website)
                [tempManufacturerDictionary setObject:manuf.website forKey:@"website"];
            if(manuf.latitude)
                [tempManufacturerDictionary setObject:manuf.latitude forKey:@"latitude"];
            if(manuf.longitude)
                [tempManufacturerDictionary setObject:manuf.longitude forKey:@"longitude"];
            if(manuf.featured)
                [tempManufacturerDictionary setObject:manuf.featured forKey:@"featured"];
            if(manuf.star_rating)
                [tempManufacturerDictionary setObject:manuf.star_rating forKey:@"star_rating"];
            if(manuf.type)
                [tempManufacturerDictionary setObject:manuf.type forKey:@"type"];
            if(manuf.thumb)
                [tempManufacturerDictionary setObject:manuf.thumb forKey:@"thumb"];
            if(manuf.weight)
                [tempManufacturerDictionary setObject:manuf.weight forKey:@"weight"];
            if(manuf.add_to_favorites)
                [tempManufacturerDictionary setObject:manuf.add_to_favorites forKey:@"add_to_favorites"];
            if(manuf.share_listing)
                [tempManufacturerDictionary setObject:manuf.share_listing forKey:@"share_listing"];
            if(manuf.fixed_order)
                [tempManufacturerDictionary setObject:manuf.fixed_order forKey:@"fixed_order"];
            if(manuf.show_call)
                [tempManufacturerDictionary setObject:manuf.show_call forKey:@"show_call"];
            if(manuf.show_web)
                [tempManufacturerDictionary setObject:manuf.show_web forKey:@"show_web"];
            if(manuf.website_text)
                [tempManufacturerDictionary setObject:manuf.website_text forKey:@"website_text"];
            if(manuf.email_address)
                [tempManufacturerDictionary setObject:manuf.email_address forKey:@"email_address"];
            if(manuf.show_email)
                [tempManufacturerDictionary setObject:manuf.show_email forKey:@"show_email"];
            if(manuf.email_text)
                [tempManufacturerDictionary setObject:manuf.email_text forKey:@"email_text"];
            if(manuf.disp_address)
                [tempManufacturerDictionary setObject:manuf.disp_address forKey:@"disp_address"];
            
            
        }
        
        
        
        CLLocation *plLocation= [[CLLocation alloc] initWithLatitude:[manuf.latitude doubleValue] longitude:[manuf.longitude doubleValue]];
        CLLocationDistance meters = [location_updated distanceFromLocation:plLocation];
        float actdist;
        actdist = meters * 0.000621371;
        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
        [nf setMaximumFractionDigits:2];
        NSString *trimmed = [nf stringFromNumber:[NSNumber numberWithFloat:actdist]];
        [tempManufacturerDictionary setObject:trimmed forKey:@"dist_offline"];
        
        [array3 addObject:tempManufacturerDictionary];
        
        
        
    }
    
    
    
    NSSortDescriptor *descriptor3 = [[NSSortDescriptor alloc] initWithKey:@"dist_offline"  ascending:YES];
    NSArray *arrtemp33 = [array3 sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor3,nil]];
    // NSArray *arrVals = [arrtemp3 copy];
    array3=[NSMutableArray arrayWithArray:arrtemp33];
    
    // array=[NSMutableArray arrayWithArray:array1];
    [array addObjectsFromArray:array3];
    
    
    }
    
    
    
    
    
   
    
    
    NSSortDescriptor *distanceSortDiscriptor = [NSSortDescriptor sortDescriptorWithKey:@"dist_offline" ascending:YES selector:@selector(localizedStandardCompare:)];
    
    [array sortUsingDescriptors:@[distanceSortDiscriptor]];
    NSArray *sortedArray = [NSArray arrayWithArray:array];
    
    NSLog(@"sortedArray :%@",sortedArray);
    
    
    NSArray *filteredArray = [[NSArray alloc]init];
    NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"featured contains[cd] %@",@"Yes"];
    filteredArray = [array filteredArrayUsingPredicate:bPredicate];
    NSLog(@"HERE :%@",filteredArray);
    
    NSMutableArray *mutta = [[NSMutableArray alloc]initWithArray:filteredArray];
    
    //[mutta addObjectsFromArray:array];
    
    NSLog(@"mutta :%@",mutta);
    
    //[array insertObject:mutta atIndex:0];
    
    NSMutableArray *arrayTodelete = [[NSMutableArray alloc]initWithArray:array];
    NSMutableArray *checkArray = [[NSMutableArray alloc]initWithArray:filteredArray];
    
    
    for(int i =0;i<[array count];i++)
    {
        for(int j=0;j<[checkArray count];j++)
        {
            if ([[[array objectAtIndex:i] valueForKey:@"Id"] isEqualToString:[[checkArray objectAtIndex:j] valueForKey:@"Id"]])
            {
                
                [arrayTodelete removeObjectAtIndex:i];
                
                NSLog(@"contains same element");
                
            }
        }
    }
    
    NSLog(@"arrayTodelete :%@",arrayTodelete);
    
    [mutta addObjectsFromArray:arrayTodelete];
    
    arraySmpl = [NSMutableArray arrayWithArray:mutta];
    
    
    
    
    fixedArrayCount = [arraySmpl count];
    [tblList reloadData];
    
    
 
}

-(CGFloat)convertMeterToMilesOrKilometer:(int)meterValue isKM:(BOOL)isKilometer{
    CGFloat convertedValue;
    if (isKilometer == TRUE) {
        convertedValue = meterValue / 1000;
    }else{
        convertedValue = meterValue / 1609.344;
    }
    
    return convertedValue;
}

-(void)testloadSplashSearch
{
   
    
    if([self offlineAvailable])
    {
        [self splashforDownloadedGuide];
    }
    else
    {
        splashSearch.contentMode=UIViewContentModeScaleToFill;
    
    NSString *imageCacheFolder=[NSString stringWithFormat:@"MyFolder%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    SDImageCache *imageCache =[SDImageCache sharedImageCache];
    imageCache = [imageCache initWithNamespace:imageCacheFolder];
    
    
    NSArray *arr= [[NSUserDefaults standardUserDefaults]objectForKey:@"guidesName"];
    
    
   NSMutableDictionary *sttt =[[NSUserDefaults standardUserDefaults]objectForKey:@"forspplashimageurlfromsearchtop"];//[arr objectAtIndex:indexe]
    
    
    
    NSString*str = [sttt objectForKey:@"splash_image"];

   
            {
                __block UIImage *img = nil;
                
               img = [[UIImage alloc] init];
                
                dispatch_async(dispatch_get_global_queue(0,0), ^{
                    
                    NSData * data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:str]];
                                     img = [UIImage imageWithData: data];
                                     
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         
                                         
                                         [loaderView removeFromSuperview];
                       
                        splashSearch.image = img;
                                         
                                         
                                         PDViewController *vc=[[PDViewController alloc]init];
                                         vc.fromOfficialGuideSearch=YES;
                                        
                                         int64_t delayInSeconds = 1.0;
                                         dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                                         dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                             
                                             [[self navigationController] pushViewController:vc animated:YES];
                                             
                                         });

                                     });
                                     });
                
                
            }
    }
    
}

-(void)splashforDownloadedGuide
{
    NSMutableDictionary *sttt =[[NSUserDefaults standardUserDefaults]objectForKey:@"forspplashimageurlfromsearchtop"];//[arr objectAtIndex:indexe]
    
    
    
    NSString*str = [sttt objectForKey:@"splash_image"];
    
    {
        if (![str isEqualToString:@"NA"])
        {
            
            NSString *appNameFolder = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"urlZip%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
            
            NSArray* foo = [appNameFolder componentsSeparatedByString:@".zip"];
            
            NSString* firstBit = [foo objectAtIndex:0];  NSString* secondBit = [foo objectAtIndex:1];
            
            
            
            
            NSArray* foo2 = [firstBit componentsSeparatedByString:@"zip/"];
            
            NSString* firstBit2 = [foo2 objectAtIndex:0]; NSString* secondBit2 = [foo2 objectAtIndex:1];
            
            NSLog(@"firstBit folder :%@",firstBit);
            NSLog(@"secondBit folder :%@",secondBit2);
            
            NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
            
            NSLog(@"str url :%@",str);
            
            NSString *filePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/splash",secondBit2]];
            
            NSArray* str3 = [str componentsSeparatedByString:@"splash/"];
            
            NSString* firstBit3 = [str3 objectAtIndex:0]; NSString* secondBit3 = [str3 objectAtIndex:1];
            
            NSLog(@"firstBit folder :%@",firstBit3);
            NSLog(@"secondBit folder :%@",secondBit3);
            
            NSString* Path = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",secondBit3]];
            
            UIImage* image = [UIImage imageWithContentsOfFile:Path];
            
           
            splashSearch.image = image;
            
            PDViewController *vc=[[PDViewController alloc]init];
            vc.fromOfficialGuideSearch=YES;
            
            
            int64_t delayInSeconds = 1.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                [[self navigationController] pushViewController:vc animated:YES];
                
            });

            
            
        }
        
        
        
    }
}
@end

