//
//  PDRelocationViewController.h
//  LocalsGuide
//
//  Created by swaroop on 19/03/14.
//  Copyright (c) 2014 iDeveloper. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDRelocationViewController : UIViewController <CLLocationManagerDelegate,GADBannerViewDelegate>
{
    GADBannerView *bannerView_;
    IBOutlet UIView *vwProcessing;
    IBOutlet UILabel *lblTitle;
}
@property (nonatomic, retain) CLLocationManager *locationManager;
-(IBAction)onBack:(id)sender;
@end
