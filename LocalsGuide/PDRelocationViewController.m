//
//  PDRelocationViewController.m
//  LocalsGuide
//
//  Created by swaroop on 19/03/14.
//  Copyright (c) 2014 iDeveloper. All rights reserved.
//

#import "PDRelocationViewController.h"
#import "AsyncImageView.h"
#import "Reachability.h"

@interface PDRelocationViewController () <UIActionSheetDelegate, UITextFieldDelegate, UITextViewDelegate>
{
    NSMutableArray *arrayContents, *arrObjRef;
    UIButton *button;
    NSArray *strings;
    int banery;
    BOOL isHotSpotAd;
    UIScrollView *scrollView;
}
@end

@implementation PDRelocationViewController
CLLocation *location_updated;
NSString *adLink;

NSString *confirmMessage, *textViewPlaceholder, *actionSheetTitle, *emailReg, *phoneReg;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (IS_IPHONE5) {
        banery = 470;
    }
    else {
        banery = 382;
    }
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = 250;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if (IS_OS_8_OR_LATER){
        [_locationManager requestWhenInUseAuthorization];}
    [_locationManager startUpdatingLocation];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    self.navigationController.navigationBarHidden = YES;
    arrObjRef = [[NSMutableArray alloc] init];
}
-(BOOL)reachable_form {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}
-(void)viewWillAppear:(BOOL)animated
{
    if([self reachable_form]){
       [self startWebService];
    }
    else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"This feature is not available in offline mode" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
       
        [alertView show];
        return;
    }
    
}
-(void)dismissKeyboard {
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    location_updated = [locations lastObject];
    isHotSpotAd = NO;
    NSMutableArray *arrHotSpots = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"HotSpotArray"]];
    for (NSDictionary *dictDetails in arrHotSpots) {
        CLLocation *targetLocation = [[CLLocation alloc] initWithLatitude:[[dictDetails objectForKey:@"lat"] doubleValue] longitude:[[dictDetails objectForKey:@"long"] doubleValue]];
        CLLocationDistance maxRadius = [[dictDetails objectForKey:@"radius"] floatValue]; // in meters
        isHotSpotAd = ([location_updated distanceFromLocation:targetLocation] <= maxRadius)?YES:NO;
        if (isHotSpotAd) {
            [[NSUserDefaults standardUserDefaults] setObject:dictDetails forKey:@"HotSpotAd"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
        }
    }
    
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
     if([self reachable_form])
    if(![removAdStatus isEqualToString:@"purchased"])
    [self showAd];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
     if([self reachable_form])
    if(![removAdStatus isEqualToString:@"purchased"])
    [self showAd];
}
-(void)showAd
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CustomAd"] isEqualToString:@"NO"]) {
        [self setAdbannerWithKey:[[NSUserDefaults standardUserDefaults] objectForKey:@"admob_id"]];
    }
    else {
        AsyncImageView *ad1Image = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, banery, 320, 50)];
        [self.view addSubview:ad1Image];
        UIButton *ad1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
        ad1Btn.frame = CGRectMake(0, banery, 320, 50);
        [ad1Btn addTarget:self action:@selector(loadAd1) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:ad1Btn];
        
        if (isHotSpotAd) {
            NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HotSpotAd"];
            if (![[dict objectForKey:@"image_listing"] isEqualToString:@"NA"]) {
                ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"image_listing"]];
                adLink = [dict objectForKey:@"link_listing"];
                int scrollHeight;

            }
        }
        else {
            NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"CustomAdDict"];
            if (![[dict objectForKey:@"ad_3"] isEqualToString:@"NA"]) {
                ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"ad_3"]];
                adLink = [dict objectForKey:@"link_3"];
                int scrollHeight;

            }
        }
    }
}
-(void)setAdbannerWithKey:(NSString *) key
{
    bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    bannerView_.adUnitID = key;
    bannerView_.delegate = self;
    bannerView_.rootViewController = self;
    [bannerView_ loadRequest:[GADRequest request]];
    bannerView_.frame = CGRectMake(0, banery, bannerView_.frame.size.width, bannerView_.frame.size.height);
    [self.view addSubview:bannerView_];
}
// Start Web service here
-(void)startWebService
{
    WebService *webService = [[WebService alloc] init];
    webService.responseTarget = self;
    webService.respondToMethod = @selector(webServiceResponse:);
    [webService startParsing:[NSString stringWithFormat:@"%@%@&device=device_type",RELOC_WEBSERVICE,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]];
}

// Get web service response here
-(void)webServiceResponse:(NSData *) responseData
{
    NSError *er;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&er];
    NSDictionary *dictValues = [dict objectForKey:@"settings"];
    NSDictionary *dictForm = [dictValues objectForKey:@"default"];
    if ([[dictForm objectForKey:@"click_phone"] isEqualToString:@"yes"]) {
        NSString *phoneNumber = [dictForm objectForKey:@"phone"];
        NSString *phoneURLString = [NSString stringWithFormat:@"telprompt://%@", phoneNumber];
        phoneURLString = [phoneURLString stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSLog(@"phone url string = %@",phoneURLString);
        NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
        [[UIApplication sharedApplication] openURL:phoneURL];
        return;
    }
    NSArray *arrTemp = [dictValues objectForKey:@"fields"];
    NSArray *arrTemp2 = [arrTemp objectAtIndex:0];
    if ([arrTemp2 count] <= 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Nothing to show" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    lblTitle.text = [dictForm objectForKey:@"form_title"];
    confirmMessage = [dictForm objectForKey:@"confirmation_msg"];
    emailReg = [dictForm objectForKey:@"email_regex"];
    phoneReg = [dictForm objectForKey:@"phone_regex"];
    arrayContents = [[NSMutableArray alloc] initWithArray:arrTemp2];
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"order"  ascending:YES];
    NSArray *arrtemp3 = [arrayContents sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
    arrayContents = [arrtemp3 copy];
    [scrollView removeFromSuperview];
    [self setUpScroll];
    
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
     if([self reachable_form])
    if(![removAdStatus isEqualToString:@"purchased"])
    [self showAd];
}
-(void)setUpScroll
{
    scrollView = [[UIScrollView alloc] init];
    if (IS_IPHONE5)
        scrollView.frame = CGRectMake(0, 48, 320,468);
    else
        scrollView.frame = CGRectMake(0, 48, 320,380);
    int i = 0, k = 0;
    [self.view addSubview:scrollView];
    [scrollView setContentSize:CGSizeMake(320, 1000)];
    [arrObjRef removeAllObjects];
    for (NSDictionary *dictionary in arrayContents) {
        if ([dictionary objectForKey:@"field_name"]) {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, i * 40 + 20, 100, 20)];
            label.text = [dictionary objectForKey:@"field_name"];
            label.backgroundColor = [UIColor clearColor];
            label.font = [UIFont fontWithName:@"Verdana" size:13];
            //[scrollView addSubview:label];
            
            if ([[dictionary objectForKey:@"type"] isEqualToString:@"textfield"]) {
                UITextField *textfield = [[UITextField alloc] initWithFrame:CGRectMake(20, i * 40 + 20, 280, 30)];
                textfield.delegate = self;
                textfield.placeholder = [dictionary objectForKey:@"field_name"];
                textfield.borderStyle = UITextBorderStyleRoundedRect;
                [scrollView addSubview:textfield];
                [arrObjRef addObject:textfield];
                if ([[dictionary objectForKey:@"field_name"] isEqualToString:@"Email"])
                    textfield.keyboardType = UIKeyboardTypeEmailAddress;
                if ([[dictionary objectForKey:@"field_name"] isEqualToString:@"Phone"])
                    textfield.keyboardType = UIKeyboardTypePhonePad;
            }
            if ([[dictionary objectForKey:@"type"] isEqualToString:@"textarea"]) {
                UITextView *textview = [[UITextView alloc] initWithFrame:CGRectMake(20, i * 40 + 20, 280, 100)];
                textview.delegate = self;
                textview.layer.borderWidth = 2.0f;
                textview.textColor = [UIColor grayColor];
                textViewPlaceholder = [dictionary objectForKey:@"field_name"];
                textview.text = [dictionary objectForKey:@"field_name"];
                textview.layer.borderColor = [[UIColor grayColor] CGColor];
                [scrollView addSubview:textview];
                [arrObjRef addObject:textview];
                i+=2;
            }
            if ([[dictionary objectForKey:@"type"] isEqualToString:@"dropdown"]) {
                button = [UIButton buttonWithType:UIButtonTypeCustom];
                button.frame = CGRectMake(20, i * 40 + 20, 280, 30);
                button.tag = k;
                button.layer.cornerRadius = 5.0f;
                button.layer.borderWidth = 2.0f;
                actionSheetTitle = [dictionary objectForKey:@"field_name"];
                [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                button.layer.borderColor = [[UIColor grayColor] CGColor];
                [button setTitle:@"- select -" forState:UIControlStateNormal];
                [button addTarget:self action:@selector(onDropDown:) forControlEvents:UIControlEventTouchUpInside];
                button.backgroundColor = [UIColor whiteColor];
                [scrollView addSubview:button];
                [arrObjRef addObject:button];
            }
        }
        i++;
        k++;
    }
    UIButton *btnSubmit = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSubmit.frame = CGRectMake(20, i * 40 + 20, 100, 26);
    [btnSubmit setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnSubmit.layer.borderColor = [[UIColor grayColor] CGColor];
    [btnSubmit setBackgroundImage:[UIImage imageNamed:@"sendbutton.png"] forState:UIControlStateNormal];
    [btnSubmit addTarget:self action:@selector(onSubmit:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:btnSubmit];
    
    [scrollView setContentSize:CGSizeMake(320, i * 20 + 450)];
}
-(void)submitFormWithArray:(NSArray *) arrayPassed
{
    NSMutableString *stringPass = [[NSMutableString alloc] initWithString:@"values={"];
    int k = 0;
    for (NSDictionary *dic in arrayContents) {
        NSString *strTemp = [NSString stringWithFormat:@"\"%@\":\"%@\",",[dic objectForKey:@"field_name"],[arrayPassed objectAtIndex:k]];
        [stringPass appendString:strTemp];
        k++;
    }
    [stringPass deleteCharactersInRange:NSMakeRange([stringPass length]-1, 1)];
    [stringPass appendString:@"}"];
    NSLog(@"passString = %@",stringPass);
    
    NSData *postData = [stringPass dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",FORM_SUBMIT_WEBSERVICE,[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    //get response
    NSHTTPURLResponse* urlResponse = nil;
    NSError *error = [[NSError alloc] init];
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
    NSString *result = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"Response Code: %d \nresult = %@", [urlResponse statusCode],result);
    
    NSLog(@"Response Code: %ld \n %@", (long)[urlResponse statusCode], request);
    if ([urlResponse statusCode] >= 200 && [urlResponse statusCode] < 300)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Success" message:confirmMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [vwProcessing removeFromSuperview];
        [alertView show];
    }
    else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Error in submission. Please try later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        [vwProcessing removeFromSuperview];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
  //  PDViewController *viewController = [[PDViewController alloc] init];
  //  [self.navigationController pushViewController:viewController animated:NO];
    [scrollView removeFromSuperview];
    [self setUpScroll];
}
-(IBAction)onSubmit:(id)sender
{
    NSLog(@"%@",arrObjRef);
    NSMutableArray *arrValues = [[NSMutableArray alloc]init];;
    for (int i = 0; i < [arrObjRef count]; i++) {
        
        if ([[arrObjRef objectAtIndex:i] isKindOfClass:[UITextField class]]) {
            UITextField *textField = (UITextField *) [arrObjRef objectAtIndex:i];
            [arrValues addObject:[textField.text length]>0?textField.text:@""];
        }
        if ([[arrObjRef objectAtIndex:i] isKindOfClass:[UITextView class]]) {
            UITextView *textView = (UITextView *) [arrObjRef objectAtIndex:i];
            [arrValues addObject:[textView.text length]>0?textView.text:@""];
        }
        if ([[arrObjRef objectAtIndex:i] isKindOfClass:[UIButton class]]) {
            UIButton *buttonSubmit = (UIButton *) [arrObjRef objectAtIndex:i];
            [arrValues addObject:buttonSubmit.titleLabel.text?buttonSubmit.titleLabel.text:@""];
        }
    }
    BOOL flagSubmit = YES;
    for (int q = 0; q < [arrayContents count]; q++) {
        NSDictionary *dictComp  = [arrayContents objectAtIndex:q];
        if ([[dictComp objectForKey:@"mandatory"] isEqualToString:@"yes"]) {
            flagSubmit = [self validateValue:[arrValues objectAtIndex:q] withType:[dictComp objectForKey:@"validate_for"]];
            if (!flagSubmit)
                break;
        }
    }
    if (flagSubmit) {
        vwProcessing.frame = CGRectMake(0, IS_IPHONE5?250:200, 320, vwProcessing.frame.size.height);
        [self.view addSubview:vwProcessing];
        [self submitFormWithArray:arrValues];
    }
    else {
        NSLog(@"validation error in form submition");
    }
}
-(BOOL)validateValue:(NSString *) value
            withType:(NSString *) key
{
    if ([key isEqualToString:@"Normal"]) {
        int k = [value length];
        BOOL x = k>0?YES:NO;
        if ([value isEqualToString:@"- select -"]) 
            x = NO;
        if (!x) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Required field is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        }
        return x;
    }
    if ([key isEqualToString:@"Phone"]) {
        NSString *phoneRegex = phoneReg;
        NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
        BOOL matches = [test evaluateWithObject:value];
        if (!matches) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Enter a valid phone number" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        }
        return matches;
    }
    if ([key isEqualToString:@"Email"]) {
//        BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
//        NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
//        NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
//        NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
        NSPredicate *emailTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailReg];
       // NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        BOOL myStringMatchesRegEx =[emailTest evaluateWithObject:value];
        if (!myStringMatchesRegEx) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Enter a valid email id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        }
        return myStringMatchesRegEx;
    }
    return NO;
}
-(IBAction)onDropDown:(id)sender
{
    UIButton *btn = (UIButton *) sender;
    NSDictionary *dict = [arrayContents objectAtIndex:btn.tag];
    NSString *str = [dict objectForKey:@"values"];
    strings = [str componentsSeparatedByString:@","];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:actionSheetTitle
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:@"Cancel"
                                                    otherButtonTitles:nil];
    
    for (NSString *title in strings) {
        [actionSheet addButtonWithTitle:title];
    }
    [actionSheet addButtonWithTitle:@"Clear"];
    [actionSheet showInView:self.view];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:textViewPlaceholder]) {
        textView.text = @"";
    }
    textView.textColor = [UIColor blackColor];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex > [strings count]) {
        [button setTitle:@"- select -" forState:UIControlStateNormal];
        return;
    }
    if (buttonIndex == 0) {
        return;
    }
    [button setTitle:[strings objectAtIndex:buttonIndex-1] forState:UIControlStateNormal];
}
-(IBAction)onBack:(id)sender
{
    PDViewController *viewController = [[PDViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:NO];
}
@end
