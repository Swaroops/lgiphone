

#import <UIKit/UIKit.h>
#import "NSString+HTML.h"
#import "MWFeedParser.h"

@interface PDRssListViewController : UIViewController <MWFeedParserDelegate,GADBannerViewDelegate>
{
    GADBannerView *bannerView_;
    IBOutlet UIView *vwLoading;
    
    // Parsing
	MWFeedParser *feedParser;
	NSMutableArray *parsedItems;
    IBOutlet UILabel *titlLabel;
	
	// Displaying
	NSArray *itemsToDisplay;
	NSDateFormatter *formatter;
    
    IBOutlet UILabel *lbTitle;
}
@property BOOL isFromHome;
@property (nonatomic, retain) CLLocationManager *locationManager;
// Properties
@property (nonatomic, strong) NSArray *itemsToDisplay;
@property (nonatomic, retain) IBOutlet UITableView *tableView;

-(void)getDictionary:(NSDictionary *) dict;
-(IBAction)onBack:(id)sender;
@end
