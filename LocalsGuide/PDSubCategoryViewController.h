
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface PDSubCategoryViewController : UIViewController <CLLocationManagerDelegate, GADBannerViewDelegate,UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UIView *vwLoading;
    IBOutlet UILabel *titleLabel;
    IBOutlet UITableView *tblContents;
    GADBannerView *bannerView_;
    
    UISearchBar *searchBar;
    NSMutableArray *searchArray;
}
@property (nonatomic, retain) CLLocationManager *locationManager;
-(void)getSubId:(NSString *) passId
         ofType:(NSString *) passType
       withName:(NSString *) name;
-(IBAction)onBack:(id)sender;
-(void)getSubArray:(NSArray *) sub;
@end
