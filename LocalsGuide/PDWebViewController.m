//
//  PDWebViewController.m
//  Visiting Ashland
//
//  Created by swaroop on 04/04/14.
//  Copyright (c) 2014 iDeveloper. All rights reserved.
//

#import "PDWebViewController.h"
#import "Reachability.h"

@interface PDWebViewController ()
{
    int banery;
    BOOL isHotSpotAd;
}
@end

@implementation PDWebViewController
NSDictionary *dictHere;
CLLocation *location_updated;
NSString *adLink;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (IS_IPHONE5) {
        banery = 470;
    }
    else {
        banery = 382;
    }
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = 250;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [_locationManager startUpdatingLocation];
    self.navigationController.navigationBarHidden = YES;
    // Do any additional setup after loading the view from its nib.
    
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
    if([self reachable])
        if(![removAdStatus isEqualToString:@"purchased"])
        [self showAd];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    location_updated = [locations lastObject];
    isHotSpotAd = NO;
    NSMutableArray *arrHotSpots = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"HotSpotArray"]];
    for (NSDictionary *dictDetails in arrHotSpots) {
        CLLocation *targetLocation = [[CLLocation alloc] initWithLatitude:[[dictDetails objectForKey:@"lat"] doubleValue] longitude:[[dictDetails objectForKey:@"long"] doubleValue]];
        CLLocationDistance maxRadius = [[dictDetails objectForKey:@"radius"] floatValue]; // in meters
        isHotSpotAd = ([location_updated distanceFromLocation:targetLocation] <= maxRadius)?YES:NO;
        if (isHotSpotAd) {
            [[NSUserDefaults standardUserDefaults] setObject:dictDetails forKey:@"HotSpotAd"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
        }
    }
    
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    
     if([self reachable])
    if(![removAdStatus isEqualToString:@"purchased"])
    [self showAd];
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    
    NSString *removeAdKey=    [NSString stringWithFormat:@"remove_ads%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currentAppId"]];
    NSString *removAdStatus=[[NSUserDefaults standardUserDefaults]objectForKey:removeAdKey];
    if([self reachable])
        if(![removAdStatus isEqualToString:@"purchased"])
    [self showAd];
}
-(void)showAd
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CustomAd"] isEqualToString:@"NO"]) {
        [self setAdbannerWithKey:[[NSUserDefaults standardUserDefaults] objectForKey:@"admob_id"]];
    }
    else {
        AsyncImageView *ad1Image = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, banery, 320, 50)];
        [self.view addSubview:ad1Image];
        UIButton *ad1Btn = [UIButton buttonWithType:UIButtonTypeCustom];
        ad1Btn.frame = CGRectMake(0, banery, 320, 50);
        [ad1Btn addTarget:self action:@selector(loadAd1) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:ad1Btn];
        NSDictionary *dictHotspot = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HotSpotAd"];
        if (isHotSpotAd && (![[dictHotspot objectForKey:@"image_listing"] isEqualToString:@"FALSE"])) {
            NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"HotSpotAd"];
            if (![[dict objectForKey:@"image_listing"] isEqualToString:@"NA"]) {
                ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"image_listing"]];
                adLink = [dict objectForKey:@"link_listing"];
            }
        }
        else {
            NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"CustomAdDict"];
            if (![[dict objectForKey:@"ad_3"] isEqualToString:@"NA"]) {
                ad1Image.imageURL = [NSURL URLWithString:[dict objectForKey:@"ad_3"]];
                adLink = [dict objectForKey:@"link_3"];
            }
        }
    }
}
-(void)setAdbannerWithKey:(NSString *) key
{
    bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    bannerView_.adUnitID = key;
    bannerView_.delegate = self;
    bannerView_.rootViewController = self;
    [bannerView_ loadRequest:[GADRequest request]];
    bannerView_.frame = CGRectMake(0, banery, bannerView_.frame.size.width, bannerView_.frame.size.height);
    [self.view addSubview:bannerView_];
}
-(void)viewWillAppear:(BOOL)animated
{
    nameLabel.text = [dictHere objectForKey:@"name"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[dictHere objectForKey:@"external_url"]]];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)reachable {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}
-(void)getDictionary:(NSDictionary *)dictDetails
{
    dictHere = dictDetails;
}
@end
