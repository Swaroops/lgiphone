

#import "WebService.h"
@implementation WebService
@synthesize respondToMethod = _respondToMethod, urlConnection = _urlConnection, responseData = _responseData, responseString = _responseString, responseTarget = _responseTarget,respondFailMethod=_respondFailMethod;
-(void)startParsing: (NSString *) URL
{
    NSLog(@"URL = %@",URL);
    URL = [URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    self.responseData = [NSMutableData data];
    NSURL *url = [NSURL URLWithString:URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    _urlConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    NSAssert(self.urlConnection != nil, @"Failure to create URL connection.");
}

#pragma mark connection delegate functions
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    if(!_withOutRetry)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Could not connect to server"
                                                       delegate:_responseTarget cancelButtonTitle:@"OK" otherButtonTitles:@"Retry",nil];
        alert.tag=346;
        
       // [alert show];
    }
    
    if ([_responseTarget respondsToSelector:_respondFailMethod])
    {
       
        [_responseTarget performSelector:_respondFailMethod withObject:nil afterDelay:0];
    }
    _responseData = nil;
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [_responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [_responseData appendData:data];
}
#pragma mark - Finish Loading -
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    _responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
    if ([_responseTarget respondsToSelector:_respondToMethod])
    {
        [_responseTarget performSelector:_respondToMethod withObject:_responseData afterDelay:0];
    }
    self.responseData = nil;
}
@end
